﻿using System;
using System.Data;

namespace AS.Crossbow.Core.Data
{
    public static class ExtensionMethods
    {
        #region Public Methods

        public static int AddInputParameter<T>(this IDbCommand cmd, string name, T value)
        {
            var p = cmd.CreateParameter();
            p.ParameterName = name;
            p.Value = value;
            return cmd.Parameters.Add(p);
        }

        public static int AddInputParameter<T>(this IDbCommand cmd, string name, T? value) where T : struct
        {
            var p = cmd.CreateParameter();
            p.ParameterName = name;
            p.Value = value.HasValue ? (object) value : DBNull.Value;
            return cmd.Parameters.Add(p);
        }

        public static int AddInputParameter(this IDbCommand cmd, string name, string value)
        {
            var p = cmd.CreateParameter();
            p.ParameterName = name;
            p.Value = string.IsNullOrEmpty(value) ? DBNull.Value : (object) value;
            return cmd.Parameters.Add(p);
        }

        public static IDbDataParameter AddOutputParameter(this IDbCommand cmd, string name, DbType dbType)
        {
            var p = cmd.CreateParameter();
            p.ParameterName = name;
            p.DbType = dbType;
            p.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(p);
            return p;
        }

        public static IDbCommand CreateCommand(this IDbTransaction transaction)
        {
            var command = transaction.Connection.CreateCommand();
            command.Transaction = transaction;

            return command;
        }

        #endregion
    }
}