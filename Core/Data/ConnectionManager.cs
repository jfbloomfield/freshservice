﻿using System;
using System.Configuration;
using System.Data;
using System.Data.Common;

namespace AS.Crossbow.Core.Data
{
    public static class ConnectionManager
    {
        #region Constants

        private const string READ_ONLY_CONNECTION_STRING_KEY = "DB_READ_ONLY";
        private const string WRITABLE_CONNECTION_STRING_KEY = "DB_WRITABLE";

        #endregion

        #region Public Methods

        public static IDbConnection GetOpenConnection(string connectionKey)
        {
            if (connectionKey == null)
            {
                throw new ArgumentNullException(nameof(connectionKey));
            }

            var setting = ConfigurationManager.ConnectionStrings[connectionKey];

            if (setting == null)
            {
                throw new ConfigurationErrorsException(
                    $"The ConnectionStrings web.config section is missing the following key [{connectionKey}]");
            }

            return GetOpenConnection(setting.ProviderName, setting.ConnectionString);
        }

        public static IDbConnection GetOpenReadOnlyConnection(int tenantId)
        {
            var setting = GetReadOnlyConnectionStringSetting(tenantId);

            return GetOpenConnection(setting.ProviderName, setting.ConnectionString);
        }

        public static IDbConnection GetOpenWritableConnection(int tenantId)
        {
            var setting = GetWritableConnectionStringSetting(tenantId);

            return GetOpenConnection(setting.ProviderName, setting.ConnectionString);
        }

        #endregion

        #region Private Methods

        private static IDbConnection GetOpenConnection(string providerName, string connectionString)
        {
            if (providerName == null)
            {
                throw new ArgumentNullException(nameof(providerName));
            }
            if (connectionString == null)
            {
                throw new ArgumentNullException(nameof(connectionString));
            }

            var provider = DbProviderFactories.GetFactory(providerName);

            var cnn = provider.CreateConnection();

            if (cnn == null)
            {
                throw new DataException($"Could not create a connection for Provider Name [{provider}].");
            }

            cnn.ConnectionString = connectionString;
            cnn.Open();

            return cnn;
        }

        private static ConnectionStringSettings GetReadOnlyConnectionStringSetting(int tenantId)
        {
            if (ConfigurationManager.ConnectionStrings[READ_ONLY_CONNECTION_STRING_KEY] == null)
            {
                throw new ConfigurationErrorsException(
                    $"The ConnectionStrings web.config section is missing the following key [{READ_ONLY_CONNECTION_STRING_KEY}]");
            }

            return ConfigurationManager.ConnectionStrings[READ_ONLY_CONNECTION_STRING_KEY];
        }

        private static ConnectionStringSettings GetWritableConnectionStringSetting(int tenantId)
        {
            if (ConfigurationManager.ConnectionStrings[WRITABLE_CONNECTION_STRING_KEY] == null)
            {
                throw new ConfigurationErrorsException(
                    $"The ConnectionStrings web.config section is missing the following key [{WRITABLE_CONNECTION_STRING_KEY}]");
            }

            return ConfigurationManager.ConnectionStrings[WRITABLE_CONNECTION_STRING_KEY];
        }

        #endregion
    }
}