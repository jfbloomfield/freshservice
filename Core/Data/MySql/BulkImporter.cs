﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using CsvHelper;
using CsvHelper.Configuration;
using CsvHelper.TypeConversion;
using MySql.Data.MySqlClient;

namespace AS.Crossbow.Core.Data.MySql
{
    public class BulkImporter : IBulkImporter
    {
        #region Constructors

        public BulkImporter()
        {
            RegisterTypeConverters();
        }

        #endregion

        #region IBulkImporter Members

        public int BulkInsert<T>(
            IEnumerable<T> data,
            MySqlTransaction transaction,
            string tableName,
            IEnumerable<CsvClassMap> classMaps)
        {
            if (data == null)
            {
                throw new ArgumentNullException(nameof(data));
            }
            if (transaction == null)
            {
                throw new ArgumentNullException(nameof(transaction));
            }
            if (tableName == null)
            {
                throw new ArgumentNullException(nameof(tableName));
            }

            var file = WriteToCsv(data, classMaps);
            var headers = ReadFileHeaderRow(file);
            var mySqlBulkLoader = new MySqlBulkLoader(transaction.Connection)
            {
                TableName = tableName,
                FileName = file.FullName,
                FieldTerminator = ",",
                FieldQuotationCharacter = '"',
                LineTerminator = Environment.NewLine,
                NumberOfLinesToSkip = 1,
                CharacterSet = "utf8"
            };
            mySqlBulkLoader.Columns.AddRange(headers);
            var rowsInserted = mySqlBulkLoader.Load();
            try
            {
                File.Delete(file.FullName);
            }
            catch (Exception)
            {
            }
            return rowsInserted;
        }

        public int BulkInsert<T>(IEnumerable<T> data, MySqlTransaction transaction, string tableName)
        {
            return BulkInsert(data, transaction, tableName, null);
        }

        public int BulkInsertFromFile(FileInfo file, IDbTransaction transaction, string tableName)
        {
            if (file == null)
            {
                throw new ArgumentNullException(nameof(file));
            }
            if (transaction == null)
            {
                throw new ArgumentNullException(nameof(transaction));
            }
            var headers = ReadFileHeaderRow(file);

            var mySqlBulkLoader = new MySqlBulkLoader((MySqlConnection) transaction.Connection)
            {
                TableName = tableName,
                FileName = file.FullName,
                FieldTerminator = ",",
                FieldQuotationCharacter = '"',
                LineTerminator = Environment.NewLine,
                NumberOfLinesToSkip = 1,
                CharacterSet = "utf8"
            };
            mySqlBulkLoader.Columns.AddRange(headers);
            var rowsInserted = mySqlBulkLoader.Load();
            try
            {
                File.Delete(file.FullName);
            }
            catch (Exception ex)
            {
                var temp = ex.Message;
            }
            return rowsInserted;
        }

        #endregion

        #region Private Methods

        private IEnumerable<string> ReadFileHeaderRow(FileSystemInfo file)
        {
            var headers = new List<string>();
            using (var fileStream = File.Open(file.FullName, FileMode.Open))
            {
                using (var streamReader = new StreamReader(fileStream))
                {
                    using (var csvReader = new CsvReader(streamReader))
                    {
                        if (csvReader.Read())
                        {
                            headers.AddRange(csvReader.FieldHeaders);
                        }
                    }
                }
            }
            return headers;
        }

        private void RegisterTypeConverters()
        {
            TypeConverterFactory.AddConverter<string>(new MySqlStringConverter());

            TypeConverterFactory.AddConverter<bool?>(new MySqlNullableConverter(typeof (bool?)));
            TypeConverterFactory.AddConverter<byte?>(new MySqlNullableConverter(typeof (byte?)));
            TypeConverterFactory.AddConverter<char?>(new MySqlNullableConverter(typeof (char?)));
            TypeConverterFactory.AddConverter<DateTime?>(new MySqlNullableConverter(typeof (DateTime?)));
            TypeConverterFactory.AddConverter<DateTimeOffset?>(new MySqlNullableConverter(typeof (DateTimeOffset?)));
            TypeConverterFactory.AddConverter<decimal?>(new MySqlNullableConverter(typeof (decimal?)));
            TypeConverterFactory.AddConverter<double?>(new MySqlNullableConverter(typeof (double?)));
            TypeConverterFactory.AddConverter<float?>(new MySqlNullableConverter(typeof (float?)));
            TypeConverterFactory.AddConverter<Guid?>(new MySqlNullableConverter(typeof (Guid?)));
            TypeConverterFactory.AddConverter<short?>(new MySqlNullableConverter(typeof (short?)));
            TypeConverterFactory.AddConverter<int?>(new MySqlNullableConverter(typeof (int?)));
            TypeConverterFactory.AddConverter<long?>(new MySqlNullableConverter(typeof (long?)));
            TypeConverterFactory.AddConverter<sbyte?>(new MySqlNullableConverter(typeof (sbyte?)));
            TypeConverterFactory.AddConverter<TimeSpan?>(new MySqlNullableConverter(typeof (TimeSpan?)));
            TypeConverterFactory.AddConverter<ushort?>(new MySqlNullableConverter(typeof (ushort?)));
            TypeConverterFactory.AddConverter<uint?>(new MySqlNullableConverter(typeof (uint?)));
            TypeConverterFactory.AddConverter<ulong?>(new MySqlNullableConverter(typeof (ulong?)));
        }

        private FileInfo WriteToCsv<T>(IEnumerable<T> data, IEnumerable<CsvClassMap> classMaps = null)
        {
            var fileName = Path.GetTempFileName();

            var file = new FileInfo(fileName);
            using (var fileStream = File.Open(fileName, FileMode.Truncate, FileAccess.Write))
            {
                using (var streamWriter = new StreamWriter(fileStream, Encoding.UTF8))
                {
                    using (var csvWriter = new CsvWriter(streamWriter))
                    {
                        if (classMaps != null)
                        {
                            foreach (var csvClassMap in classMaps)
                            {
                                csvWriter.Configuration.RegisterClassMap(csvClassMap);
                            }
                        }
                        csvWriter.WriteHeader<T>();
                        foreach (var datum in data)
                        {
                            csvWriter.WriteRecord(datum);
                        }
                    }
                }
            }
            return file;
        }

        #endregion
    }
}