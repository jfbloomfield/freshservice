﻿using System;
using CsvHelper.TypeConversion;

namespace AS.Crossbow.Core.Data.MySql
{
    public class MySqlNullableConverter : NullableConverter
    {
        #region Constructors

        public MySqlNullableConverter(Type type) : base(type)
        {
        }

        #endregion

        #region Public Methods

        public override string ConvertToString(TypeConverterOptions options, object value)
        {
            return value == null ? @"\N" : base.ConvertToString(options, value);
        }

        #endregion
    }

    public class MySqlStringConverter : StringConverter
    {
        #region Public Methods

        public override string ConvertToString(TypeConverterOptions options, object value)
        {
            return value == null ? @"\N" : base.ConvertToString(options, value);
        }

        #endregion
    }
}