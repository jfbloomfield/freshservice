using System.Collections.Generic;
using System.Data;
using System.IO;
using CsvHelper.Configuration;
using MySql.Data.MySqlClient;

namespace AS.Crossbow.Core.Data.MySql
{
    public interface IBulkImporter
    {
        #region Public Methods

        int BulkInsert<T>(
            IEnumerable<T> data,
            MySqlTransaction transaction,
            string tableName,
            IEnumerable<CsvClassMap> classMaps);

        int BulkInsert<T>(IEnumerable<T> data, MySqlTransaction transaction, string tableName);

        int BulkInsertFromFile(FileInfo file, IDbTransaction transaction, string tableName);

        #endregion
    }
}