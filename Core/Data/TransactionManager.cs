﻿using System;
using System.Data;

namespace AS.Crossbow.Core.Data
{
    public static class TransactionManager
    {
        #region Public Methods

        public static IDbTransaction GetTransaction(IDbConnection connection)
        {
            if (connection == null)
            {
                throw new ArgumentNullException(nameof(connection));
            }

            if (connection.State != ConnectionState.Open)
            {
                connection.Open();
            }

            return connection.BeginTransaction(IsolationLevel.ReadCommitted);
        }

        #endregion
    }
}