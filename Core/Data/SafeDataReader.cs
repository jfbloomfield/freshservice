﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

namespace AS.Crossbow.Core.Data
{
    /// <summary>
    ///     This is a DataReader that 'fixes' any null values before they are returned to our business code. NOTE: This code
    ///     varies slightly from code which is found in the CSLA framework The CSLA framework can be found at
    ///     http://www.lhotka.net/cslanet/
    /// </summary>
    public class SafeDataReader : IDataReader
    {
        #region Member Variables

        private bool _disposedValue; // To detect redundant calls

        #endregion

        #region Constructors

        public SafeDataReader(IDataReader dataReader)
        {
            if (dataReader == null)
            {
                throw new ArgumentNullException(nameof(dataReader));
            }

            DataReader = dataReader;
        }

        #endregion

        #region Properties

        /// <summary>
        /// </summary>
        protected IDataReader DataReader { get; }

        public int Depth => DataReader.Depth;

        public int FieldCount => DataReader.FieldCount;

        public bool IsClosed => DataReader.IsClosed;

        public object this[string name]
        {
            get
            {
                if (name == null)
                {
                    throw new ArgumentNullException(nameof(name));
                }

                var val = DataReader[name];

                return DBNull.Value.Equals(val) ? null : val;
            }
        }

        public virtual object this[int i] => DataReader.IsDBNull(i) ? null : DataReader[i];

        public int RecordsAffected => DataReader.RecordsAffected;

        #endregion

        #region IDataReader Members

        public void Close()
        {
            DataReader.Close();
        }

        public DataTable GetSchemaTable()
        {
            return DataReader.GetSchemaTable();
        }

        public bool NextResult()
        {
            return DataReader.NextResult();
        }

        public bool Read()
        {
            return DataReader.Read();
        }

        #endregion

        #region IDataRecord Members

        public virtual bool GetBoolean(int i)
        {
            return GetBoolean(i, false);
        }

        public virtual byte GetByte(int i)
        {
            return GetByte(i, 0);
        }

        public virtual long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
        {
            if (buffer == null)
            {
                throw new ArgumentNullException(nameof(buffer));
            }

            return GetBytes(i, fieldOffset, buffer, bufferoffset, length, 0);
        }

        public virtual char GetChar(int i)
        {
            return GetChar(i, char.MinValue);
        }

        public virtual long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
        {
            if (buffer == null)
            {
                throw new ArgumentNullException(nameof(buffer));
            }

            return GetChars(i, fieldoffset, buffer, bufferoffset, length, 0);
        }

        public virtual IDataReader GetData(int i)
        {
            return DataReader.GetData(i);
        }

        public virtual string GetDataTypeName(int i)
        {
            return DataReader.GetDataTypeName(i);
        }

        public virtual DateTime GetDateTime(int i)
        {
            return GetDateTime(i, DateTime.MinValue);
        }

        public virtual decimal GetDecimal(int i)
        {
            return GetDecimal(i, 0);
        }

        public virtual double GetDouble(int i)
        {
            return GetDouble(i, 0);
        }

        public virtual Type GetFieldType(int i)
        {
            return DataReader.GetFieldType(i);
        }

        public virtual float GetFloat(int i)
        {
            return GetFloat(i, 0);
        }

        public virtual Guid GetGuid(int i)
        {
            return GetGuid(i, Guid.Empty);
        }

        public virtual short GetInt16(int i)
        {
            return GetInt16(i, 0);
        }

        public virtual int GetInt32(int i)
        {
            return GetInt32(i, 0);
        }

        public virtual long GetInt64(int i)
        {
            return GetInt64(i, 0);
        }

        public virtual string GetName(int i)
        {
            return DataReader.GetName(i);
        }

        public virtual int GetOrdinal(string name)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            return DataReader.GetOrdinal(name);
        }

        public virtual string GetString(int i)
        {
            return GetString(i, string.Empty);
        }

        public virtual object GetValue(int i)
        {
            return GetValue(i, null);
        }

        public virtual int GetValues(object[] values)
        {
            if (values == null)
            {
                throw new ArgumentNullException(nameof(values));
            }

            return DataReader.GetValues(values);
        }

        public virtual bool IsDBNull(int i)
        {
            return DataReader.IsDBNull(i);
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.

            Dispose(true);

            GC.SuppressFinalize(this);
        }

        #endregion

        #region Public Methods

        public virtual IEnumerable<IDataRecord> AsEnumerable()
        {
            while (DataReader.Read())
            {
                yield return DataReader;
            }
        }

        public virtual bool GetBoolean(string name)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            return GetBoolean(DataReader.GetOrdinal(name));
        }

        public virtual bool GetBoolean(string name, bool nullValue)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            return GetBoolean(DataReader.GetOrdinal(name), nullValue);
        }

        public virtual bool GetBoolean(int index, bool nullValue)
        {
            var valStringUpperCase = DataReader.GetValue(index).ToString().ToUpper();

            switch (valStringUpperCase)
            {
                case "TRUE":
                case "T":
                case "YES":
                case "Y":
                    return true;
                case "FALSE":
                case "F":
                case "NO":
                case "N":
                    return false;
                default:
                    return DataReader.IsDBNull(index) ? nullValue : Convert.ToBoolean(DataReader.GetValue(index));
            }
        }

        public virtual bool GetBoolean(string name, string trueValue)
        {
            return (string.Compare(GetString(name), trueValue, StringComparison.OrdinalIgnoreCase) == 0);
        }

        public virtual bool? GetBooleanNullable(int index)
        {
            if (DataReader.IsDBNull(index))
            {
                return null;
            }

            return GetBoolean(index);
        }

        public virtual bool? GetBooleanNullable(string name)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            return GetBooleanNullable(DataReader.GetOrdinal(name));
        }

        public virtual byte GetByte(string name)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            return GetByte(DataReader.GetOrdinal(name));
        }

        public virtual byte GetByte(string name, byte nullValue)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            return GetByte(DataReader.GetOrdinal(name), nullValue);
        }

        public virtual byte GetByte(int index, byte nullValue)
        {
            return DataReader.IsDBNull(index) ? nullValue : DataReader.GetByte(index);
        }

        public virtual byte? GetByteNullable(string name)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            return GetByteNullable(DataReader.GetOrdinal(name));
        }

        public virtual byte? GetByteNullable(int index)
        {
            if (DataReader.IsDBNull(index))
            {
                return null;
            }

            return GetByte(index);
        }

        public virtual long GetBytes(string name, long fieldOffset, byte[] buffer, int bufferOffset, int length)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            if (buffer == null)
            {
                throw new ArgumentNullException(nameof(buffer));
            }

            return GetBytes(DataReader.GetOrdinal(name), fieldOffset, buffer, bufferOffset, length);
        }

        public virtual long GetBytes(
            string name,
            long fieldOffset,
            byte[] buffer,
            int bufferOffset,
            int length,
            long nullValue)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            if (buffer == null)
            {
                throw new ArgumentNullException(nameof(buffer));
            }

            return GetBytes(DataReader.GetOrdinal(name), fieldOffset, buffer, bufferOffset, length, nullValue);
        }

        public virtual long GetBytes(
            int index,
            long fieldOffset,
            byte[] buffer,
            int bufferOffset,
            int length,
            long nullValue)
        {
            if (buffer == null)
            {
                throw new ArgumentNullException(nameof(buffer));
            }

            return DataReader.IsDBNull(index)
                       ? nullValue
                       : DataReader.GetBytes(index, fieldOffset, buffer, bufferOffset, length);
        }

        public virtual char GetChar(string name)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            return GetChar(DataReader.GetOrdinal(name));
        }

        public virtual char GetChar(string name, char nullValue)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            return GetChar(DataReader.GetOrdinal(name), nullValue);
        }

        public virtual char GetChar(int index, char nullValue)
        {
            if (DataReader.IsDBNull(index))
            {
                return nullValue;
            }

            var myChar = new char[1];

            DataReader.GetChars(index, 0, myChar, 0, 1);

            return myChar[0];
        }

        public virtual long GetChars(string name, long fieldOffset, char[] buffer, int bufferOffset, int length)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            if (buffer == null)
            {
                throw new ArgumentNullException(nameof(buffer));
            }

            return GetChars(DataReader.GetOrdinal(name), fieldOffset, buffer, bufferOffset, length);
        }

        public virtual long GetChars(
            string name,
            long fieldOffset,
            char[] buffer,
            int bufferOffset,
            int length,
            long nullValue)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            if (buffer == null)
            {
                throw new ArgumentNullException(nameof(buffer));
            }

            return GetChars(DataReader.GetOrdinal(name), fieldOffset, buffer, bufferOffset, length, nullValue);
        }

        public virtual long GetChars(
            int index,
            long fieldOffset,
            char[] buffer,
            int bufferOffset,
            int length,
            long nullValue)
        {
            if (buffer == null)
            {
                throw new ArgumentNullException(nameof(buffer));
            }

            return DataReader.IsDBNull(index)
                       ? nullValue
                       : DataReader.GetChars(index, fieldOffset, buffer, bufferOffset, length);
        }

        public virtual IDataReader GetData(string name)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            return GetData(DataReader.GetOrdinal(name));
        }

        public virtual string GetDataTypeName(string name)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            return GetDataTypeName(DataReader.GetOrdinal(name));
        }

        public virtual DateTime GetDateTime(string name)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            return GetDateTime(DataReader.GetOrdinal(name));
        }

        public virtual DateTime GetDateTime(string name, DateTime nullValue)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            return GetDateTime(DataReader.GetOrdinal(name), nullValue);
        }

        public virtual DateTime GetDateTime(int index, DateTime nullValue)
        {
            return DataReader.IsDBNull(index) ? nullValue : Convert.ToDateTime(DataReader.GetValue(index));
        }

        public virtual DateTime? GetDateTimeNullable(int index)
        {
            if (DataReader.IsDBNull(index))
            {
                return null;
            }

            return GetDateTime(index);
        }

        public virtual DateTime? GetDateTimeNullable(string name)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            return GetDateTimeNullable(DataReader.GetOrdinal(name));
        }

        public virtual decimal GetDecimal(string name)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            return GetDecimal(DataReader.GetOrdinal(name));
        }

        public virtual decimal GetDecimal(string name, decimal nullValue)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            return GetDecimal(DataReader.GetOrdinal(name), nullValue);
        }

        public virtual decimal GetDecimal(int index, decimal nullValue)
        {
            return DataReader.IsDBNull(index) ? nullValue : Convert.ToDecimal(DataReader.GetValue(index));
        }

        public virtual decimal? GetDecimalNullable(int index)
        {
            if (DataReader.IsDBNull(index))
            {
                return null;
            }

            return GetDecimal(index);
        }

        public virtual decimal? GetDecimalNullable(string name)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            return GetDecimalNullable(DataReader.GetOrdinal(name));
        }

        public virtual double GetDouble(string name)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            return GetDouble(DataReader.GetOrdinal(name));
        }

        public virtual double GetDouble(string name, double nullValue)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            return GetDouble(DataReader.GetOrdinal(name), nullValue);
        }

        public virtual double GetDouble(int index, double nullValue)
        {
            return DataReader.IsDBNull(index) ? nullValue : Convert.ToDouble(DataReader.GetValue(index));
        }

        public virtual double? GetDoubleNullable(string name)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            return GetDoubleNullable(DataReader.GetOrdinal(name));
        }

        public virtual double? GetDoubleNullable(int index)
        {
            if (DataReader.IsDBNull(index))
            {
                return null;
            }

            return GetDouble(index);
        }

        public virtual T GetEnum<T>(string name)
        {
            return GetEnum<T>(GetOrdinal(name));
        }

        public virtual T GetEnum<T>(int i)
        {
            if (!typeof (T).IsEnum)
            {
                throw new ArgumentException(typeof (T) + " is not an Enum");
            }

            return (T) Enum.ToObject(typeof (T), GetInt32(i));
        }

        public virtual Type GetFieldType(string name)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            return GetFieldType(DataReader.GetOrdinal(name));
        }

        public virtual float GetFloat(string name)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            return GetFloat(DataReader.GetOrdinal(name));
        }

        public virtual float GetFloat(string name, float nullValue)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            return GetFloat(DataReader.GetOrdinal(name), nullValue);
        }

        public virtual float GetFloat(int index, float nullValue)
        {
            return DataReader.IsDBNull(index) ? nullValue : DataReader.GetFloat(index);
        }

        public virtual float? GetFloatNullable(string name)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            return GetFloatNullable(DataReader.GetOrdinal(name));
        }

        public virtual float? GetFloatNullable(int index)
        {
            if (DataReader.IsDBNull(index))
            {
                return null;
            }

            return GetFloat(index);
        }

        public virtual Guid GetGuid(string name)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            return GetGuid(DataReader.GetOrdinal(name));
        }

        public virtual Guid GetGuid(string name, Guid nullValue)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            return GetGuid(DataReader.GetOrdinal(name), nullValue);
        }

        public virtual Guid GetGuid(int index, Guid nullValue)
        {
            return DataReader.IsDBNull(index) ? nullValue : DataReader.GetGuid(index);
        }

        public virtual short GetInt16(string name)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            return GetInt16(DataReader.GetOrdinal(name));
        }

        public virtual short GetInt16(string name, short nullValue)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            return GetInt16(DataReader.GetOrdinal(name), nullValue);
        }

        public virtual short GetInt16(int index, short nullValue)
        {
            return DataReader.IsDBNull(index) ? nullValue : Convert.ToInt16(DataReader.GetValue(index));
        }

        public virtual short? GetInt16Nullable(string name)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            return GetInt16Nullable(DataReader.GetOrdinal(name));
        }

        public virtual short? GetInt16Nullable(int index)
        {
            if (DataReader.IsDBNull(index))
            {
                return null;
            }

            return GetInt16(index);
        }

        public virtual int GetInt32(string name)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            return GetInt32(DataReader.GetOrdinal(name));
        }

        public virtual int GetInt32(string name, int nullValue)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            return GetInt32(DataReader.GetOrdinal(name), nullValue);
        }

        public virtual int GetInt32(int index, int nullValue)
        {
            return DataReader.IsDBNull(index) ? nullValue : Convert.ToInt32(DataReader.GetValue(index));
        }

        public virtual int? GetInt32Nullable(string name)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            return GetInt32Nullable(DataReader.GetOrdinal(name));
        }

        public virtual int? GetInt32Nullable(int index)
        {
            if (DataReader.IsDBNull(index))
            {
                return null;
            }

            return GetInt32(index);
        }

        public virtual long GetInt64(string name)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            return GetInt64(DataReader.GetOrdinal(name));
        }

        public virtual long GetInt64(string name, long nullValue)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            return GetInt64(DataReader.GetOrdinal(name), nullValue);
        }

        public virtual long GetInt64(int index, long nullValue)
        {
            return DataReader.IsDBNull(index) ? nullValue : Convert.ToInt64(DataReader.GetValue(index));
        }

        public virtual long? GetInt64Nullable(string name)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            return GetInt64Nullable(DataReader.GetOrdinal(name));
        }

        public virtual long? GetInt64Nullable(int index)
        {
            if (DataReader.IsDBNull(index))
            {
                return null;
            }

            return GetInt64(index);
        }

        public virtual string GetString(string name)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            return GetString(DataReader.GetOrdinal(name));
        }

        public virtual string GetString(string name, string nullValue)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            if (nullValue == null)
            {
                throw new ArgumentNullException(nameof(nullValue));
            }

            return GetString(DataReader.GetOrdinal(name), nullValue);
        }

        public virtual string GetString(int index, string nullValue)
        {
            if (nullValue == null)
            {
                throw new ArgumentNullException(nameof(nullValue));
            }

            return DataReader.IsDBNull(index) ? nullValue : DataReader.GetString(index);
        }

        public virtual string GetStringNullable(string name)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            return GetStringNullable(DataReader.GetOrdinal(name));
        }

        public virtual string GetStringNullable(int index)
        {
            return DataReader.IsDBNull(index) ? null : DataReader.GetString(index);
        }

        public virtual object GetValue(string name)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            return GetValue(DataReader.GetOrdinal(name));
        }

        public object GetValue(string name, object nullValue)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            return GetValue(DataReader.GetOrdinal(name), nullValue);
        }

        public virtual object GetValue(int index, object nullValue)
        {
            return DataReader.IsDBNull(index) ? nullValue : DataReader.GetValue(index);
        }

        public virtual bool HasColumn(string columnName)
        {
            for (var i = 0; i < DataReader.FieldCount; i++)
            {
                if (DataReader.GetName(i).Equals(columnName, StringComparison.InvariantCultureIgnoreCase))
                {
                    return true;
                }
            }
            return false;
        }

        public virtual bool HasRows()
        {
            var dr = DataReader as DbDataReader;

            if (dr == null)
            {
                throw new ApplicationException(
                    "Could not determine if the SafeDataReader HasRows because the SafeDataReader could not be cast to a DbDataReader.");
            }

            return dr.HasRows;
        }

        public virtual DataTable ToDataTable()
        {
            var dt = new DataTable();
            for (var i = 0; i > DataReader.FieldCount; ++i)
            {
                dt.Columns.Add(
                    new DataColumn {ColumnName = DataReader.GetName(i), DataType = DataReader.GetFieldType(i)});
            }
            while (DataReader.Read())
            {
                var row = dt.NewRow();
                DataReader.GetValues(row.ItemArray);
                dt.Rows.Add(row);
            }

            return dt;
        }

        #endregion

        #region Private Methods

        /// <summary>
        ///     Disposes the object.
        /// </summary>
        /// <param name="disposing"> True if called by the public Dispose method. </param>
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposedValue)
            {
                if (disposing)
                {
                    // free unmanaged resources when explicitly called

                    DataReader.Dispose();
                }

                // free shared unmanaged resources
            }

            _disposedValue = true;
        }

        #endregion

        ~SafeDataReader()
        {
            Dispose(false);
        }
    }
}