﻿using System;
using System.Data;
using System.Data.Common;
using AS.Crossbow.Core.Common;
using AS.Crossbow.Core.Helpers;
using AS.Crossbow.Core.Helpers.Interfaces;

namespace AS.Crossbow.Core.Data
{
    public abstract class BaseRepository
    {
        #region Member Variables

        private readonly IRepositoryExceptionHelper _repositoryExceptionHelper = new RepositoryExceptionHelper();

        #endregion

        #region Private Methods

        private void EnsureCommandHasTransaction(IDbCommand command)
        {
            if (command == null)
            {
                throw new ArgumentNullException(nameof(command));
            }

            if (command.Transaction == null)
            {
                throw new MissingTransactionException();
            }
        }

        protected virtual int ExecuteNonQuery(IDbCommand command, int commandTimeout = 30)
        {
            if (command == null)
            {
                throw new ArgumentNullException(nameof(command));
            }

            EnsureCommandHasTransaction(command);

            try
            {
                command.CommandTimeout = commandTimeout;

                return command.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                throw _repositoryExceptionHelper.CreateApplicationDbException(ex, command);
            }
        }

        protected virtual IDataReader ExecuteReader(
            IDbCommand command,
            CommandBehavior commandBehavior = CommandBehavior.Default,
            int commandTimeout = 30)
        {
            if (command == null)
            {
                throw new ArgumentNullException(nameof(command));
            }

            EnsureCommandHasTransaction(command);

            try
            {
                command.CommandTimeout = commandTimeout;

                return command.ExecuteReader(commandBehavior);
            }
            catch (DbException ex)
            {
                throw _repositoryExceptionHelper.CreateApplicationDbException(ex, command);
            }
        }

        protected virtual SafeDataReader ExecuteSafeDataReader(
            IDbCommand command,
            CommandBehavior commandBehavior = CommandBehavior.Default,
            int commandTimeout = 30)
        {
            return new SafeDataReader(ExecuteReader(command, commandBehavior, commandTimeout));
        }

        protected virtual object ExecuteScalar(IDbCommand command, int commandTimeout = 30)
        {
            if (command == null)
            {
                throw new ArgumentNullException(nameof(command));
            }

            EnsureCommandHasTransaction(command);

            try
            {
                command.CommandTimeout = commandTimeout;

                return command.ExecuteScalar();
            }
            catch (DbException ex)
            {
                throw _repositoryExceptionHelper.CreateApplicationDbException(ex, command);
            }
        }

        protected virtual void FillDataSet(DbDataAdapter adapter, DataSet ds, int commandTimeout = 30)
        {
            if (adapter == null)
            {
                throw new ArgumentNullException(nameof(adapter));
            }

            if (ds == null)
            {
                throw new ArgumentNullException(nameof(ds));
            }

            if (adapter.SelectCommand == null)
            {
                throw new ArgumentException("Select Command is missing or null.", nameof(adapter));
            }

            EnsureCommandHasTransaction(adapter.SelectCommand);

            try
            {
                if (adapter.SelectCommand != null)
                {
                    adapter.SelectCommand.CommandTimeout = commandTimeout;
                }

                adapter.Fill(ds);
            }
            catch (DbException ex)
            {
                throw _repositoryExceptionHelper.CreateApplicationDbException(ex, adapter.SelectCommand);
            }
        }

        protected virtual void FillDataTable(DbDataAdapter adapter, DataTable dt, int commandTimeout = 30)
        {
            if (adapter == null)
            {
                throw new ArgumentNullException(nameof(adapter));
            }

            if (dt == null)
            {
                throw new ArgumentNullException(nameof(dt));
            }

            if (adapter.SelectCommand == null)
            {
                throw new ArgumentException("Select Command is missing or null.", nameof(adapter));
            }

            EnsureCommandHasTransaction(adapter.SelectCommand);

            try
            {
                if (adapter.SelectCommand != null)
                {
                    adapter.SelectCommand.CommandTimeout = commandTimeout;
                }

                adapter.Fill(dt);
            }
            catch (DbException ex)
            {
                throw _repositoryExceptionHelper.CreateApplicationDbException(ex, adapter.SelectCommand);
            }
        }

        protected int UpdateDataSet(DbDataAdapter adapter, DataSet ds, int updateBatchSize = 1, int commandTimeout = 30)
        {
            if (adapter == null)
            {
                throw new ArgumentNullException(nameof(adapter));
            }
            if (ds == null)
            {
                throw new ArgumentNullException(nameof(ds));
            }

            if (adapter.UpdateCommand == null)
            {
                throw new ArgumentException("Update Command is missing or null.", nameof(adapter));
            }

            EnsureCommandHasTransaction(adapter.UpdateCommand);

            try
            {
                if (adapter.UpdateCommand != null)
                {
                    adapter.UpdateBatchSize = updateBatchSize;
                    adapter.UpdateCommand.CommandTimeout = commandTimeout;
                }

                return adapter.Update(ds);
            }
            catch (DbException ex)
            {
                throw _repositoryExceptionHelper.CreateApplicationDbException(ex, adapter.UpdateCommand);
            }
        }

        protected int UpdateDataTable(
            DbDataAdapter adapter,
            DataTable dt,
            int updateBatchSize = 1,
            int commandTimeout = 30)
        {
            if (adapter == null)
            {
                throw new ArgumentNullException(nameof(adapter));
            }

            if (dt == null)
            {
                throw new ArgumentNullException(nameof(dt));
            }

            if (adapter.UpdateCommand == null)
            {
                throw new ArgumentException("Update Command is missing or null.", nameof(adapter));
            }

            EnsureCommandHasTransaction(adapter.UpdateCommand);

            try
            {
                if (adapter.UpdateCommand != null)
                {
                    adapter.UpdateBatchSize = updateBatchSize;
                    adapter.UpdateCommand.CommandTimeout = commandTimeout;
                }

                return adapter.Update(dt);
            }
            catch (DbException ex)
            {
                throw _repositoryExceptionHelper.CreateApplicationDbException(ex, adapter.UpdateCommand);
            }
        }

        #endregion
    }
}