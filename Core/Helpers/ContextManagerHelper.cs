﻿using System.Runtime.Remoting.Messaging;
using System.Web;

namespace AS.Crossbow.Core.Helpers
{
    internal static class ContextManagerHelper
    {
        #region Constants

        private const string DB_ERROR_KEY = "DbErrorMessage";

        #endregion

        #region Properties

        public static string ContextDbErrorMessage
        {
            get
            {
                if (IsInWebContext())
                {
                    return HttpContext.Current.Items[DB_ERROR_KEY] == null
                               ? string.Empty
                               : HttpContext.Current.Items[DB_ERROR_KEY].ToString();
                }

                return CallContext.GetData(DB_ERROR_KEY) == null
                           ? string.Empty
                           : CallContext.GetData(DB_ERROR_KEY).ToString();
            }
            set
            {
                if (IsInWebContext())
                {
                    HttpContext.Current.Items[DB_ERROR_KEY] = value;
                }
                else
                {
                    CallContext.SetData(DB_ERROR_KEY, value);
                }
            }
        }

        #endregion

        #region Private Methods

        private static bool IsInWebContext()
        {
            return HttpContext.Current != null;
        }

        #endregion
    }
}