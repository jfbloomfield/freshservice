﻿using System;
using System.Configuration;
using AS.Crossbow.Core.Helpers.Interfaces;

namespace AS.Crossbow.Core.Helpers
{
    public class ConfigurationHelper : IConfigurationHelper
    {
        #region IConfigurationHelper Members

        public string GetAppSetting(string key, string defaultValue)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }

            var value = GetAppSettingImpl(key);

            if (string.IsNullOrEmpty(value))
            {
                value = defaultValue;
            }

            return value;
        }

        public string GetAppSetting(string key)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }

            return GetAppSettingImpl(key);
        }

        #endregion

        #region Private Methods

        private string GetAppSettingImpl(string key)
        {
            string value;
            try
            {
                value = ConfigurationManager.AppSettings[key];
            }
            catch
            {
                value = string.Empty;
            }

            return value;
        }

        #endregion
    }
}