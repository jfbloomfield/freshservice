﻿using System;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using AS.Crossbow.Core.Common;
using AS.Crossbow.Core.Helpers.Interfaces;

namespace AS.Crossbow.Core.Helpers
{
    public class RepositoryExceptionHelper : IRepositoryExceptionHelper
    {
        #region IRepositoryExceptionHelper Members

        public virtual ApplicationDbException CreateApplicationDbException(DbException ex, IDbCommand command)
        {
            if (ex == null)
            {
                throw new ArgumentNullException(nameof(ex));
            }
            if (command == null)
            {
                throw new ArgumentNullException(nameof(command));
            }
            var str = new StringBuilder();

            str.AppendFormat(Environment.NewLine);
            str.AppendFormat("Exception: {0}{1}{1}", ex.Message, Environment.NewLine);
            str.AppendFormat("User: {0}{1}{1}", Thread.CurrentPrincipal.Identity.Name, Environment.NewLine);

            //  Original Query
            str.AppendFormat("Sql: {0}{1}{1}", command.CommandText, Environment.NewLine);

            //  Parameters
            //  ----------
            var paramString = string.Join(
                ", ",
                command.Parameters.Cast<DbParameter>()
                    .Select(param => $"{param.ParameterName}='{param.Value}'")
                    .ToArray());

            str.AppendFormat("Parameters: {0}{1}{1}", paramString, Environment.NewLine);

            //  Concatenated Query
            //  ------------------
            var query = command.CommandText;
            foreach (DbParameter parameter in command.Parameters)
            {
                if (parameter.DbType == DbType.String || parameter.DbType == DbType.DateTime)
                {
                    query = Regex.Replace(query, "\\b" + parameter.ParameterName + "\\b", "'" + parameter.Value + "'");
                }
                else
                {
                    query = Regex.Replace(query, "\\b" + parameter.ParameterName + "\\b", parameter.Value.ToString());
                }
            }

            query = query.Replace("@", string.Empty);

            str.AppendFormat("Concatenated Query: {0}{1}{1}", query, Environment.NewLine);

            var message = str.ToString();

            Console.WriteLine(message);

            return new ApplicationDbException(message, ex);
        }

        #endregion
    }
}