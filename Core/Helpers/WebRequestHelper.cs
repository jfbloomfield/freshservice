﻿using System;
using System.Net;
using AS.Crossbow.Core.Helpers.Interfaces;

namespace AS.Crossbow.Core.Helpers
{
    public class WebRequestHelper : IWebRequestHelper
    {
        #region IWebRequestHelper Members

        public WebRequest CreateWebRequest(string requestUriString)
        {
            if (requestUriString == null)
            {
                throw new ArgumentNullException(nameof(requestUriString));
            }

            return WebRequest.Create(requestUriString);
        }

        public WebRequest CreateWebRequest(Uri requestUri)
        {
            if (requestUri == null)
            {
                throw new ArgumentNullException(nameof(requestUri));
            }

            return WebRequest.Create(requestUri);
        }

        #endregion
    }
}