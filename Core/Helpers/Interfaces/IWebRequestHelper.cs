﻿using System;
using System.Net;

namespace AS.Crossbow.Core.Helpers.Interfaces
{
    public interface IWebRequestHelper
    {
        #region Public Methods

        WebRequest CreateWebRequest(string requestUriString);
        WebRequest CreateWebRequest(Uri requestUri);

        #endregion
    }
}