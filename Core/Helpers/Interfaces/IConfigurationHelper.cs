﻿namespace AS.Crossbow.Core.Helpers.Interfaces
{
    public interface IConfigurationHelper
    {
        #region Public Methods

        string GetAppSetting(string key);
        string GetAppSetting(string key, string defaultValue);

        #endregion
    }
}