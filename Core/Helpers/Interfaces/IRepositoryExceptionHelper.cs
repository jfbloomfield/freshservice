using System.Data;
using System.Data.Common;
using AS.Crossbow.Core.Common;

namespace AS.Crossbow.Core.Helpers.Interfaces
{
    public interface IRepositoryExceptionHelper
    {
        #region Public Methods

        ApplicationDbException CreateApplicationDbException(DbException ex, IDbCommand command);

        #endregion
    }
}