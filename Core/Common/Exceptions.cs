﻿using System;
using System.Data.Common;

namespace AS.Crossbow.Core.Common
{
    public class ApplicationDbException : DbException
    {
        #region Constructors

        public ApplicationDbException(string message, Exception innerException) : base(message, innerException)
        {
        }

        #endregion
    }

    public class MissingTransactionException : DbException
    {
        #region Constants

        private const string MESSAGE =
            "A transaction was not associated with the command being executed.  Transactions are required.  Please attached a transaction to your command prior to executing it.";

        #endregion

        #region Constructors

        public MissingTransactionException() : base(MESSAGE)
        {
        }

        #endregion
    }

    public class JavaScriptException : Exception
    {
        #region Constructors

        public JavaScriptException(string message) : base(message)
        {
        }

        #endregion
    }

    [Serializable]
    public class MissingAppSettingParameterException : Exception
    {
        #region Constructors

        public MissingAppSettingParameterException(string message) : base(message)
        {
        }

        #endregion
    }

    [Serializable]
    public class InvalidAppSettingParameterException : Exception
    {
        #region Constructors

        public InvalidAppSettingParameterException(string message) : base(message)
        {
        }

        #endregion
    }
}