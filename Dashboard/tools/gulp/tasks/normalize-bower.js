'use strict';

var gulp = require('gulp');
var conf = require('../config').bower;
var bowerMain = require("main-bower-files");
var bowerNormalizer = require("gulp-bower-normalize");

gulp.task('normalize-bower', function () {
    return gulp.src(bowerMain(), { base: conf.bowerComponents })
        .pipe(bowerNormalizer({ bowerJson: conf.config }))
        .pipe(gulp.dest(conf.bowerDest));
});

