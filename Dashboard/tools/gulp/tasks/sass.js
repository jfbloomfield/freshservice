'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var autoPrefixer = require("gulp-autoprefixer");
var sourcemaps = require("gulp-sourcemaps");
var globbing = require('gulp-css-globbing');
var config = require('../config').sass;

gulp.task('sass', function () {
  return gulp.src(config.source)
    .pipe(globbing({
      extensions: ['.scss']
    }))
    .pipe(sass({
        outputStyle: "expanded",
        omitSourceMapUrl: true
    }).on('error', sass.logError))
    .pipe(autoPrefixer("last 2 version", "ie 8", "ie 9"))
    .pipe(sourcemaps.write("./"))
    .pipe(gulp.dest(config.dest));
});
