var gulp = require('gulp');
var del = require('del');
var runSequence = require('run-sequence');

gulp.task('bundle',function(callback) {
    runSequence('delete',
        'production-mode',
        'build',
        'jspm',
        'index-bundle',
        'fonts',
        'css',
        callback);
});



