var gulp = require('gulp');
var config = require('../config').fonts;

gulp.task('fonts', function () {
    gulp.src(config.fonts)
        .pipe(gulp.dest(config.dest));
});