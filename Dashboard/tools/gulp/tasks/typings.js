'use strict';

var gulp = require('gulp');
var conf = require('../config').typings;
var typescript = require('gulp-typescript');
var gulpTypings = require("gulp-typings");
var del = require("del");

gulp.task('typings', function () {
    return gulp.src(conf.config)
        .pipe(gulpTypings());
});
