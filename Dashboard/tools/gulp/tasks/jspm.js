var gulp = require('gulp');
var Builder = require('systemjs-builder');
var concat = require('gulp-concat');
var config = require('../config').jspm;

gulp.task('jspm', ['jspm-globals'], function () {
    var builder = new Builder();
    return builder.loadConfig('./config.js')
        .then(function () {
            return builder
                .bundle(config.main,
                config.outFile,
                {
                    normalize: true,
                    minify: true,
                    mangle: true,
                    globalDefs: {DEBUG: false}
                });
        })
        .catch(function (ex) {
            console.log('error', ex);
        });
});

gulp.task('jspm-globals', function () {
    return gulp.src(config.globals)
        .pipe(concat('globals.js'))
        .pipe(gulp.dest('dist/scripts'));
});