'use strict';

var gulp = require('gulp');
var conf = require('../config').typescript;
var typescript = require('gulp-typescript');
var sourcemaps = require('gulp-sourcemaps');
var tslint = require('gulp-tslint');
var cache = require('gulp-cached');
var tsProject = typescript.createProject('tsconfig.json',{sortOutput:true});

gulp.task('typescript', function () {
  return tsProject.src()
    .pipe(cache('typescript'))
    .pipe(sourcemaps.init())
    // .pipe(tslint()) //For the sake of build times
    // .pipe(tslint.report('prose', {emitError: false}))
    .pipe(typescript(tsProject))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(conf.dest));
});


