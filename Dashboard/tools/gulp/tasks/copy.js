var gulp = require('gulp');
var replace = require('gulp-replace');
var conf = require('../config').copy;

/**
 * Copy assets, html, jspm config & index.html from app directory to development directory
 */
gulp.task('copy', function () {
  gulp
    .src([conf.assets])
    .pipe(gulp.dest(conf.dest + '/assets'));

  gulp
    .src(conf.vendorFiles)
    .pipe(gulp.dest(conf.dest + '/vendor'));

  return gulp
    .src([conf.js])
    .pipe(replace('"github:*": "target/program/jspm/github/*"', '"github:*": "jspm/github/*"'))
    .pipe(replace('"npm:*": "target/program/jspm/npm/*"', '"npm:*": "jspm/npm/*"'))
    .pipe(gulp.dest(conf.dest));
});
