var gulp = require('gulp');
var runSequence = require('run-sequence');

/**
 * Run all tasks needed for a build in defined order
 */
gulp.task('build', function (callback) {
  runSequence('delete',
    ['bower','typings'],
    ['normalize-bower'],
    'copy',
    'html',
    'typescript',
    ['templates','sass'],
    callback);
});
