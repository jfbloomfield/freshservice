var gulp = require('gulp');
var templateCache = require('gulp-angular-templatecache');
var insert = require('gulp-insert');
var conf = require('../config').html;

gulp.task('templates', function () {
  return gulp.src(conf.source)
    .pipe(templateCache({root: '/src/', moduleSystem: 'system', standalone: true}))
    //need to add an angular require to keep karma happy!
    .pipe(insert.prepend('var angular = require(\'angular\');'))
    .pipe(gulp.dest(conf.dest));
});
