var gulp = require('gulp');
var concat = require('gulp-concat');
var autoPrefixer = require("gulp-autoprefixer");
var minifyCSS = require('gulp-minify-css');
var order = require('gulp-order');
var es = require('event-stream');
var replace = require('gulp-replace');
var config = require('../config').css;

gulp.task('css', function () {
    var bowerCss = gulp.src(config.bowerCss)
            .pipe(replace(/url\((')?\.\.\/fonts\//g, 'url($1fonts/'))
            .pipe(concat('bower.css')),
        appCss = gulp.src(config.appCss)
            .pipe(autoPrefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9'))
            .pipe(minifyCSS())
            .pipe(concat('app.css')),
        combinedCss = es.concat(bowerCss, appCss)
            .pipe(order(['bower.css', 'app.css']))
            .pipe(concat('css.css'));
    return combinedCss.pipe(gulp.dest(config.dest));
});