var gulp = require('gulp');
var replace = require('gulp-replace');
var config = require('../config').productionMode;

gulp.task('production-mode', function () {
    return gulp.src(config.main)
        .pipe(replace('\/\/enableProdMode();','enableProdMode();'))
        .pipe(gulp.dest(config.dest));
});