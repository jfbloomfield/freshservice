var gulp = require('gulp');
var htmlreplace = require('gulp-html-replace');
var config = require('../config').html;

gulp.task('index-bundle', function () {
    var timestamp = Date.now();

    return gulp.src(config.mvcView)
        .pipe(htmlreplace({
            'css': `dist/css/css.css?_p=${timestamp}`,
            'js': [
                `dist/scripts/app-bundle.js?_p=${timestamp}`
            ],
            'globals':`dist/scripts/globals.js?_p=${timestamp}`
        }))
        .pipe(gulp.dest(config.mvcFolder));
});