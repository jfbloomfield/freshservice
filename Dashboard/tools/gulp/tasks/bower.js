'use strict';

var gulp = require('gulp');
var bower = require("gulp-bower");
var conf = require('../config').bower;

gulp.task('bower', function () {
    return bower({cwd: conf.cwd});
});

