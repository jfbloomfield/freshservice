var programApp = 'app';
var programDist = 'dist';


module.exports = {
    production: programDist,
    bower: {
        bowerComponents: "bower_components",
        bowerDest: "ClientLib",
        config: "bower.json",
        cwd: process.cwd()
    },
    copy: {
        assets: programApp + '/assets/**/*.*',
        js: programApp + '/**/*.js',
        vendorFiles: ['node_modules/angular2/bundles/angular2-polyfills.min.js'],
        dest: programDist
    },
    css: {
        bowerCss: [
            'ClientLib/components-font-awesome/css/font-awesome.css',
            'ClientLib/selectize/css/selectize.css'
        ],
        appCss: [programDist +'/'+programApp+'/**/*.css'],
        dest: programDist +'/css'
    },
    delete: {
        program: ['target/program/**/*', '!target/program/jspm/**']
    },
    fonts: {
        fonts: ['Content/fonts/**/*'],
        dest: programDist + '/css/fonts'

    },
    html: {
        source: programApp + '/**/*.html',
        dest: programDist + '/' + programApp,
        mvcFolder: 'Views/EntryPages',
        mvcView: ['Views/EntryPages/program.cshtml','Views/EntryPages/index.cshtml']

    },
    images: {
        source: programApp + '/assets/**/*.*',
        dest: programDist + '/assets'
    },
    jspm: {
        main: programDist+'/'+programApp+'/main.js',
        outFile: programDist+'/scripts/app-bundle.js',
        globals: ["node_modules/es6-shim/es6-shim.min.js",
            "node_modules/systemjs/dist/system-polyfills.js",
            "node_modules/angular2/es6/dev/src/testing/shims_for_IE.js",
            "node_modules/angular2/bundles/angular2-polyfills.js",
            "node_modules/systemjs/dist/system.src.js",
            "node_modules/rxjs/bundles/Rx.js",
            "node_modules/angular2/bundles/angular2.dev.js",
            "node_modules/lodash/lodash.js",
        ]

    },
    productionMode: {
        main: programApp + '/main.ts',
        dest: programApp

    },
    sass: {
        main: programApp + '/scss/app.scss',
        source: [programApp + '/**/*.scss',],
        dest: programDist + '/' + programApp

    },
    scripts: {
        source: programDist + '/program.js',
        dest: programDist
    },
    typescript: {

        scripts: programApp + '/**/*.ts',
        dest: programDist

    },
    typings: {
        config: 'typings.json',
    },
    watch: {
        html: programApp + '/**/*.html',
        scripts: programApp + '/**/*.ts',
        sass: programApp + '/**/*.scss',
        assets: programApp + '/assets/**/*.*'
    }
};
