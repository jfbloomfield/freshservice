﻿var sessionExpirationPollingSeconds = 5;
var countdownSeconds = 300;
var extendSessionModal;
var sessionExpirationIntervalId;
var displayCountdownIntervalId;
var count = countdownSeconds;
var extendSessionUrl = window.location.href;
var timeoutSessionUrl = "/AIMSNET/Security/Timeout.aspx";
var logoutSessionUrl = "/AIMSNET/Security/Logout.aspx";
var keepAliveUrl = "/AIMSNET/KeepAlive.aspx?trnd=0&updateassession=1";

function isInFrame() {
    return (top != self);
}

$(document).ready(function() {
    if (!isInFrame() && $("#sm-countdown-modal").length) {
        initializeModal();

        sessionExpirationIntervalId = window.setInterval(function() {
            if (!isSessionTimedOut()) {
                var shouldDisplayModal = ((getTimeoutMilliSeconds() - (countdownSeconds * 1000)) <= 0);

                if (isSessionModalOpenAlready()) {
                    if (!shouldDisplayModal) {
                        window.clearInterval(displayCountdownIntervalId);
                        extendSessionModal.modal("hide");
                    }
                } else {
                    if (shouldDisplayModal) {
                        promptToExtendSession();
                    }
                }
            }
        }, (sessionExpirationPollingSeconds * 1000));
    }

    $(".DisableOnSubmit").click(function() {
        var buttonId = $(this).attr("id");

        setTimeout(function() {

            if (typeof Page_ClientValidate == "function") {
                if (Page_IsValid) {
                    $("#" + buttonId).attr("disabled", "true");
                }
            } else {
                $("#" + buttonId).attr("disabled", "true");
            }
        }, 50);
    });
});

function isSessionModalOpenAlready() {
    return extendSessionModal.hasClass("in");
}

function isSessionTimedOut() {
    return ($.cookie("UI") == undefined);
}

function getTimeoutMilliSeconds() {
    var currentDate = new Date();
    var timeoutDate = new Date(parseInt($.cookie("TO")));
    return (timeoutDate - currentDate);
}

function displayCountdown() {
    countdown();
    displayCountdownIntervalId = window.setInterval(countdown, 1000);
}

function countdown() {
    var cd = new Date(count * 1000);
    var minutes = cd.getUTCMinutes();
    var seconds = cd.getUTCSeconds();
    var minutesDisplay = minutes === 1 ? "1 minute " : minutes === 0 ? "" : minutes + " minutes ";
    var secondsDisplay = seconds === 1 ? "1 second" : seconds + " seconds";
    var cdDisplay = minutesDisplay + secondsDisplay;

    $("#sm-countdown").html(cdDisplay);
    if (count === 0) {
        timeoutSession();
    }

    count--;
}

function initializeModal() {
    extendSessionModal = $("#sm-countdown-modal");

    $("#sessionTimeoutModalContinueButton").click(function() {
        extendSessionModal.modal("hide");

        window.clearInterval(displayCountdownIntervalId);

        $.get(replaceQueryStringParameter(keepAliveUrl, "trnd", new Date().getTime()), function (data) {
            if (data.toLowerCase().indexOf("keepalive.aspx") === -1) {
                alertAndLogoutSession();
            }
        })
            .fail(function () {
                alertAndLogoutSession();
            });
    });

    $("#sessionTimeoutModalLogOutButton").click(function() {
        extendSessionModal.modal("hide");
        logoutSession();
    });
}

function alertAndLogoutSession() {
    alert("Your session has expired. You will need to re-authenticate in order to continue.");
    logoutSession();
}

function promptToExtendSession() {
    this.title = this.title + " ";
    window.focus();
    extendSessionModal.modal("show");
    count = countdownSeconds;
    displayCountdown();
}

function timeoutSession() {
    extendSessionModal.modal("hide");
    location.href = timeoutSessionUrl;
}

function logoutSession() {
    extendSessionModal.modal("hide");
    location.href = logoutSessionUrl;
}

function refreshSession() {
    window.clearInterval(displayCountdownIntervalId);

    var img = new Image(1, 1);
    img.src = replaceQueryStringParameter(keepAliveUrl, "trnd", new Date().getTime());
}

function replaceQueryStringParameter(uri, key, value) {
    var re = new RegExp("([?|&])" + key + "=.*?(&|$)", "i");
    var separator = uri.indexOf("?") !== -1 ? "&" : "?";
    if (uri.match(re)) {
        return uri.replace(re, "$1" + key + "=" + value + "$2");
    } else {
        return uri + separator + key + "=" + value;
    }
}

//Each page should add the "formDirtyChecker" class to the form if it wants
//to have the confirmation before leaving the page
//This function leverages the jquery.dirtyform.js plug-in
//http://putspost.blogspot.com/2009/01/dirtyform.html

function AddDirtyFormHandler() {
    $(".tagDirtyChecker")
        .dirty_form({
            addClassOn: function() {
                //This function will also remove the class, so only
                //pass it back when we have not already set it.
                if ($("#divManuallySetDirtyFlag.changed").length == 0) {
                    return $("#divManuallySetDirtyFlag");
                }
            }
        })
        .dirty(function(event, data) {
            window.onbeforeunload = confirmUnsavedChanges;
        });
    $(".bypassDirtyCheckButton").click(function() {
        $(this).addClass("doNotPrompt");
    });
};