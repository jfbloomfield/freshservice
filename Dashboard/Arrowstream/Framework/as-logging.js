﻿function logError(msg) {
    if (logErrorUrl == null) {
        alert("logErrorUrl must be defined.");
        return;
    }

    $.ajax({
        type: "POST",
        url: logErrorUrl,
        data: { message: msg }
    });
}

window.onerror = function(msg, url, line) {
    logError("Message:" + msg + "\n" +
        "Url: " + url + "\n" +
        "Line: " + line);
};