﻿module ArrowStream {

    export interface IMicroStrategyClientParameter {
        ElementId: string;
        Value: number;
        Text: string;
    }

    export interface IWindowOpener {
        addHash(url: string): string;
        getHost(): string;
        microStrategyPopup(pageUrl: string): void;
        //microStrategyPopupWithParams(key: string, parameters: IMicroStrategyClientParameter[], logger: Logger.ILogger): void;
        openCenteredWindow(url: string, name: string, width: number, height: number): any;
        openMaximizedWindow(url: string, name: string): any;
        openWindow(url: string, name: string, left: number, top: number, width: number, height: number): any;
        statusBarHeight(): number;
        winOpen(sURL: string, sName: string, iW: number, iH: number, iScrollbar?: number): void;
    }

    export class WindowOpener implements IWindowOpener {

        statusBarHeight(): number {
            if (navigator.userAgent.indexOf("IE 7.0") !== -1)
                return 32;
            else if ((navigator.appCodeName !== "Mozilla") && (navigator.appMinorVersion.indexOf("SP2") !== -1) &&
            (navigator.userAgent.indexOf("Windows NT 5.1") !== -1))
                return 32;
            return 0;
        }

        winOpen(sURL: string, sName: string, iW: number, iH: number, iScrollbar?: number): void {
            var sParms: string;
            var iTop: number;
            var iLeft: number;
            var bNeedScroll = false;

            iTop = (screen.availHeight - (32 + this.statusBarHeight()) - iH) / 2;
            iLeft = (screen.availWidth - iW - 9) / 2;
            if (iTop < 0) iTop = 0;
            if (iLeft < 0) iLeft = 0;
            sParms = `width=${iW},height=${iH},top=${iTop},left=${iLeft}`;
            if (iScrollbar === 100)
                sParms += ",resizable=1,scrollbars=1";
            else {
                if (iScrollbar === 99) sParms += ",resizable=1";
                if (iScrollbar === 1 || bNeedScroll) sParms += ",scrollbars=1";
            }

            var wWin = window.open(this.addHash(sURL), sName, sParms);
            if (wWin) wWin.focus();
            self.setTimeout(() => {
                wWin.focus();
            }, 500);
        }

        addHash(url: string): string {
            var arr = url.split("?");

            var domain = location.protocol + "//";
            if (document.domain === "localhost") {
                domain += "dev.arrowstream.com";
            } else {
                domain += document.domain;
            }

            var completeUrl = domain + "/hash_winopen.asp" + ((arr.length === 2) ? `?${arr[1]}&` : "?") + "xscr=" + arr[0];

            var returnUrl: string = null;

            $.ajax({
                type: "POST",
                url: completeUrl,
                success: function(result) {
                    returnUrl = url + ((arr.length === 2) ? "&" : "?") + "hash=" + result;
                },
                error: function(xhr) {
                    alert(`An error occurred: ${xhr.status} ${xhr.statusText}`);
                },
                async: false
            });

            return returnUrl;
        }

        getHost(): string {
            //fullpath
            //var host = window.location.protocol + "://" + window.location.host + "/" + window.location.pathname;
            //host

            var host = window.location.host;
            var hostToReturn = window.location.protocol + "//" + host + "/";

            return hostToReturn;
        }

        openWindow(url: string, name: string, left: number, top: number, width: number, height: number): any {
            var windowFeatures = `width=${width},height=${height},status,resizable,left=${left},top=${top}screenX=${left},screenY=${top},scrollbars=1`;

            var myWindow = window.open(url, name, windowFeatures);

            if (myWindow) {
                myWindow.focus();
                myWindow.resizeTo(width, height);
                myWindow.moveTo(left, top);
            }

            return myWindow;
        }

        openCenteredWindow(url: string, name: string, width: number, height: number): any {
            var top = parseInt(((screen.availHeight / 2) - (height / 2)) + "");
            var left = parseInt(((screen.availWidth / 2) - (width / 2)) + "");

            return this.openWindow(url, name, left, top, width, height);
        }

        openMaximizedWindow(url: string, name: string): any {
            return this.openCenteredWindow(url, name, screen.availWidth * .95, screen.availHeight * .95);
        }

        microStrategyPopup(pageUrl: string): void {
            var targetUrl = window.location.protocol + "//" + window.location.host + "/aimsnet/" + pageUrl;
            var win = this.openMaximizedWindow(targetUrl, "reuse"); //, (screen.height === 600) ? 780 : 960, (screen.height === 600) ? 520 : 650);
            if (win) {
                setTimeout(() => { win.focus(); }, 400);
            }
        }

        //microStrategyPopupWithParams(key: string, parameters: IMicroStrategyClientParameter[], logger: Logger.ILogger): void {
        //    var pageUrl = "MicroStrategyPopup.aspx?Key=" + key;
        //    if (parameters == null || parameters.length === 0) {
        //        this.microStrategyPopup(pageUrl);
        //    } else {
        //        $.ajax({
        //            type: "POST",
        //            url: "/ondemand/LandingPage/SetMicroStrategyParameters",
        //            dataType: "application/json",
        //            data: { parameters: parameters },
        //            success: (data: any, textStatus: string, jqXHR: JQueryXHR) => {
        //                this.microStrategyPopup(pageUrl);
        //            },
        //            error: (jqXHR: JQueryXHR, textStatus: string, errorThrown: string) => {
        //                logger.error("Failed to set Microstrategy parameters on server", jqXHR.status + " " + jqXHR.statusText);
        //            }
        //        });
        //    }
        //}
    }
}