﻿$(document).ready(function () {
    addHandlers();
});

function getNewGuid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) { var r = Math.random() * 16 | 0, v = c == 'x' ? r : r & 0x3 | 0x8; return v.toString(16); });
}

String.prototype.endsWith = function (pattern) {
    var d = this.length - pattern.length;
    return d >= 0 && this.lastIndexOf(pattern) === d;
};

//############################################################################
//Replaced ajaxSetup with ajaxStart
//ajaxStart will run a single time for all ajax requests
// as opposed to for each one
//Here is more info:  http://onegoodexample.wordpress.com/tag/beforesend/
//Seems to be working
//############################################################################
function implementWaitScreenForAllAjaxCalls(waitElement) {
    $(document).ajaxStart(function() {
        waitElement.show();
    }).ajaxStop(function() {
        waitElement.hide();
    });
}


function asConfirm(message, callback) {
    return bootbox.confirm(message, callback).find('.modal-body').addClass('as-modal-header');
}

function asAlert(message) {
    return bootbox.alert(message).find('.modal-body').addClass('as-modal-header');
}

function asAlert(message, callback) {
    return bootbox.alert(message, callback).find('.modal-body').addClass('as-modal-header');
}

function addHandlers() {
    addFilterInputs();
}

function addFilterInputs() {
    //requires jquery.formatCurrency.js.  We do not use now, but wanted to leave here for reference as it may come up

    handleCurrencyFormats();
    handlePercentageFormat();
    handleNumericFormat();
    handleIntegerFormat();
    handleNoThousandsSeperator();
    handleRawIntegerFormat();
}

function handleCurrencyFormats(div) {
    var string = 'input[class*=currency]:text';
    $(string).filter_input({ regex: '[0-9$,.-]' });
    $(string).blur(function () {
        if ($(this).hasClass('currency')) {
            $(this).formatCurrency({ colorize: true, negativeFormat: '-%s%n', roundToDecimalPlace: 2 });
        } else if ($(this).hasClass('currency_0_decimal')) {
            $(this).formatCurrency({ colorize: true, negativeFormat: '-%s%n', roundToDecimalPlace: 0 });
        } else if ($(this).hasClass('currency_1_decimal')) {
            $(this).formatCurrency({ colorize: true, negativeFormat: '-%s%n', roundToDecimalPlace: 1 });
        } else if ($(this).hasClass('currency_2_decimal')) {
            $(this).formatCurrency({ colorize: true, negativeFormat: '-%s%n', roundToDecimalPlace: 2 });
        } else if ($(this).hasClass('currency_3_decimal')) {
            $(this).formatCurrency({ colorize: true, negativeFormat: '-%s%n', roundToDecimalPlace: 3 });
        } else if ($(this).hasClass('currency_4_decimal')) {
            $(this).formatCurrency({ colorize: true, negativeFormat: '-%s%n', roundToDecimalPlace: 4 });
        }
    });
}

function handleNumericFormat() {
    var string = 'input[class*=numeric]:text';
    $(string).filter_input({ regex: '[0-9-.]' });
    $(string).blur(function() {
        if ($(this).hasClass('numeric')) {
            $(this).formatCurrency({ symbol: '', colorize: true, negativeFormat: '-%s%n', roundToDecimalPlace: 0 });
        } else if ($(this).hasClass('numeric_0_decimal')) {
            $(this).formatCurrency({ symbol: '', colorize: true, negativeFormat: '-%s%n', roundToDecimalPlace: 0 });
        } else if ($(this).hasClass('numeric_1_decimal')) {
            $(this).formatCurrency({ symbol: '', colorize: true, negativeFormat: '-%s%n', roundToDecimalPlace: 1 });

        } else if ($(this).hasClass('numeric_2_decimal')) {
            $(this).formatCurrency({ symbol: '', colorize: true, negativeFormat: '-%s%n', roundToDecimalPlace: 2 });
        } else if ($(this).hasClass('numeric_3_decimal')) {
            $(this).formatCurrency({ symbol: '', colorize: true, negativeFormat: '-%s%n', roundToDecimalPlace: 3 });
        } else if ($(this).hasClass('numeric_4_decimal')) {
            $(this).formatCurrency({ symbol: '', colorize: true, negativeFormat: '-%s%n', roundToDecimalPlace: 4 });
        }

    });
}

function handleRawIntegerFormat() {
    var string = 'input[class*=raw-integer]:text';
    $(string).filter_input({ regex: '[0-9]' });
    $(string).blur(function () {
        $(this).formatCurrency({ symbol: '', colorize: true, groupDigits: false, negativeFormat: '-%s%n', roundToDecimalPlace: 0 });
    });
}


function handlePercentageFormat() {
    var string = 'input[class*=percentage]:text';
    $(string).filter_input({ regex: '[0-9%-.]', live: true });
    $(string).blur(function () {

        if ($(this).hasClass('percentage')) {
            $(this).formatCurrency({ symbol: '', colorize: true, negativeFormat: '-%s%n', roundToDecimalPlace: 0 });
        } else if ($(this).hasClass('percentage_0_decimal')) {
            $(this).formatCurrency({ symbol: '', colorize: true, negativeFormat: '-%s%n', roundToDecimalPlace: 0 });
        } else if ($(this).hasClass('percentage_1_decimal')) {
            $(this).formatCurrency({ symbol: '', colorize: true, negativeFormat: '-%s%n', roundToDecimalPlace: 1 });

        } else if ($(this).hasClass('percentage_2_decimal')) {
            $(this).formatCurrency({ symbol: '', colorize: true, negativeFormat: '-%s%n', roundToDecimalPlace: 2 });
        } else if ($(this).hasClass('percentage_3_decimal')) {
            $(this).formatCurrency({ symbol: '', colorize: true, negativeFormat: '-%s%n', roundToDecimalPlace: 3 });
        } else if ($(this).hasClass('percentage_4_decimal')) {
            $(this).formatCurrency({ symbol: '', colorize: true, negativeFormat: '-%s%n', roundToDecimalPlace: 4 });
        }

        if ($(this).val().toString().length > 0) {
            var current = $(this).val();

            current.replace(" %", "");

            current += " %";

            $(this).val(current);
        }
    });
}

function handleNoThousandsSeperator() {
    var string = 'input[class*=noThousandsSeparator]:text';
    $(string).filter_input({ regex: '[0-9-.]' });
    $(string).blur(function () {

        if ($(this).hasClass('noThousandsSeparator')) {
            $(this).val($(this).val().split(",").join(""));
        }
    });
}

function handleIntegerFormat() {

    var string = 'input.integer:text';
    $(string).filter_input({ regex: '[0-9-]' });

    string = 'input.integer:text';
    $(string).filter_input({ regex: '[0-9-]', live: true });
    $(string).blur(function () {
        $(this).formatCurrency({ symbol: '', colorize: true, negativeFormat: '-%s%n', roundToDecimalPlace: 0 });
    });
}

//##################################################################
//QUERYSTRING
//##################################################################

function replaceQueryStringParameter(uri, key, value) {
    var re = new RegExp("([?|&])" + key + "=.*?(&|$)", "i");
    var separator = uri.indexOf('?') !== -1 ? "&" : "?";
    if (uri.match(re)) {
        return uri.replace(re, '$1' + key + "=" + value + '$2');
    } else {
        return uri + separator + key + "=" + value;
    }
}