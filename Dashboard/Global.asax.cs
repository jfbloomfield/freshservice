﻿using System;
using System.Configuration;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.SessionState;
using AS.Framework.Common.Helpers;
using Elmah;
using log4net.Config;

namespace AS.Freshservice.Dashboard
{
        public class Global : HttpApplication
        {
            #region Member Variables

            private string _sqlErrorMessage;

            #endregion

            #region Private Methods

            protected void Application_PostAuthorizeRequest()
            {
                if (IsWebApiRequest())
                {
                    HttpContext.Current.SetSessionStateBehavior(SessionStateBehavior.Required);
                }
            }

            protected void Application_Start(object sender, EventArgs e)
            {
                AreaRegistration.RegisterAllAreas();
                GlobalConfiguration.Configure(WebApiConfig.Register);
                FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
                RouteConfig.RegisterRoutes(RouteTable.Routes);
                BundleConfig.RegisterBundles(BundleTable.Bundles);
                 
                XmlConfigurator.Configure();
            }

            protected void ErrorLog_Filtering(object sender, ExceptionFilterEventArgs e)
            {
                _sqlErrorMessage = ContextManagerHelper.ContextDbErrorMessage;
            }

            protected void ErrorMail_Mailing(object sender, ErrorMailEventArgs e)
            {
                var subject = e.Mail.Subject;

                if (ConfigurationManager.AppSettings["ApplicationName"] != null)
                {
                    subject = subject.Replace("[application_name]", ConfigurationManager.AppSettings["ApplicationName"]);
                }

                if (ConfigurationManager.AppSettings["Environment"] != null)
                {
                    subject = subject.Replace("[environment]", ConfigurationManager.AppSettings["Environment"]);
                }

                subject = subject.Replace("[server]", Environment.MachineName);

                e.Mail.Subject = subject;

                if (!string.IsNullOrWhiteSpace(_sqlErrorMessage))
                {
                    var index = e.Mail.Body.IndexOf("<h1>");

                    var str = new StringBuilder();
                    str.Append("<h1>Sql Statement</h1>");
                    str.Append("<p style='padding: 2em; background-color: #E0E0E0'>");
                    str.Append(_sqlErrorMessage);
                    str.Append("</p>");

                    e.Mail.Body = e.Mail.Body.Insert(index, str.ToString());
                }
            }

            private bool IsWebApiRequest()
            {
                var isWebApiRequest = false;
                if (HttpContext.Current.Request.AppRelativeCurrentExecutionFilePath != null)
                {
                    isWebApiRequest =
                        HttpContext.Current.Request.AppRelativeCurrentExecutionFilePath.StartsWith(
                            WebApiConfig.UrlPrefixRelative);
                }
                return isWebApiRequest;
            }

            #endregion
        }
    }
