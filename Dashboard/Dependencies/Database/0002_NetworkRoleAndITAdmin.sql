DECLARE @CrossbowNetworkRole VARCHAR(30) = 'Crossbow Network User'

DECLARE @Sections TABLE (
     ID INT NULL
    ,SectionMastName VARCHAR(20) NOT NULL
)

INSERT INTO @Sections (ID, SectionMastName)
    SELECT ID, Name FROM AIMS.dbo.SectionMast WHERE Name IN ('CROSSBOW_ENTRY_PAGES','MENU_CROSSBOW')

DECLARE @Roles TABLE
(
    ID INT PRIMARY KEY
)

IF NOT EXISTS (SELECT TOP 1 1 FROM AIMS.dbo.UserRoleMast WHERE Descr = @CrossbowNetworkRole)
BEGIN
    INSERT INTO AIMS.dbo.UserRoleMast (Descr, Scope, ScopeKey)
         VALUES (@CrossbowNetworkRole, 'A', 0)
END

INSERT INTO @Roles
    SELECT ID
    FROM AIMS.dbo.UserRoleMast
    WHERE Descr IN ('IT Admins', 'ArrowStream IT Admins', @CrossbowNetworkRole)

INSERT INTO AIMS.dbo.UserRoleDet
    (
        UserRoleMast_ID, 
        SectionMast_ID, 
        PermAdmin, 
        PermRead, 
        PermUpdate, 
        PermAdd, 
        PermDelete, 
        PermNotNB, 
        PermExport, 
        PermSpecial1, 
        PermSpecial2, 
        PermSpecial3, 
        PermSpecial4, 
        PermSpecial5
    )
    SELECT
        r.ID, -- UserRoleMast_ID
        s.ID, --SectionMast_ID
        'N', --PermAdmin
        'Y', --PermRead
        'N', --PermUpdate
        'N', --PermAdd
        'N', --PermDelete
        'N', --PermNotNB
        'N', --PermExport
        'N', --PermSpecial1
        'N', --PermSpecial2
        'N', --PermSpecial3
        'N', --PermSpecial4
        'N'  --PermSpecial5
    FROM @Roles AS r, @Sections AS s
    WHERE NOT EXISTS (
        SELECT TOP 1 1
        FROM AIMS.dbo.UserRoleDet rd
        WHERE r.ID = rd.UserRoleMast_ID AND s.ID = rd.SectionMast_ID
    )