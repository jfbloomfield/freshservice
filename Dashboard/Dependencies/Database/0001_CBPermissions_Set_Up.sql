USE AIMS;
GO

DECLARE @SectionMastName VARCHAR(30);
DECLARE @SectionMastTitle VARCHAR(100);
DECLARE @SectionMastId INT
DECLARE @Roles TABLE
(
	ID INT PRIMARY KEY
)

SET @SectionMastName = 'CROSSBOW_ENTRY_PAGES';
SET @SectionMastTitle = 'Crossbow Entry Pages';


IF NOT EXISTS (SELECT TOP 1 Name FROM AIMS.dbo.SectionMast WHERE Name = @SectionMastName)
BEGIN
	INSERT INTO AIMS.dbo.SectionMast 
	(
		Name, 
		Title,
		ProductOptimizer
	)
	VALUES 
	(
		@SectionMastName, 
		@SectionMastTitle,
		'Y'
	)
END

SELECT @SectionMastId = ID
FROM AIMS.dbo.SectionMast 
WHERE Name = @SectionMastName

INSERT INTO @Roles
SELECT ID
FROM AIMS.dbo.UserRoleMast
WHERE Descr IN ('IT Admins')
AND ID NOT IN (
	SELECT UserRoleMast_ID
	FROM dbo.UserRoleDet
	WHERE SectionMast_ID = @SectionMastId
)

INSERT INTO AIMS.dbo.UserRoleDet
(
	UserRoleMast_ID, 
	SectionMast_ID, 
	PermAdmin, 
	PermRead, 
	PermUpdate, 
	PermAdd, 
	PermDelete, 
	PermNotNB, 
	PermExport, 
	PermSpecial1, 
	PermSpecial2, 
	PermSpecial3, 
	PermSpecial4, 
	PermSpecial5
)
SELECT r.ID, -- UserRoleMast_ID
	@SectionMastId, --SectionMast_ID
	'Y', --PermAdmin
	'Y', --PermRead
	'Y', --PermUpdate
	'Y', --PermAdd
	'Y', --PermDelete
	'Y', --PermNotNB
	'Y', --PermExport
	'Y', --PermSpecial1
	'Y', --PermSpecial2
	'Y', --PermSpecial3
	'Y', --PermSpecial4
	'Y'  --PermSpecial5
FROM @Roles r


SET @SectionMastName = 'CROSSBOW_ADMIN_PAGES';
SET @SectionMastTitle = 'Crossbow Admin Pages';


IF NOT EXISTS (SELECT TOP 1 Name FROM AIMS.dbo.SectionMast WHERE Name = @SectionMastName)
BEGIN
	INSERT INTO AIMS.dbo.SectionMast 
	(
		Name, 
		Title,
		ProductOnDemand
	)
	VALUES 
	(
		@SectionMastName, 
		@SectionMastTitle,
		'Y'
	)
END