﻿using System;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using AS.Crossbow.EntryPages.Common;
using AS.Crossbow.EntryPages.Controllers.Helpers.Interfaces;
using AS.Crossbow.EntryPages.Models;
using AS.Framework.Common.Repositories;
using AS.Framework.Core.BusinessEntities;
using AS.Framework.MVC.Attributes;
using AS.Framework.Permissions.Common;

namespace AS.Crossbow.EntryPages.Controllers
{
    [AimsMvcAuthorize(Constants.PermissionCode.NETWORK_PAGE, PermissionEnums.PermissionType.Read)]
    public class RegistrationPagesController : Controller
    {
        #region Member Variables

        private readonly IControllerHelper _controllerHelper;

        #endregion

        #region Constructors

        public RegistrationPagesController(IControllerHelper controllerHelper)
        {
            _controllerHelper = controllerHelper;
        }

        #endregion

        #region Controller Actions

        [HttpGet]
        public ActionResult Index()
        {
            ActionResult ret;
            using (new UnitOfWork())
            {
                if (!Request.RawUrl.EndsWith("/"))
                {
                    return new RedirectToRouteResult(RouteData.Values);
                }

                ViewBag.Title = _controllerHelper.GetPageTitle();

                var templateBundleUrl = Scripts.Url("~/bundles/html/templates").ToHtmlString();
                var templateBundleQueryString =
                    templateBundleUrl.Substring(templateBundleUrl.LastIndexOf("?", StringComparison.Ordinal));

                var templateBundleHash = HttpUtility.ParseQueryString(templateBundleQueryString)["v"];
                int tenantId;
                CoreUser coreUser;
                var userDomain = "unknown";
                tenantId = _controllerHelper.GetTenantId();
                using (new UnitOfWork())
                {
                    
                    coreUser = _controllerHelper.GetCoreUser();
                    var emailRegex = new Regex(@"^[^@]+@((.*)\.\w+)");
                    var emailMatch = emailRegex.Match(coreUser.EmailAddress);
                    if (emailMatch.Success)
                    {
                        userDomain = emailMatch.Groups[1].Value.ToUpper();
                    }
                }

                return View(new EntryPagesViewModel(templateBundleHash, _controllerHelper.GetUserName(), tenantId, userDomain, coreUser.Id));
            }
        }

        #endregion
    }
}