﻿using System.Web.Http;
using AS.Crossbow.EntryPages.Common;
using AS.Crossbow.EntryPages.Controllers.Helpers.Interfaces;
using AS.Crossbow.EntryPages.Models.ProgramPage;
using AS.Framework.Permissions.Common;
using AS.Framework.WebApi.Attributes;

namespace AS.Crossbow.EntryPages.Controllers
{
    [AimsWebApiAuthorize(Constants.PermissionCode.PROGRAM_PAGE, PermissionEnums.PermissionType.Read)]
    [RoutePrefix("api/program")]
    public class ProgramPageController : ApiController
    {
        #region Member Variables

        private readonly IProgramHelper _programHelper;

        private readonly int _tenantId;

        #endregion

        #region Constructors

        public ProgramPageController(IControllerHelper controllerHelper, IProgramHelper programHelper)
        {
            _programHelper = programHelper;
            _tenantId = controllerHelper.GetTenantId();
        }

        #endregion

        #region Controller Actions


        [HttpGet]
        [Route("managedStatistics")]
        public ManagedStatistics GetManagedStatistics()
        {
            return _programHelper.GetManagedStatistics(_tenantId);
        }

        #endregion
    }
}