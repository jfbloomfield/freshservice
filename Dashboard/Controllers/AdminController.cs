﻿
using System.Web.Http;
using AS.Crossbow.EntryPages.Common;
using AS.Crossbow.EntryPages.Controllers.Helpers.Interfaces;
using AS.Crossbow.EntryPages.Models.Admin;
using AS.Framework.Common.Repositories;
using AS.Framework.Permissions.Common;
using AS.Framework.WebApi.Attributes;

namespace AS.Crossbow.EntryPages.Controllers
{
    [RoutePrefix("api/admin")]
    [AimsWebApiAuthorize(Constants.PermissionCode.ADMIN_PAGES, PermissionEnums.PermissionType.Read)]
    public class AdminController : ApiController
    {
        #region Member Variables

        private readonly IAdminHelper _adminHelper;

        private readonly int _tenantId;

        #endregion

        #region Constructors

        public AdminController(IControllerHelper controllerHelper, IAdminHelper adminHelper)
        {
            _adminHelper = adminHelper;

            _tenantId = controllerHelper.GetTenantId();
        }

        #endregion

        #region Controller Actions

        [HttpGet]
        [Route("getaccountsettings")]
        public AccountSettings GetAccountSettings()
        {
            return _adminHelper.GetAccountSettings(_tenantId);
        }

        [HttpPost]
        [Route("saveaccountsettings")]
        public AccountSettings SaveAccountSettings(AccountSettings accountSettings)
        {
            return _adminHelper.SaveAccountSettings(_tenantId, accountSettings);
        }

        #endregion
    }
}