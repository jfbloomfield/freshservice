﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AS.Crossbow.EntryPages.Controllers.Helpers.Interfaces;
using AS.Crossbow.EntryPages.Models;
using AS.Crossbow.EntryPages.Models.BusinessObjects;
using AS.Crossbow.EntryPages.Models.NetworkPage;
using AS.Crossbow.EntryPages.Models.NetworkPage.PillSummary;
using AS.Crossbow.EntryPages.Repositories.DTO.Network;
using AS.Crossbow.EntryPages.Repositories.Interfaces;

namespace AS.Crossbow.EntryPages.Controllers.Helpers
{
    public class NetworkPageHelper : INetworkPageHelper
    {
        #region Member Variables

        private readonly ICalendarRepository _calendarRepository;

        private readonly INetworkRepository _networkRepository;

        #endregion

        #region Constructors

        public NetworkPageHelper(INetworkRepository networkRepository, ICalendarRepository calendarRepository)
        {
            _networkRepository = networkRepository;
            _calendarRepository = calendarRepository;
        }

        #endregion

        #region INetworkPageHelper Members

        public List<DistributionCenter> GetDistributionCenters(int tenantId)
        {
            var dcs = new List<DistributionCenter>();

            var receivers = _networkRepository.GetReceiverDtos(tenantId);
            foreach (var receiver in receivers)
            {
                dcs.Add(
                    new DistributionCenter
                    {
                        Id = receiver.Id,
                        Name = receiver.Name,
                        HasData = receiver.HasPillReceiverRecords
                    });
            }
            return dcs;
        }

        public NetworkSummary GetNetworkSummary(
            int tenantId,
            int? dcId,
            DateTime? currentStartPeriodStartDate,
            DateTime? currentEndPeriodStartDate)
        {
            var networkSummary = new NetworkSummary();

            var bars = new List<NetworkChartBar>();

            IEnumerable<SavingsDto> savingsDtos;
            var maxDate = DateTime.Today;
            var minDate = DateTime.Today.AddMonths(-25);

            if (dcId.HasValue && dcId.Value > 0)
            {
                savingsDtos = _networkRepository.GetSavings(tenantId, dcId.Value, minDate, maxDate);
            }
            else
            {
                savingsDtos = _networkRepository.GetSavings(tenantId, minDate, maxDate);
            }

            foreach (var savingsDto in savingsDtos)
            {
                var period = new NetworkChartPeriod
                {
                    Code = savingsDto.CalendarDetailAbbreviation,
                    Desc = savingsDto.CalendarDetailName,
                    StartDate = savingsDto.CalendarDetailStartDate,
                    EndDate = savingsDto.CalendarDetailEndDate,
                    CalendarDetailId = savingsDto.CalendarDetailId,
                    Period = savingsDto.Period,
                    Year = savingsDto.Year
                };

                bars.Add(new NetworkChartBar {Period = period, Value = savingsDto.Savings});
            }

            networkSummary.NetworkChart = new NetworkChart {Bars = bars, PeriodType = DeterminePeriodType(savingsDtos)};

            var startEndPeriods = GetInitialStartEndPeriods(
                bars,
                currentStartPeriodStartDate,
                currentEndPeriodStartDate);
            if (startEndPeriods != null)
            {
                networkSummary.InitialStartPeriod = startEndPeriods.StartPeriod;
                networkSummary.InitialEndPeriod = startEndPeriods.EndPeriod;

                networkSummary.PillSummaries = GetPillSummaries(
                    tenantId,
                    dcId,
                    networkSummary.InitialStartPeriod.StartDate,
                    networkSummary.InitialEndPeriod.StartDate);
            }

            return networkSummary;
        }

        public PillSummaries GetPillSummaries(
            int tenantId,
            int? dcId,
            DateTime startingPeriodStartDate,
            DateTime endingPeriodStartDate)
        {
            var pillSummariesTask =
                Task.Run(
                    () =>
                    GetPillSummariesWithoutSavingsDriverPillPeriods(
                        tenantId,
                        dcId,
                        startingPeriodStartDate,
                        endingPeriodStartDate));

            var savingsDriverPillPeriods = GetSavingsDriverPillPeriods(
                tenantId,
                dcId,
                startingPeriodStartDate,
                endingPeriodStartDate);

            var pillSummaries = pillSummariesTask.Result;

            pillSummaries.SavingsDriverPillPeriods = savingsDriverPillPeriods.Result;

            return pillSummaries;
        }

        public PagedResponse<SavingsDriverLane> GetSavingsDriverLanesByType(
            int tenantId,
            int start,
            int end,
            string sort,
            string order = "asc",
            int limit = 10,
            int offset = 0,
            int? dcId = null)
        {
            var calendar = _calendarRepository.GetDefaultCalendar(tenantId);

            var periods = GetPeriods(calendar, start, end);

            var laneImpactsTask = GetLaneSavingsImpacts(
                tenantId,
                periods.Skip(1).ToList(),
                sort,
                order,
                dcId,
                offset,
                limit);

            var laneIdsTask = GetLaneIds(laneImpactsTask);

            var lanesTask = GetLanes(tenantId, laneIdsTask);

            var groupedPillLanePeriodsTask = GetGroupedPillLanePeriods(
                laneIdsTask: laneIdsTask,
                tenantId: tenantId,
                calendarDetailIds: periods);

            var savingsDriverLanesTask = CreateSavingsDriverLanes(lanesTask, groupedPillLanePeriodsTask);

            var savingsDriverLanes = savingsDriverLanesTask.Result;

            return new PagedResponse<SavingsDriverLane>(laneImpactsTask.Result.Total, savingsDriverLanes);
        }

        #endregion

        #region Controller Actions

        public Task<List<SavingsDriverLane>> CreateSavingsDriverLanes(
            Task<IList<Lane>> lanesTask,
            Task<Dictionary<int, List<SavingsDriverPillLanePeriod>>> groupedPillLanePeriodsTask)
        {
            var task = Task.Run(
                () =>
                {
                    var groupedPillLanePeriods = groupedPillLanePeriodsTask.Result;

                    var lanes = lanesTask.Result;

                    return
                        lanes.Where(l => groupedPillLanePeriods.ContainsKey(l.Id))
                            .Select(lane => new SavingsDriverLane(lane, groupedPillLanePeriods[lane.Id].ToList()))
                            .ToList();
                });
            return task;
        }

        public Task<Dictionary<int, List<SavingsDriverPillLanePeriod>>> GetGroupedPillLanePeriods(
            int tenantId,
            Task<List<int>> laneIdsTask,
            IList<int> calendarDetailIds)
        {
            var task = Task.Run(
                () =>
                {
                    var pillLanePeriods =
                        _networkRepository.GetSavingsDriverPillLanePeriods(
                            calendarDetailIds: calendarDetailIds,
                            laneIds: laneIdsTask.Result,
                            tenantId: tenantId);

                    return pillLanePeriods.GroupBy(x => x.LaneId).ToDictionary(x => x.Key, x => x.ToList());
                });
            return task;
        }

        public Task<List<int>> GetLaneIds(Task<PagedResponse<LaneSavingsImpactDto>> lanesTask)
        {
            var task = Task.Run(() => lanesTask.Result.Rows.Select(x => x.LaneId).ToList());
            return task;
        }

        public Task<IList<Lane>> GetLanes(int tenantId, Task<List<int>> laneIds)
        {
            var task = Task.Run(
                () =>
                {
                    var lanes = _networkRepository.GetLanes(tenantId, laneIds.Result);
                    IList<Lane> orderedLanes = new List<Lane>();

                    foreach (var laneId in laneIds.Result)
                    {
                        orderedLanes.Add(lanes.Single(x => x.Id == laneId));
                    }

                    return orderedLanes;

                });
            return task;
        }

        public Task<PagedResponse<LaneSavingsImpactDto>> GetLaneSavingsImpacts(
            int tenantId,
            IList<int> calendarDetailIds,
            string sortColumn,
            string sortOrder,
            int? receiverId = null,
            int offset = 0,
            int limit = 10)
        {
            var task =
                Task.Run(
                    () =>
                    _networkRepository.GetLanesBySavingsImpact(
                        receiverId: receiverId,
                        tenantId: tenantId,
                        calendarDetailIds: calendarDetailIds,
                        limit: limit,
                        offset: offset,
                        sortColumn: sortColumn,
                        sortOrder: sortOrder));
            return task;
        }

        public static IList<int> GetPeriods(
            Calendar calendar,
            DateTime startingPeriodStartDate,
            DateTime endingPeriodStartDate)
        {
            var periods =
                calendar.Periods.Where(
                    x => x.StartDate >= startingPeriodStartDate && x.StartDate <= endingPeriodStartDate)
                    .OrderBy(x => x.StartDate)
                    .Select(x => x.Id)
                    .ToList();
            return periods;
        }

        #endregion

        #region Private Methods

        private PeriodType DeterminePeriodType(IEnumerable<SavingsDto> savingsDtos)
        {
            if (
                savingsDtos.Any(
                    savingsDto =>
                    savingsDto.CalendarDetailStartDate.Day != 1 || savingsDto.CalendarDetailEndDate.AddDays(1).Day != 1 ||
                    savingsDto.CalendarDetailStartDate.Month != savingsDto.CalendarDetailEndDate.Month ||
                    savingsDto.CalendarDetailStartDate.Year != savingsDto.CalendarDetailEndDate.Year))
            {
                return PeriodType.FiscalPeriod;
            }
            return PeriodType.Monthly;
        }

        private Dictionary<int, NewAndDiscontinuedCount> GetFirstNewAndDiscontinuedCount(
            IEnumerable<NewAndDiscontinuedLaneDto> newAndDiscontinuedLanes,
            Calendar calendar)
        {
            var calendarDict = calendar.Periods.ToDictionary(cd => cd.Id);

            var firstNew = new Dictionary<int, CalendarDetail>();
            var firstDiscontinued = new Dictionary<int, CalendarDetail>();
            var counts = new Dictionary<int, NewAndDiscontinuedCount>();

            foreach (var calendarDetail in calendar.Periods)
            {
                counts[calendarDetail.Id] = new NewAndDiscontinuedCount();
            }

            foreach (var newAndDiscontinuedLaneDto in newAndDiscontinuedLanes)
            {
                var laneId = newAndDiscontinuedLaneDto.LaneId;
                var cal = calendarDict[newAndDiscontinuedLaneDto.CalendarDetailId];
                var count = counts[cal.Id];

                if (newAndDiscontinuedLaneDto.NewLanesCount > 0)
                {
                    CalendarDetail currentNew;
                    firstNew.TryGetValue(laneId, out currentNew);
                    if (currentNew == null)
                    {
                        firstNew[laneId] = cal;
                        count.NewLanesCount++;
                    }
                    else if (currentNew.StartDate > cal.StartDate)
                    {
                        firstNew[laneId] = cal;
                        count.NewLanesCount++;
                        counts[currentNew.Id].NewLanesCount--;
                    }
                }

                if (newAndDiscontinuedLaneDto.DiscontinuedLanesCount > 0)
                {
                    CalendarDetail currentDiscontinued;
                    firstDiscontinued.TryGetValue(laneId, out currentDiscontinued);
                    if (currentDiscontinued == null)
                    {
                        firstDiscontinued[laneId] = cal;
                        count.DiscontinuedLanesCount++;
                    }
                    else if (currentDiscontinued.StartDate > cal.StartDate)
                    {
                        firstDiscontinued[laneId] = cal;
                        count.DiscontinuedLanesCount++;
                        counts[currentDiscontinued.Id].NewLanesCount--;
                    }
                }
            }

            return counts;
        }

        private static IList<int> GetPeriods(
            Calendar calendar,
            int startingCalendarDetailId,
            int endingCalendarDetailId)
        {
            var startingPeriodStartDate = calendar.Periods.First(x => x.Id == startingCalendarDetailId).StartDate;
            var endingPeriodStartDate = calendar.Periods.First(x => x.Id == endingCalendarDetailId).StartDate;

            return GetPeriods(calendar, startingPeriodStartDate, endingPeriodStartDate);
        }

        private PillSummaries GetPillSummariesWithoutSavingsDriverPillPeriods(
            int tenantId,
            int? dcId,
            DateTime startingPeriodStartDate,
            DateTime endingPeriodStartDate)
        {
            var pillSummaries = new PillSummaries();

            var pillsSummaryDto = _networkRepository.GetPillsSummaryDto(
                tenantId,
                dcId,
                startingPeriodStartDate,
                endingPeriodStartDate);

            var accessorialFeesPillSummary = new AccessorialFeesPillSummary();
            accessorialFeesPillSummary.StartingFeesPerLoad = pillsSummaryDto._17d_AccessorialFeesPerLoadStart;
            accessorialFeesPillSummary.EndingFeesPerLoad = pillsSummaryDto._17e_AccessorialFeesPerLoadEnd;
            accessorialFeesPillSummary.StartingPercentOfTotalCost = pillsSummaryDto._17f_AccessorialPercentStart;
            accessorialFeesPillSummary.EndingPercentOfTotalCost = pillsSummaryDto._17g_AccessorialPercentEnd;
            accessorialFeesPillSummary.StartingDetentionChargesPerLoad =
                pillsSummaryDto._17h_DetentionChargesPerLoadStart;
            accessorialFeesPillSummary.EndingDetentionChargesPerLoad = pillsSummaryDto._17i_DetentionChargesPerLoadEnd;

            accessorialFeesPillSummary.ImpactAmount = pillsSummaryDto._7a_AccessorialFeesImpact;
            accessorialFeesPillSummary.PillSummaryAccessorialFeesPerLoadEnd =
                pillsSummaryDto._7b_AccessorialFeesPerLoadEnd;
            accessorialFeesPillSummary.PillSummaryAccessorialFeesPerLoadStart =
                pillsSummaryDto._7c_AccessorialFeesPerLoadStart;
            pillSummaries.AccessorialFeesPillSummary = accessorialFeesPillSummary;

            var backhaulUsagePillSummary = new BackhaulUsagePillSummary();
            backhaulUsagePillSummary.EndingLoadsPercent = pillsSummaryDto._16e_BackhaulLoadsEnd;
            backhaulUsagePillSummary.StartingLoadsPercent = pillsSummaryDto._16d_BackhaulLoadsStart;
            backhaulUsagePillSummary.EndingCases = pillsSummaryDto._16g_CasesMovedBackhaulEnd;
            backhaulUsagePillSummary.EndingPoundsPerLoad = pillsSummaryDto._16i_PoundsPerBackhaulEnd;
            backhaulUsagePillSummary.StartingCases = pillsSummaryDto._16f_CasesMovedBackhaulStart;
            backhaulUsagePillSummary.StartingPoundsPerLoad = pillsSummaryDto._16h_PoundsPerBackhaulStart;
            backhaulUsagePillSummary.ImpactAmount = pillsSummaryDto._6a_BackhaulUsageImpact;
            backhaulUsagePillSummary.PillSummaryBackhaulUsageEnd = pillsSummaryDto._6b_BackhaulUsageEnd;
            backhaulUsagePillSummary.PillSummaryBackhaulUsageStart = pillsSummaryDto._6c_BackhaulUsageStart;
            pillSummaries.BackhaulUsagePillSummary = backhaulUsagePillSummary;

            var buyingPatternPillSummary = new BuyingPatternPillSummary();
            buyingPatternPillSummary.EndingEquipmentUtilizationPercent = pillsSummaryDto._14e_UtilizationEnd;
            buyingPatternPillSummary.EndingLtlUsagePercent = pillsSummaryDto._14i_LTLUsageEnd;
            buyingPatternPillSummary.EndingPoundsPerShipment = pillsSummaryDto._14g_PoundsPerShipmentEnd;
            buyingPatternPillSummary.StartingEquipmentUtilizationPercent = pillsSummaryDto._14d_UtilizationStart;
            buyingPatternPillSummary.StartingLtlUsagePercent = pillsSummaryDto._14h_LTLUsageStart;
            buyingPatternPillSummary.StartingPoundsPerShipment = pillsSummaryDto._14f_PoundsPerShipmentStart;
            buyingPatternPillSummary.ImpactAmount = pillsSummaryDto._4a_BuyingPatternImpact;
            buyingPatternPillSummary.PillSummaryWeightPerShipmentEnd = pillsSummaryDto._4b_WeightPerShipmentEnd;
            buyingPatternPillSummary.PillSummaryWeightPerShipmentStart = pillsSummaryDto._4c_WeightPerShipmentStart;
            pillSummaries.BuyingPatternPillSummary = buyingPatternPillSummary;

            var carrierRatesPillSummary = new CarrierRatesPillSummary();
            carrierRatesPillSummary.EndingIntermodalLineHaulCostPerMile = pillsSummaryDto._13i_IMCostPerMileEnd;
            carrierRatesPillSummary.EndingLtlLineHaulCostPerCwt = pillsSummaryDto._13e_LTLCostPerCWTEnd;
            carrierRatesPillSummary.EndingTruckloadLineHaulCostPerMile = pillsSummaryDto._13g_TLCostPerMileEnd;
            carrierRatesPillSummary.StartingIntermodalLineHaulCostPerMile = pillsSummaryDto._13h_IMCostPerMileStart;
            carrierRatesPillSummary.StartingLtlLineHaulCostPerCwt = pillsSummaryDto._13d_LTLCostPerCWTStart;
            carrierRatesPillSummary.StartingTruckloadLineHaulCostPerMile = pillsSummaryDto._13f_TLCostPerMileStart;
            carrierRatesPillSummary.ImpactAmount = pillsSummaryDto._3a_CarrierRatesImpact;
            carrierRatesPillSummary.PillSummaryNonBHLinehaulCostPerMileEnd =
                pillsSummaryDto._3b_NonBHLinehaulCostPerMileEnd;
            carrierRatesPillSummary.PillSummaryNonBHLinehaulCostPerMileStart =
                pillsSummaryDto._3c_NonBHLinehaulCostPerMileStart;
            pillSummaries.CarrierRatesPillSummary = carrierRatesPillSummary;

            var discontinuedLanesPillSummary = new DiscontinuedLanesPillSummary();
            discontinuedLanesPillSummary.FreightLanesDiscontinuedByManagement = pillsSummaryDto._18a_LanesDiscontinued;
            discontinuedLanesPillSummary.LostMonthlyAllowance = pillsSummaryDto._18c_LostMonthlyAllowance;
            discontinuedLanesPillSummary.PriorSavingsOnDiscontinuedLanes = pillsSummaryDto._18b_PriorSavings;
            discontinuedLanesPillSummary.ImpactAmount = pillsSummaryDto._11a_DiscontinuedLanesImpact;
            discontinuedLanesPillSummary.PillSummaryDiscontinuedLaneCount = pillsSummaryDto._11b_DiscontinuedLaneCount;
            pillSummaries.DiscontinuedLanesPillSummary = discontinuedLanesPillSummary;

            var freightAllowancesPillSummary = new FreightAllowancesPillSummary();
            freightAllowancesPillSummary.StartingAllowancePerCwt = pillsSummaryDto._19d_AllowancePerCWTStart;
            freightAllowancesPillSummary.EndingAllowancePerCwt = pillsSummaryDto._19e_AllowancePerCWTEnd;
            freightAllowancesPillSummary.StartingAllowancePerCase = pillsSummaryDto._19f_AllowancePerCaseStart;
            freightAllowancesPillSummary.EndingAllowancePerCase = pillsSummaryDto._19g_AllowancePerCaseEnd;
            freightAllowancesPillSummary.StartingTruckloadLineHaulCostPerCwt =
                pillsSummaryDto._19h_LinehaulCostPerCWTStart;
            freightAllowancesPillSummary.EndingTruckloadLineHaulCostPerCwt = pillsSummaryDto._19i_LinehaulCostPerCWTEnd;
            freightAllowancesPillSummary.ImpactAmount = pillsSummaryDto._10a_FreightAllowanceImpact;
            freightAllowancesPillSummary.PillSummaryAllowancePerPoundEnd = pillsSummaryDto._10b_AllowancePerPoundEnd;
            freightAllowancesPillSummary.PillSummaryAllowancePerPoundStart = pillsSummaryDto._10c_AllowancePerPoundStart;
            pillSummaries.FreightAllowancesPillSummary = freightAllowancesPillSummary;

            var freightConsolidationPillSummary = new ConsolidationPillSummary();
            freightConsolidationPillSummary.StartingPercentOfAllLoadsConsolidated =
                pillsSummaryDto._21d_PercentLoadsConsolidatedStart;
            freightConsolidationPillSummary.EndingPercentOfAllLoadsConsolidated =
                pillsSummaryDto._21e_PercentLoadsConsolidatedEnd;
            freightConsolidationPillSummary.StartingConsolidatedLoads = pillsSummaryDto._21f_LoadsConsolidatedStart;
            freightConsolidationPillSummary.EndingConsolidatedLoads = pillsSummaryDto._21g_LoadsConsolidatedEnd;
            freightConsolidationPillSummary.StartingLtlLoadsConsolidatedToTruckload =
                pillsSummaryDto._21h_LTLConsolidatedStart;
            freightConsolidationPillSummary.EndingLtlLoadsConsolidatedToTruckload =
                pillsSummaryDto._21i_LTLConsolidatedEnd;
            freightConsolidationPillSummary.ImpactAmount = pillsSummaryDto._8a_FreightConsolidationImpact;
            freightConsolidationPillSummary.PillSummaryMultiStopPercentEnd = pillsSummaryDto._8b_MultiStopPercentEnd;
            freightConsolidationPillSummary.PillSummaryMultiStopPercentStart = pillsSummaryDto._8c_MultiStopPercentStart;
            pillSummaries.ConsolidationPillSummary = freightConsolidationPillSummary;

            var fuelCostPillSummary = new FuelCostPillSummary();
            fuelCostPillSummary.EndingDeptOfEnergyDieselIndexCostPerCwt = pillsSummaryDto._20i_PricePerGallonEnd;
            fuelCostPillSummary.EndingFuelCostPerMile = pillsSummaryDto._20g_FuelCostPerMileEnd;
            fuelCostPillSummary.EndingFuelCostPercentOfLoadCost = pillsSummaryDto._20e_FuelPercentOfCostEnd;
            fuelCostPillSummary.StartingDeptOfEnergyDieselIndexCostPerCwt = pillsSummaryDto._20h_PricePerGallonStart;
            fuelCostPillSummary.StartingFuelCostPerMile = pillsSummaryDto._20f_FuelCostPerMileStart;
            fuelCostPillSummary.StartingFuelCostPercentOfLoadCost = pillsSummaryDto._20d_FuelPercentOfCostStart;
            fuelCostPillSummary.ImpactAmount = pillsSummaryDto._9a_FuelImpact;
            fuelCostPillSummary.PillSummaryFuelCostPercentEnd = pillsSummaryDto._9b_FuelCostPercentEnd;
            fuelCostPillSummary.PillSummaryFuelCostPercentStart = pillsSummaryDto._9c_FuelCostPercentStart;
            pillSummaries.FuelCostPillSummary = fuelCostPillSummary;

            var newLanesPillSummary = new NewLanesPillSummary();
            newLanesPillSummary.NewLanesUnderManagement = pillsSummaryDto._15a_LanesNew;
            newLanesPillSummary.NewMonthlyAllowance = pillsSummaryDto._15c_NewMonthlyAllowance;
            newLanesPillSummary.SavingsOnNewLanes = pillsSummaryDto._15b_NewSavings;
            newLanesPillSummary.ImpactAmount = pillsSummaryDto._5a_NewLanesImpact;
            newLanesPillSummary.PillSummaryNewLaneCount = pillsSummaryDto._5b_NewLaneCount;
            pillSummaries.NewLanesPillSummary = newLanesPillSummary;

            var volumePillSummary = new VolumePillSummary();
            volumePillSummary.StartingPoundsPerShipment = pillsSummaryDto._12h_PoundsPerShipmentStart;
            volumePillSummary.EndingPoundsPerShipment = pillsSummaryDto._12i_PoundsPerShipmentEnd;
            volumePillSummary.StartingShippedPounds = pillsSummaryDto._12d_PoundsShippedStart;
            volumePillSummary.EndingShippedPounds = pillsSummaryDto._12e_PoundsShippedEnd;
            volumePillSummary.StartingShippedCases = pillsSummaryDto._12f_CasesShippedStart;
            volumePillSummary.EndingShippedCases = pillsSummaryDto._12g_CasesShippedEnd;
            volumePillSummary.ImpactAmount = pillsSummaryDto._2a_VolumeImpact;
            volumePillSummary.PillSummaryWeightEnd = pillsSummaryDto._2b_WeightEnd;
            volumePillSummary.PillSummaryWeightStart = pillsSummaryDto._2c_WeightStart;
            pillSummaries.VolumePillSummary = volumePillSummary;

            return pillSummaries;
        }

        private Task<IList<SavingsDriverPillPeriod>> GetSavingsDriverPillPeriods(
            int tenantId,
            int? dcId,
            DateTime startingPeriodStartDate,
            DateTime endingPeriodStartDate)
        {
            var calendar = _calendarRepository.GetDefaultCalendar(tenantId);

            var periods = GetPeriods(calendar, startingPeriodStartDate, endingPeriodStartDate);

            var newAndDiscontinuedLaneCountsTask = Task.Run(
                () =>
                {
                    var newAndDiscontinuedLaneDtos = _networkRepository.GetNewAndDiscontinuedLaneDtos(
                        tenantId,
                        periods,
                        dcId);

                    return GetFirstNewAndDiscontinuedCount(newAndDiscontinuedLaneDtos, calendar);
                });

            var savingsDriverPillPeriodDtosTask =
                Task.Run(
                    () =>
                    _networkRepository.GetSavingsDriverPillPeriods(
                        calendarDetailIds: periods,
                        receiverId: dcId,
                        tenantId: tenantId));

            var savingsDriverPillPeriodsTask = Task.Run(
                () =>
                {
                    var newAndDiscontinuedLaneCounts = newAndDiscontinuedLaneCountsTask.Result;
                    var savingsDriverPillPeriods = savingsDriverPillPeriodDtosTask.Result;

                    foreach (var savingsDriverPillPeriod in savingsDriverPillPeriods)
                    {
                        var count = newAndDiscontinuedLaneCounts[savingsDriverPillPeriod.CalendarDetailId];
                        savingsDriverPillPeriod.SingleNewLanesCount = count.NewLanesCount;
                        savingsDriverPillPeriod.SingleDiscontinuedLanesCount = count.DiscontinuedLanesCount;
                    }

                    return savingsDriverPillPeriods;
                });

            return savingsDriverPillPeriodsTask;
        }

        #endregion

        internal StartEndPeriods GetInitialStartEndPeriods(
            List<NetworkChartBar> bars,
            DateTime? currentStartPeriodStartDate,
            DateTime? currentEndPeriodStartDate)
        {
            NetworkChartPeriod endPeriodSelectedDefault = null;
            NetworkChartPeriod startPeriodSelectedDefaultBackup = null;
            NetworkChartPeriod startPeriodSelectedDefault = null;

            if (currentStartPeriodStartDate.HasValue && currentEndPeriodStartDate.HasValue)
            {
                var startbar = bars.FirstOrDefault(x => x.Period.StartDate == currentStartPeriodStartDate.Value);
                if (startbar?.Value != null)
                {
                    var endBar = bars.FirstOrDefault(x => x.Period.StartDate == currentEndPeriodStartDate.Value);
                    if (endBar?.Value != null)
                    {
                        return new StartEndPeriods {StartPeriod = startbar.Period, EndPeriod = endBar.Period};
                    }
                }
            }

            for (var index = bars.Count - 1; index >= 0; index--)
            {
                if (bars[index].Value != null)
                {
                    if (endPeriodSelectedDefault == null)
                    {
                        if (bars[index].Period.EndDate < DateTime.Today && index > 0)
                        {
                            endPeriodSelectedDefault = bars[index].Period;
                        }
                    }
                    else if (startPeriodSelectedDefaultBackup == null)
                    {
                        startPeriodSelectedDefaultBackup = bars[index].Period;
                    }
                    else if (bars[index].Period.Period == endPeriodSelectedDefault.Period &&
                             bars[index].Period.Year == endPeriodSelectedDefault.Year - 1)
                    {
                        startPeriodSelectedDefault = bars[index].Period;
                        break;
                    }
                }
            }

            if (endPeriodSelectedDefault != null)
            {
                if (startPeriodSelectedDefault != null)
                {
                    return new StartEndPeriods
                    {
                        StartPeriod = startPeriodSelectedDefault,
                        EndPeriod = endPeriodSelectedDefault
                    };
                }
                if (startPeriodSelectedDefaultBackup != null)
                {
                    return new StartEndPeriods
                    {
                        StartPeriod = startPeriodSelectedDefaultBackup,
                        EndPeriod = endPeriodSelectedDefault
                    };
                }
            }

            return null;
        }

        #region Nested Type: NewAndDiscontinuedCount

        internal class NewAndDiscontinuedCount
        {
            #region Properties

            public int DiscontinuedLanesCount { get; set; }
            public int NewLanesCount { get; set; }

            #endregion
        }

        #endregion

        #region Nested Type: StartEndPeriods

        internal class StartEndPeriods
        {
            #region Properties

            public NetworkChartPeriod EndPeriod { get; set; }
            public NetworkChartPeriod StartPeriod { get; set; }

            #endregion
        }

        #endregion
    }
}