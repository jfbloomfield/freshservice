﻿using System;
using System.Web;
using AS.Crossbow.EntryPages.Common;
using AS.Crossbow.EntryPages.Controllers.Helpers.Interfaces;
using AS.Crossbow.EntryPages.Models;
using AS.Crossbow.EntryPages.Repositories.Interfaces;
using AS.Framework.Common.Repositories;
using AS.Framework.Core.BusinessEntities;
using AS.Framework.Core.Repositories.Interfaces;

namespace AS.Crossbow.EntryPages.Controllers.Helpers
{
    public class ControllerHelper : IControllerHelper
    {
        #region Member Variables

        private readonly IAspParametersRepository _aspParametersRepository;
        private readonly ICoreUserRepository _coreUserRepository;
        private readonly IAimsRepository _aimsRepository;
        private readonly IAdminRepository _adminRepository;

        #endregion

        #region Constructors

        public ControllerHelper(IAspParametersRepository aspParametersRepository, ICoreUserRepository coreUserRepository, IAimsRepository aimsRepository, IAdminRepository adminRepository)
        {
            _aspParametersRepository = aspParametersRepository;
            _coreUserRepository = coreUserRepository;
            _aimsRepository = aimsRepository;
            _adminRepository = adminRepository;
        }

        #endregion

        #region Public Methods

        public virtual string GetPageTitle()
        {
            using (new UnitOfWork())
            {
                return $"{_aspParametersRepository.GetParameter(Constants.AspParameters.PAGE_TITLE)} - Crossbow";
            }
        }

        public virtual int GetTenantId()
        {
            
            AimsTenantInformation tenantInfo;
            tenantInfo = new AimsTenantInformation();
            //get tenant ID from aspparameters and get db server name
            //then check that server name is in core.tenant.dbservername,  if not throw error.  
            //if matches return tenant_id
            using (new UnitOfWork())
            {
               tenantInfo = _aimsRepository.GetTenantInformation();
            }

            if (tenantInfo != null)
                {
                    //get server name in core.tenant
                    var server = _adminRepository.GetDBServerNameByTenantId(tenantInfo.Tenant_ID);
                    if (tenantInfo.DbServerName == server)
                    {
                        return tenantInfo.Tenant_ID;
                    }
                }

                throw new DataMisalignedException("Tenant ID and DB server name do not match.");
            
        }

        public virtual string GetUserName()
        {
            return ((AIMSPrincipal) HttpContext.Current.User).Identity.Name;
        }

        public virtual CoreUser GetCoreUser()
        {
            var aimsPrincipal = HttpContext.Current.User as AIMSPrincipal;
            var aimsIdentity = aimsPrincipal.Identity as AIMSIdentity;

            return _coreUserRepository.GetUser(aimsIdentity.UserId);
        }

        #endregion
    }
}