﻿using System;
using AS.Crossbow.EntryPages.Controllers.Helpers.Interfaces;
using AS.Crossbow.EntryPages.Models.Admin;
using AS.Crossbow.EntryPages.Repositories.Interfaces;

namespace AS.Crossbow.EntryPages.Controllers.Helpers
{
    public class AdminHelper : IAdminHelper
    {
        #region Member Variables

        private readonly IAdminRepository _adminRepository;

        #endregion

        #region Constructors

        public AdminHelper(IAdminRepository adminRepository)
        {
            _adminRepository = adminRepository;
        }

        #endregion

        #region Public Methods

        public AccountSettings GetAccountSettings(int tenantId)
        {
            //return only lookup if adding new tenant
            var accountSettings = tenantId > 0 ? _adminRepository.GetAccountSettings(tenantId) : new AccountSettings();
            accountSettings.States = _adminRepository.GetStates(tenantId);
            accountSettings.BusinessTypes = _adminRepository.GetBusinessTypes(tenantId);
            return accountSettings;
        }

        public AccountSettings SaveAccountSettings(int tenantId, AccountSettings accountSettings)
        {
            if (accountSettings == null)
            {
                throw new ArgumentNullException(nameof(accountSettings));
            }
            if (accountSettings.Id > 0)
            {
                _adminRepository.UpdateAccountSettings(tenantId, accountSettings);
            }
            else
            {
                accountSettings.Id = _adminRepository.InsertAccountSettings(tenantId, accountSettings);
            }
            return accountSettings;
        }

        #endregion
    }
}