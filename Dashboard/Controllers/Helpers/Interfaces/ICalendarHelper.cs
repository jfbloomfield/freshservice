﻿using AS.Crossbow.EntryPages.Models;

namespace AS.Crossbow.EntryPages.Controllers.Helpers.Interfaces
{
    public interface ICalendarHelper
    {
        #region Public Methods

        Calendar GetDefaultCalendar(int tenantId);

        #endregion
    }
}