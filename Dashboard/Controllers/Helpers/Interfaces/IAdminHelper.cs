﻿using AS.Crossbow.EntryPages.Models.Admin;

namespace AS.Crossbow.EntryPages.Controllers.Helpers.Interfaces
{
    public interface IAdminHelper
    {
        AccountSettings GetAccountSettings(int tenantId);
        AccountSettings SaveAccountSettings(int tenantId, AccountSettings accountSettings);
    }
}