using AS.Framework.Core.BusinessEntities;

namespace AS.Crossbow.EntryPages.Controllers.Helpers.Interfaces
{
    public interface IControllerHelper
    {
        #region Public Methods

        CoreUser GetCoreUser();
        string GetPageTitle();
        int GetTenantId();
        string GetUserName();

        #endregion
    }
}