﻿using AS.Crossbow.EntryPages.Models.ProgramPage;

namespace AS.Crossbow.EntryPages.Controllers.Helpers.Interfaces
{
    public interface IProgramHelper
    {
        #region Public Methods

        ManagedStatistics GetManagedStatistics(int tenantId);

        #endregion
    }
}