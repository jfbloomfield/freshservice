using System;
using System.Collections.Generic;
using AS.Crossbow.EntryPages.Models;
using AS.Crossbow.EntryPages.Models.BusinessObjects;
using AS.Crossbow.EntryPages.Models.NetworkPage;
using AS.Crossbow.EntryPages.Models.NetworkPage.PillSummary;

namespace AS.Crossbow.EntryPages.Controllers.Helpers.Interfaces
{
    public interface INetworkPageHelper
    {
        #region Public Methods

        List<DistributionCenter> GetDistributionCenters(int tenantId);

        NetworkSummary GetNetworkSummary(
            int tenantId,
            int? dcId,
            DateTime? currentStartPeriodStartDate,
            DateTime? currentEndPeriodStartDate);

        PillSummaries GetPillSummaries(
            int tenantId,
            int? dcId,
            DateTime startingPeriodStartDate,
            DateTime endingPeriodStartDate);

        PagedResponse<SavingsDriverLane> GetSavingsDriverLanesByType(
            int tenantId,
            int start,
            int end,
            string sort,
            string order = "asc",
            int limit = 10,
            int offset = 0,
            int? dcId = null);

        #endregion
    }
}