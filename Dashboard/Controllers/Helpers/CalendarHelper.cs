﻿using AS.Crossbow.EntryPages.Controllers.Helpers.Interfaces;
using AS.Crossbow.EntryPages.Models;
using AS.Crossbow.EntryPages.Repositories.Interfaces;

namespace AS.Crossbow.EntryPages.Controllers.Helpers
{
    public class CalendarHelper : ICalendarHelper
    {
        #region Member Variables

        private readonly ICalendarRepository _calendarRepository;

        #endregion

        #region Constructors

        public CalendarHelper(ICalendarRepository calendarRepository)
        {
            _calendarRepository = calendarRepository;
        }

        #endregion

        #region ICalendarHelper Members

        public Calendar GetDefaultCalendar(int tenantId)
        {
            return _calendarRepository.GetDefaultCalendar(tenantId);
        }

        #endregion
    }
}