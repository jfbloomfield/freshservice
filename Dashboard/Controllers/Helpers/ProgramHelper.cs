﻿using AS.Crossbow.EntryPages.Controllers.Helpers.Interfaces;
using AS.Crossbow.EntryPages.Models.ProgramPage;
using AS.Crossbow.EntryPages.Repositories.Interfaces;

namespace AS.Crossbow.EntryPages.Controllers.Helpers
{
    public class ProgramHelper : IProgramHelper
    {
        #region Member Variables

        private readonly IProgramRepository _programRepository;

        #endregion

        #region Constructors

        public ProgramHelper(IProgramRepository programRepository)
        {
            _programRepository = programRepository;
        }

        #endregion

        #region IProgramHelper Members

        public ManagedStatistics GetManagedStatistics(int tenantId)
        {
            var allCases = _programRepository.GetCurrentYearCaseCount(tenantId);
            var managedCases = _programRepository.GetCurrentYearNetworkCaseCount(tenantId);

            return new ManagedStatistics(managedCases: managedCases, allCases: allCases);
        }

        #endregion
    }
}