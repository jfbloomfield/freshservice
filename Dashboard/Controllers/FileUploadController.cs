﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using AS.Crossbow.Core.Data;
using AS.Crossbow.Core.Data.MySql;
using AS.Crossbow.EntryPages.Common;
using AS.Crossbow.EntryPages.Controllers.Helpers.Interfaces;
using AS.Crossbow.EntryPages.Models.Admin;
using AS.Crossbow.EntryPages.Repositories.Interfaces;
using AS.Framework.Common.Repositories;
using AS.Framework.MVC.Attributes;
using AS.Framework.Permissions.Common;
using AS.Framework.WebApi.Attributes;
using AS.ValidationEngine.BusinessClasses.Interfaces;
using AS.ValidationEngine.Interfaces;
using AS.ValidationEngine.Model;
using CsvHelper;
using Elmah;
using Newtonsoft.Json;
using OfficeOpenXml;

namespace AS.Crossbow.EntryPages.Controllers
{
    [RoutePrefix("api/FileUpload")]
    [AimsWebApiAuthorize(Constants.PermissionCode.ADMIN_PAGES, PermissionEnums.PermissionType.Read)]
    public class FileUploadController : ApiController
    {
        #region Member Variables

        private readonly IControllerHelper _controllerHelper;

        private readonly IExcelBC _excelBc;

        private readonly IExcelUploadRepository _iExcelUploadRepository;
        private readonly IValidator _validator;

        private readonly int _tenantId;
        #endregion

        #region Constructors

        public FileUploadController(
            IControllerHelper controllerHelper,
            IValidator validator,
            IExcelUploadRepository iExcelUploadRepository,
            IExcelBC excelBc)
        {
            _controllerHelper = controllerHelper;
            _validator = validator;
            _excelBc = excelBc;
            _iExcelUploadRepository = iExcelUploadRepository;

            _tenantId = _controllerHelper.GetTenantId();
            
        }

        #endregion

        #region Controller Actions

        public string GetFileName(MultipartFileData fileData)
        {
            return fileData.Headers.ContentDisposition.FileName;
        }

        [Route("Upload")]
        [HttpPost]
        public async Task<HttpResponseMessage> Upload()
        {
            if (!Request.Content.IsMimeMultipartContent())
            {
                Request.CreateResponse(HttpStatusCode.UnsupportedMediaType);
            }
            var excelImportFile = new ExcelImportFile();

            excelImportFile.TenantId = _tenantId;
            try
            {
                var root = HttpContext.Current.Server.MapPath("~/temp");
                if (!Directory.Exists(root))
                {
                    Directory.CreateDirectory(root);
                }

                var provider = new MultipartFormDataStreamProvider(root);
                var result = await Request.Content.ReadAsMultipartAsync(provider);

                // On upload, files are given a generic name like "BodyPart_26d6abe1-3ae1-416a-9429-b35f15e6e5d5"
                // so this is how you can get the original file name
                excelImportFile.OriginalFileName = GetDeserializedFileName(result.FileData.First());

                var path = result.FileData.First().LocalFileName;

                //var inputFile = new FileInfo(path);
                //path = path + ".xls";
                //inputFile.MoveTo(path);

                //open file and do validations
                var definitionFileLocation = AppDomain.CurrentDomain.BaseDirectory +
                                             "Content\\files\\CrossbowImportSepcification.xlsx";
                var definitionFile = new ExcelDataFile(definitionFileLocation, "Import Fields");

                var excelDataFile = new ExcelDataFile(path, "Data");

                //need lookup lists for 
                var lookupLists = new List<LookupList>();

                using (var connection = ConnectionManager.GetOpenReadOnlyConnection(1))
                {
                    using (var transaction = TransactionManager.GetTransaction(connection))
                    {
                        lookupLists.Add(_iExcelUploadRepository.GetModeLookupList(transaction));
                        lookupLists.Add(
                            _iExcelUploadRepository.GetEquipmentLookupList(excelImportFile.TenantId, transaction));
                    }
                }

                //add tempprotect lookuplist
                lookupLists.Add(GetTempProtectLookupList());

                excelImportFile.ValidateDataSet = _validator.ValidateExcelDataSetWithExcelDefinition(
                    definitionFile,
                    excelDataFile,
                    lookupLists);

                //if no file level validation errors but errors in file then create excel file and return file location
                if (excelImportFile.ValidateDataSet.Violations.Count > 0)
                {
                    //display violations on screen
                    return Request.CreateResponse(HttpStatusCode.OK, excelImportFile);
                }
                if (excelImportFile.ValidateDataSet.NumberOfViolations > 0)
                {
                    //return file for user to fix problems
                    excelImportFile.CheckFilePath = WebConfigurationManager.AppSettings["ExcelFileDirectory"] + "\\" +
                                                    excelImportFile.OriginalFileName;
                    if (File.Exists(excelImportFile.CheckFilePath))
                    {
                        File.Delete(excelImportFile.CheckFilePath);
                    }

                    //for BackHaul translate 1 and 0 back to Y and N
                    //excelImportFile.ValidateDataSet.DataSet.Tables[0].Select("[Backhaul Flag] = '1'")
                    //    .ToList<DataRow>()
                    //    .ForEach(x => x["Backhaul Flag"] = "Y");
                    //excelImportFile.ValidateDataSet.DataSet.Tables[0].Select("[Backhaul Flag] = '0'")
                    //    .ToList<DataRow>()
                    //    .ForEach(x => x["Backhaul Flag"] = "N");

                    using (var ep = new ExcelPackage())
                    {
                        var ews = ep.Workbook.Worksheets.Add("LaneData");
                        ews.Cells["A1"].LoadFromDataTable(excelImportFile.ValidateDataSet.Data.Tables[0], true);
                        foreach (DataColumn column in excelImportFile.ValidateDataSet.Data.Tables[0].Columns)
                        {
                            if (column.DataType == typeof (DateTime))
                            {
                                using (
                                    var col =
                                        ews.Cells[
                                            2,
                                            column.Ordinal + 1,
                                            excelImportFile.ValidateDataSet.Data.Tables[0].Rows.Count + 1,
                                            column.Ordinal + 1])
                                {
                                    col.Style.Numberformat.Format = "dd/MM/yyyy";
                                }
                            }
                        }

                        var outputFile = new FileInfo(excelImportFile.CheckFilePath);

                        ep.SaveAs(outputFile);
                    }

                    //now set address so can be downloaded from web
                    excelImportFile.CheckFilePath = WebConfigurationManager.AppSettings["ExcelFileWeb"] + "\\" +
                                                    excelImportFile.OriginalFileName;
                }
                else
                {
                    //insert file into db
                    using (var connection = ConnectionManager.GetOpenReadOnlyConnection(1))
                    {
                        using (var transaction = TransactionManager.GetTransaction(connection))
                        {
                            excelImportFile.StoredFileName = Guid.NewGuid();
                            var importLogId = _iExcelUploadRepository.InsertImportLog(transaction, excelImportFile);
                            var columns = _excelBc.GetColumnDefinitionFromExcel(definitionFile);

                            var file = GetImportLoadDataSet(
                                excelImportFile.ValidateDataSet.TypedData.Tables[0],
                                columns,
                                importLogId);
                            var _bulkImporter = new BulkImporter();
                            excelImportFile.NumberOfRecords = _bulkImporter.BulkInsertFromFile(
                                file,
                                transaction,
                                "temp.importload");

                            transaction.Commit();

                            //store file for records
                            using (var ep = new ExcelPackage())
                            {
                                var ews = ep.Workbook.Worksheets.Add("LaneData");
                                ews.Cells["A1"].LoadFromDataTable(
                                    excelImportFile.ValidateDataSet.TypedData.Tables[0],
                                    true);
                                var outputFile =
                                    new FileInfo(
                                        WebConfigurationManager.AppSettings["ExcelFileDirectory"] + "\\" +
                                        excelImportFile.StoredFileName + ".xlsx");

                                ep.SaveAs(outputFile);
                            }
                        }
                    }
                }

                //delete temp file
                if (path != string.Empty)
                {
                    if (File.Exists(path))
                    {
                        File.Delete(path);
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, excelImportFile);
            }
            catch (Exception ex)
            {
                if (excelImportFile.ValidateDataSet == null)
                {
                    excelImportFile.ValidateDataSet = new ValidatedDataSet();
                }
                if (excelImportFile.ValidateDataSet.Violations == null)
                {
                    excelImportFile.ValidateDataSet.Violations = new List<string>();
                }
                excelImportFile.ValidateDataSet.Violations.Add("Unable to load file.  Please contact administrator.");
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Request.CreateResponse(HttpStatusCode.OK, excelImportFile);
        }

        #endregion

        #region Private Methods

        private string GetDeserializedFileName(MultipartFileData fileData)
        {
            var fileName = GetFileName(fileData);
            return JsonConvert.DeserializeObject(fileName).ToString();
        }

        private FileInfo GetImportLoadDataSet(DataTable dt, List<ColumnDefinition> columns, int logId)
        {
            var fileName = Path.GetTempFileName();

            var file = new FileInfo(fileName);
            using (var fileStream = File.Open(fileName, FileMode.Truncate, FileAccess.Write))
            {
                using (var streamWriter = new StreamWriter(fileStream, Encoding.UTF8))
                {
                    using (var csvWriter = new CsvWriter(streamWriter))
                    {
                        csvWriter.WriteField("ImportLog_ID");
                        foreach (var col in columns)
                        {
                            csvWriter.WriteField(col.SqlFieldName);
                        }
                        csvWriter.NextRecord();
                        foreach (DataRow datum in dt.Rows)
                        {
                            csvWriter.WriteField(logId);
                            foreach (var col in columns)
                            {
                                if (col.SqlType == "System.DateTime")
                                {
                                    DateTime date;
                                    if (DateTime.TryParse(datum[col.Name].ToString(), out date))
                                    {
                                        csvWriter.WriteField(date.ToString("yyyy/M/d"));
                                    }
                                    else
                                    {
                                        csvWriter.WriteField("");
                                    }
                                }

                                else
                                {
                                    csvWriter.WriteField(datum[col.Name]);
                                }
                            }
                            csvWriter.NextRecord();
                        }
                    }
                }
            }
            return file;
        }

        private LookupList GetTempProtectLookupList()
        {
            var tempprotect = new LookupList();
            tempprotect.Type = "tempprotect";
            tempprotect.Values = new List<string>();
            tempprotect.Values.Add("D");
            tempprotect.Values.Add("F");
            tempprotect.Values.Add("R");

            return tempprotect;
        }

        #endregion
    }
}