﻿using System.Web.Http;
using AS.Crossbow.EntryPages.Common;
using AS.Crossbow.EntryPages.Controllers.Helpers.Interfaces;
using AS.Crossbow.EntryPages.Models;
using AS.Framework.Common.Repositories;
using AS.Framework.Permissions.Common;
using AS.Framework.WebApi.Attributes;

namespace AS.Crossbow.EntryPages.Controllers
{
    [AimsWebApiAuthorize(Constants.PermissionCode.NETWORK_PAGE, PermissionEnums.PermissionType.Read)]
    [RoutePrefix("api/calendar")]
    public class CalendarController : ApiController
    {
        #region Member Variables

        private readonly ICalendarHelper _calendarHelper;
        private readonly IControllerHelper _controllerHelper;
        private int? _tenantId;

        #endregion

        #region Constructors

        public CalendarController(ICalendarHelper calendarHelper, IControllerHelper controllerHelper)
        {
            _calendarHelper = calendarHelper;
            _controllerHelper = controllerHelper;
        }

        #endregion

        #region Properties

        private int TenantId
        {
            get
            {
                if (!_tenantId.HasValue)
                {
                   _tenantId = _controllerHelper.GetTenantId();
                }
                return _tenantId.Value;
            }
        }

        #endregion

        #region Controller Actions

        [HttpGet]
        [Route("default")]
        public Calendar GetDefaultCalendar()
        {
            return _calendarHelper.GetDefaultCalendar(TenantId);
        }

        #endregion
    }
}