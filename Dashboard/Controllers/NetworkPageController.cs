﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using AS.Crossbow.EntryPages.Common;
using AS.Crossbow.EntryPages.Controllers.Helpers.Interfaces;
using AS.Crossbow.EntryPages.Models;
using AS.Crossbow.EntryPages.Models.BusinessObjects;
using AS.Crossbow.EntryPages.Models.NetworkPage;
using AS.Crossbow.EntryPages.Models.NetworkPage.PillSummary;
using AS.Framework.Common.Repositories;
using AS.Framework.Permissions.Common;
using AS.Framework.WebApi.Attributes;

namespace AS.Crossbow.EntryPages.Controllers
{
    [AimsWebApiAuthorize(Constants.PermissionCode.NETWORK_PAGE, PermissionEnums.PermissionType.Read)]
    [RoutePrefix("api/network")]
    public class NetworkPageController : ApiController
    {
        #region Member Variables

        private readonly INetworkPageHelper _networkPageHelper;
        private readonly int _tenantId;

        #endregion

        #region Constructors

        public NetworkPageController(IControllerHelper controllerHelper, INetworkPageHelper networkPageHelper)
        {
            _networkPageHelper = networkPageHelper;

            _tenantId = controllerHelper.GetTenantId();
        }

        #endregion

        #region Controller Actions

        [HttpGet]
        [Route("dcs")]
        public List<DistributionCenter> GetDistributionCenters()
        {
            return _networkPageHelper.GetDistributionCenters(_tenantId);
        }

        [HttpGet]
        [Route("summary")]
        public NetworkSummary GetNetworkSummary(
            int? dcId,
            DateTime? currentStartPeriodStartDate,
            DateTime? currentEndPeriodStartDate)
        {
            return _networkPageHelper.GetNetworkSummary(
                _tenantId,
                dcId,
                currentStartPeriodStartDate,
                currentEndPeriodStartDate);
        }

        [HttpGet]
        [Route("pillSummaries")]
        public PillSummaries GetPillSummaries(
            int? dcId,
            DateTime startingPeriodStartDate,
            DateTime endingPeriodStartDate)
        {
            return _networkPageHelper.GetPillSummaries(_tenantId, dcId, startingPeriodStartDate, endingPeriodStartDate);
        }

        [HttpGet]
        [Route("savingsDriverLanes")]
        public PagedResponse<SavingsDriverLane> GetSavingsDriverLanesByDriver(
            int start,
            int end,
            string sort,
            string order = "asc",
            int limit = 10,
            int offset = 0,
            int? dcId = null)
        {
            return _networkPageHelper.GetSavingsDriverLanesByType(
                start:start,
                end: end,
                dcId:dcId,
                tenantId: _tenantId,
                sort: sort,
                order: order,
                limit: limit,
                offset: offset
               ); 
        }

        #endregion
    }
}