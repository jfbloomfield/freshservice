﻿using System.Web.Mvc;
using AS.Crossbow.Core.Common;
using Elmah;

namespace AS.Crossbow.EntryPages.Controllers
{
    [AllowAnonymous]
    public class ErrorController : Controller
    {
        #region Controller Actions

        public ActionResult LogJavaScriptError(string message)
        {
            ErrorSignal.FromCurrentContext().Raise(new JavaScriptException(message));

            return new ContentResult();
        }

        #endregion
    }
}