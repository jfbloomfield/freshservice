﻿using System.Collections.Generic;
using System.IO;
using System.Web.Optimization;
using AS.Framework.MVC.Helpers;

namespace AS.Freshservice.Dashboard
{
    public class BundleConfig
    {
        #region Public Methods

        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(
                new ScriptBundle("~/bundles/js/preload").Include(
                    "~/clientlib/jquery/js/jquery.min.js",
                    "~/arrowstream/framework/as-logging.js"));

            var vendorScriptBundle = new Bundle("~/bundles/js/vendor") {Orderer = new AsIsBundleOrderer()};
            vendorScriptBundle.Include(
                "~/clientlib/jquery-ui/js/jquery-ui.js",
                "~/clientlib/jquery-cookie/js/jquery.cookie.js",
                "~/clientlib/angular/js/angular.js",
                "~/clientlib/angular-route/js/angular-route.js",
                "~/clientlib/angular-jwt/js/angular-jwt.js",
                "~/clientlib/angular-local-storage/js/angular-local-storage.js",
                "~/clientlib/angular-animate/js/angular-animate.js",
                "~/clientlib/angular-sanitize/js/angular-sanitize.js",
                "~/clientlib/angular-ui-router/js/angular-ui-router.js",
                "~/clientlib/angular-bootstrap/js/ui-bootstrap-tpls.js",
                "~/clientlib/angular-permission/js/angular-permission.js",
                "~/clientlib/angular-toggle-switch/js/angular-toggle-switch.js",
                "~/clientlib/moment/js/moment.js",
                "~/clientlib/gsap/js/tweenmax.js",
                "~/clientlib/numeral/js/numeral.js",
                "~/clientlib/microplugin/js/microplugin.js",
                "~/clientlib/sifter/js/sifter.js",
                "~/clientlib/selectize/js/selectize.js",
                "~/clientlib/bootstrap/js/bootstrap.js",
                "~/clientlib/bootstrap-table/js/bootstrap-table.js",
                "~/clientlib/bootbox/js/bootbox.js",
                "~/clientlib/nya-bootstrap-select/js/nya-bs-select.js",
                "~/clientlib/d3/js/d3.js",
                "~/clientlib/d3-tip/js/index.js",
                "~/clientlib/blocks.logger/js/as.blocks.logger.js",
                "~/clientlib/blocks.formatter/js/as.blocks.formatter.js",
                "~/clientlib/blocks.router/js/as.blocks.router.js",
                "~/clientlib/blocks.templateHelper/js/as.blocks.templatehelper.js",
                "~/clientlib/blocks.asTracking/js/as.blocks.astracking.js",
                "~/clientlib/blocks.appInsights/js/as.blocks.appinsights.js",
                "~/clientlib/ng-file-upload/js/ng-file-upload.js",
                "~/clientlib/rxjs/js/rx.all.js",
                "~/clientlib/angular-rx/js/rx.angular.js");

            bundles.Add(vendorScriptBundle);

            bundles.Add(new ScriptBundle("~/bundles/js/app").Include("~/target/network/main.js"));

            bundles.Add(new ScriptBundle("~/bundles/js/program").Include
                (
                    "~/node_modules/es6-shim/es6-shim.min.js",
                    "~/node_modules/systemjs/dist/system-polyfills.js",
                    "~/node_modules/angular2/es6/dev/src/testing/shims_for_IE.js",
                    "~/node_modules/angular2/bundles/angular2-polyfills.js",
                    "~/node_modules/systemjs/dist/system.src.js",
                    "~/node_modules/rxjs/bundles/Rx.js",
                    "~/node_modules/angular2/bundles/angular2.dev.js",
                    "~/node_modules/lodash/lodash.js"
                ));
            bundles.Add(new ScriptBundle("~/bundles/js/bower-program").Include
                (
                    "~/clientlib/jquery/js/jquery.min.js",
                    "~/clientlib/sifter/js/sifter.js",
                    "~/clientlib/microplugin/js/microplugin.js",
                    "~/clientlib/selectize/js/selectize.js",
                    "~/clientlib/gsap/js/TweenMax.js",
                    "~/clientlib/jquery-cookie/js/jquery.cookie.js",
                    "~/clientlib/bootstrap/js/bootstrap.js"
                ));

            bundles.Add(
                new ScriptBundle("~/bundles/js/framework").Include(
                    "~/arrowstream/framework/as-session-timeout.js",
                    "~/arrowstream/framework/jquery.filter_input.js",
                    "~/arrowstream/framework/as-framework-mvc.js"));

            bundles.Add(new ScriptBundle("~/bundles/html/templates").IncludeDirectory("~/app", "*.html", true));

            bundles.Add(
                new StyleBundle("~/bundles/css/jquery").Include(
                    "~/clientlib/jquery-ui/css/jquery-ui.css",
                    new CssRewriteUrlTransformHelper()));

            bundles.Add(
                new StyleBundle("~/bundles/css/bootstrap-table").Include(
                    "~/clientlib/bootstrap-table/css/bootstrap-table.css",
                    new CssRewriteUrlTransformHelper()));

            bundles.Add(
                new StyleBundle("~/bundles/css/angular-toggle-switch").Include(
                    "~/clientlib/angular-toggle-switch/css/angular-toggle-switch.css",
                    new CssRewriteUrlTransformHelper()));

            bundles.Add(
                new StyleBundle("~/bundles/css/main-bootstrap").Include(
                    "~/content/css/main-bootstrap.css",
                    new CssRewriteUrlTransformHelper()));

            bundles.Add(
                new StyleBundle("~/bundles/css/main-as").Include(
                    "~/content/css/main-as.css",
                    new CssRewriteUrlTransformHelper()));

            bundles.Add(
                new StyleBundle("~/bundles/css/nya-bs-select").Include(
                    "~/clientlib/nya-bootstrap-select/css/nya-bs-select.css",
                    new CssRewriteUrlTransformHelper()));

            bundles.Add(
                new StyleBundle("~/bundles/css/font-awesome").Include(
                    "~/clientlib/components-font-awesome/css/font-awesome.css",
                    new CssRewriteUrlTransformHelper()));
        }

        #endregion
    }

    public class AsIsBundleOrderer : IBundleOrderer
    {
        #region IBundleOrderer Members

        public IEnumerable<BundleFile> OrderFiles(BundleContext context, IEnumerable<BundleFile> files)
        {
            return files;
        }

        #endregion

        #region Public Methods

        public virtual IEnumerable<FileInfo> OrderFiles(BundleContext context, IEnumerable<FileInfo> files)
        {
            return files;
        }

        #endregion
    }
}