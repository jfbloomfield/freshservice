﻿using System.Web.Http;
using System.Web.Http.ExceptionHandling;
using AS.Freshservice.Dashboard.DependencyResolution;
using AS.Framework.Common.Helpers;
using AS.Framework.WebApi.Attributes;
using Elmah.Contrib.WebApi;
using Microsoft.Owin.Security.OAuth;

namespace AS.Freshservice.Dashboard
{
    public static class WebApiConfig
    {
        #region Properties

        public static string UrlPrefix
        {
            get { return "api"; }
        }

        public static string UrlPrefixRelative
        {
            get { return "~/api"; }
        }

        #endregion

        #region Public Methods

        public static void Register(HttpConfiguration config)
        {
            //config.SuppressDefaultHostAuthentication();
            //config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));

            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                "DefaultApi",
                UrlPrefix + "/{controller}/{action}/{id}",
                new { action = RouteParameter.Optional, id = RouteParameter.Optional });

           
            config.Filters.Add(new AimsWebApiAuthenticationAttribute(new SecurityHelper()));
            config.Filters.Add(new AuthorizeAttribute());

            config.Services.Add(typeof (IExceptionLogger), new ElmahExceptionLogger());

            var container = IoC.Initialize();
            config.DependencyResolver = new StructureMapResolver(container);

            config.Formatters.JsonFormatter.UseDataContractJsonSerializer = false;
        }

        #endregion
    }
}