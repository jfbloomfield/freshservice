﻿using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;

namespace AS.Freshservice.Dashboard
{
    public class RouteConfig
    {
        #region Public Methods

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapRoute(
                "Logout",
                "Logout",
                new { controller = "Dashboard", action = "Logout", id = UrlParameter.Optional });
            routes.MapRoute(
                "Default",
                "{controller}/{action}/{id}",
                new {controller = "Dashboard", action = "Index", id = UrlParameter.Optional});
        }

        #endregion
    }
}