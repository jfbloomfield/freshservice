﻿using AS.Crossbow.Core.Data;
using AS.Crossbow.EntryPages.Repositories.Interfaces;

namespace AS.Crossbow.EntryPages.Repositories
{
    public class ProgramRepository : BaseRepository, IProgramRepository
    {
        #region IProgramRepository Members

        public int? GetCurrentYearCaseCount(int tenantId)
        {
            int? ret = null;
            var sqlAll = @"
SELECT SUM(Cases)
    FROM
        core.shipment
        INNER JOIN (
            SELECT MIN(cd.StartDate) AS MinStartDate, MAX(cd.EndDate) AS MaxEndDate
                FROM
                    core.calendardetail AS cd
                    INNER JOIN core.calendar AS c
                        ON c.ID = cd.Calendar_ID
                WHERE
                    c.Tenant_ID = @tenantId
                    AND c.IsDefault = true
                    AND cd.StartDate <= CURRENT_DATE()
                    AND cd.`Year` = (
                        SELECT `Year`
                            FROM
                                core.calendardetail AS xcd
                                INNER JOIN core.calendar AS xc
                                    ON xc.ID = xcd.Calendar_ID
                            WHERE
                                xc.Tenant_ID = @tenantId
                                AND xc.IsDefault = true
                                AND xcd.StartDate <= CURRENT_DATE()
                                AND xcd.EndDate >= CURRENT_DATE()
                    )
        ) AS x
    WHERE
        shipment.Tenant_ID = @tenantId
        AND shipment.DeliveryDate >= x.MinStartDate
        AND shipment.DeliveryDate <= x.MaxEndDate;
";
            using (var connection = ConnectionManager.GetOpenReadOnlyConnection(tenantId))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (var command = transaction.CreateCommand())
                    {
                        command.CommandText = sqlAll;
                        command.AddInputParameter("tenantId", tenantId);

                        using (var reader = ExecuteSafeDataReader(command))
                        {
                            if (reader.Read())
                            {
                                ret = reader.GetInt32(0);
                            }
                        }
                    }
                }
            }

            return ret;
        }

        public int? GetCurrentYearNetworkCaseCount(int tenantId)
        {
            int? ret = null;
            var sqlAll = @"
SELECT SUM(Cases)
    FROM datamart.pillnetwork
    WHERE
        pillnetwork.Tenant_ID = @tenantId
        AND pillnetwork.CalendarDetail_ID IN (
            SELECT cd.ID
                FROM
                    core.calendardetail AS cd
                    INNER JOIN core.calendar AS c
                        ON c.ID = cd.Calendar_ID
                WHERE
                    c.Tenant_ID = @tenantId
                    AND c.IsDefault = true
                    AND cd.StartDate <= CURRENT_DATE()
                    AND cd.`Year` = (
                        SELECT `Year`
                            FROM
                                core.calendardetail AS xcd
                                INNER JOIN core.calendar AS xc
                                    ON xc.ID = xcd.Calendar_ID
                            WHERE
                                xc.Tenant_ID = @tenantId
                                AND xc.IsDefault = true
                                AND xcd.StartDate <= CURRENT_DATE()
                                AND xcd.EndDate >= CURRENT_DATE()
                    )
        );
";
            using (var connection = ConnectionManager.GetOpenReadOnlyConnection(tenantId))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (var command = transaction.CreateCommand())
                    {
                        command.CommandText = sqlAll;
                        command.AddInputParameter("tenantId", tenantId);

                        using (var reader = ExecuteSafeDataReader(command))
                        {
                            if (reader.Read())
                            {
                                ret = reader.GetInt32(0);
                            }
                        }
                    }
                }
            }

            return ret;
        }

        #endregion
    }
}