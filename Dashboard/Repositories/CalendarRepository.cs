﻿using System.Collections.Generic;
using AS.Crossbow.Core.Data;
using AS.Crossbow.EntryPages.Models;
using AS.Crossbow.EntryPages.Repositories.Interfaces;

namespace AS.Crossbow.EntryPages.Repositories
{
    public class CalendarRepository : BaseRepository, ICalendarRepository
    {
        #region ICalendarRepository Members

        public Calendar GetDefaultCalendar(int tenantId)
        {
            Calendar calendar = null;
            var periods = new List<CalendarDetail>();
            var sql = @"
SELECT
         c.ID AS Calendar_ID
        ,c.Tenant_ID
        ,c.Name AS Calendar_Name
        ,c.IsDefault AS Calendar_IsDefault
        ,cd.ID AS CalendarDetail_ID
        ,cd.Name AS CalendarDetail_Name
        ,cd.Abbreviation AS CalendarDetail_Abbreviation
        ,cd.Period AS CalendarDetail_Period
        ,cd.Year AS CalendarDetail_Year
        ,cd.StartDate AS CalendarDetail_StartDate
        ,cd.EndDate AS CalendarDetail_EndDate
    FROM
        core.calendar AS c
        INNER JOIN core.calendardetail AS cd ON c.ID = cd.Calendar_ID
    WHERE
        c.Tenant_ID = @tenantId
        AND c.IsDefault = 1
    ORDER BY cd.StartDate
";

            using (var connection = ConnectionManager.GetOpenReadOnlyConnection(tenantId))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (var command = transaction.CreateCommand())
                    {
                        command.CommandText = sql;
                        command.AddInputParameter("tenantId", tenantId);

                        using (var reader = ExecuteSafeDataReader(command))
                        {
                            while (reader.Read())
                            {
                                periods.Add(MapToCalendarDetail(reader));
                                calendar = MapToCalendar(reader, periods);
                            }
                        }
                    }
                }
            }

            return calendar;
        }

        #endregion

        #region Private Methods

        private Calendar MapToCalendar(SafeDataReader reader, IList<CalendarDetail> periods)
        {
            return new Calendar(
                name: reader.GetString("Calendar_Name"),
                id: reader.GetInt32("Calendar_ID"),
                isDefault: reader.GetBoolean("Calendar_IsDefault"),
                tenantId: reader.GetInt32("Tenant_ID"),
                periods: periods);
        }

        private CalendarDetail MapToCalendarDetail(SafeDataReader reader)
        {
            return new CalendarDetail(
                id: reader.GetInt32("CalendarDetail_ID"),
                name: reader.GetString("CalendarDetail_Name"),
                abbreviation: reader.GetString("CalendarDetail_Abbreviation"),
                calendarId: reader.GetInt32("Calendar_ID"),
                period: reader.GetInt16("CalendarDetail_Period"),
                startDate: reader.GetDateTime("CalendarDetail_StartDate"),
                endDate: reader.GetDateTime("CalendarDetail_EndDate"));
        }

        #endregion
    }
}