﻿using System.Collections.Generic;
using System.Data;
using AS.Crossbow.Core.Data;
using AS.Crossbow.EntryPages.Models.Admin;
using AS.Crossbow.EntryPages.Repositories.Interfaces;

namespace AS.Crossbow.EntryPages.Repositories
{
    public class AdminRepository : BaseRepository, IAdminRepository
    {
        #region IAdminRepository Members

        public AccountSettings GetAccountSettings(int tenantId)
        {
            var account = new AccountSettings();
            using (var connection = ConnectionManager.GetOpenReadOnlyConnection(tenantId))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (var command = transaction.CreateCommand())
                    {
                        command.CommandText =
                            "SELECT a.*, bt.Description FROM core.account a  INNER JOIN core.businesstype bt ON a.businessType_ID = bt.ID WHERE Tenant_ID = @tenant_id";
                        command.CommandType = CommandType.Text;
                        command.AddInputParameter("tenant_id", tenantId);
                        using (var reader = ExecuteSafeDataReader(command))
                        {
                            while (reader.Read())
                            {
                                Map(reader, account);
                            }
                        }
                    }
                }
            }
            return account;
        }

        public IList<BusinessType> GetBusinessTypes(int tenantId)
        {
            var businessTypes = new List<BusinessType>();
            using (var connection = ConnectionManager.GetOpenReadOnlyConnection(tenantId))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (var command = transaction.CreateCommand())
                    {
                        command.CommandText = "SELECT ID, Description FROM core.BusinessType";
                        command.CommandType = CommandType.Text;
                        using (var reader = ExecuteSafeDataReader(command))
                        {
                            while (reader.Read())
                            {
                                var type = new BusinessType();
                                type.Id = reader.GetInt32("Id");
                                type.Description = reader.GetString("Description");
                                businessTypes.Add(type);
                            }
                        }
                    }
                }
            }
            return businessTypes;
        }

        public string GetDBServerNameByTenantId(int tenantId)
        {
            var serverName = string.Empty;
            using (var connection = ConnectionManager.GetOpenReadOnlyConnection(tenantId))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (var command = transaction.CreateCommand())
                    {
                        command.CommandText = "SELECT DBServerName FROM core.tenant WHERE ID = @tenant_id";
                        command.CommandType = CommandType.Text;
                        command.AddInputParameter("tenant_id", tenantId);
                        using (var reader = ExecuteSafeDataReader(command))
                        {
                            while (reader.Read())
                            {
                                serverName = reader.GetString("DBServerName");
                            }
                        }
                    }
                }
            }
            return serverName;
        }

        public IList<string> GetStates(int tenantId)
        {
            var states = new List<string>();
            using (var connection = ConnectionManager.GetOpenReadOnlyConnection(tenantId))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (var command = transaction.CreateCommand())
                    {
                        command.CommandText = "SELECT Abbreviation FROM core.state";
                        command.CommandType = CommandType.Text;
                        using (var reader = ExecuteSafeDataReader(command))
                        {
                            while (reader.Read())
                            {
                                states.Add(reader.GetString("Abbreviation"));
                            }
                        }
                    }
                }
            }
            return states;
        }

        public int InsertAccountSettings(int tenantId, AccountSettings accountSettings)
        {
            var id = 0;
            using (var connection = ConnectionManager.GetOpenReadOnlyConnection(tenantId))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (var command = transaction.CreateCommand())
                    {
                        command.CommandText = @"INSERT INTO core.account
(Tenant_ID, CompanyName ,BusinessType_ID,ContactName,PhoneNumber,Address1,Address2,City,State,ZipCode)
VALUES (@tenantId, @companyName, @businessTypeId, @contactName, @phoneNumber, @address1, @address2, @city, @state, @zipCode);
SELECT LAST_INSERT_ID();";
                        command.CommandType = CommandType.Text;
                        command.AddInputParameter("tenantId", tenantId);
                        command.AddInputParameter("companyName", accountSettings.CompanyName);
                        command.AddInputParameter("businessTypeId", accountSettings.BusinessType.Id);
                        command.AddInputParameter("contactName", accountSettings.ContactName);
                        command.AddInputParameter("phoneNumber", accountSettings.PhoneNumber);
                        command.AddInputParameter("address1", accountSettings.Address1);
                        command.AddInputParameter("address2", accountSettings.Address2);
                        command.AddInputParameter("city", accountSettings.City);
                        command.AddInputParameter("state", accountSettings.State);
                        command.AddInputParameter("zipCode", accountSettings.ZipCode);
                        var returnId = ExecuteScalar(command).ToString();
                        int.TryParse(returnId, out id);
                        transaction.Commit();
                        return id;
                    }
                }
            }
        }

        public void UpdateAccountSettings(int tenantId, AccountSettings accountSettings)
        {
            using (var connection = ConnectionManager.GetOpenReadOnlyConnection(tenantId))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (var command = transaction.CreateCommand())
                    {
                        command.CommandText = @"UPDATE core.account
SET CompanyName = @companyName,
BusinessType_ID = @businessTypeId,
ContactName = @contactName,
PhoneNumber = @phoneNumber,
Address1 = @address1,
Address2 = @address2,
City = @city,
State = @state,
ZipCode = @zipCode
WHERE ID = @id";
                        command.CommandType = CommandType.Text;
                        command.AddInputParameter("id", accountSettings.Id);
                        command.AddInputParameter("companyName", accountSettings.CompanyName);
                        command.AddInputParameter("businessTypeId", accountSettings.BusinessType.Id);
                        command.AddInputParameter("contactName", accountSettings.ContactName);
                        command.AddInputParameter("phoneNumber", accountSettings.PhoneNumber);
                        command.AddInputParameter("address1", accountSettings.Address1);
                        command.AddInputParameter("address2", accountSettings.Address2);
                        command.AddInputParameter("city", accountSettings.City);
                        command.AddInputParameter("state", accountSettings.State);
                        command.AddInputParameter("zipCode", accountSettings.ZipCode);
                        transaction.Commit();
                        ExecuteNonQuery(command);
                    }
                }
            }
        }

        #endregion

        #region Private Methods

        private void Map(SafeDataReader reader, AccountSettings account)
        {
            account.Id = reader.GetInt32("ID");
            account.CompanyName = reader.GetString("CompanyName");
            account.BusinessType = new BusinessType();
            account.BusinessType.Id = reader.GetInt32("BusinessType_ID");
            account.BusinessType.Description = reader.GetString("Description");
            account.ContactName = reader.GetString("ContactName");
            account.PhoneNumber = reader.GetString("PhoneNumber");
            account.Address1 = reader.GetString("Address1");
            account.Address2 = reader.GetString("Address2");
            account.City = reader.GetString("City");
            account.State = reader.GetString("State");
            account.ZipCode = reader.GetString("ZipCode");
        }

        #endregion
    }
}