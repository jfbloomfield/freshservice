﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using AS.Crossbow.Core.Data;
using AS.Crossbow.EntryPages.Models;
using AS.Crossbow.EntryPages.Models.NetworkPage;
using AS.Crossbow.EntryPages.Repositories.DTO.Network;
using AS.Crossbow.EntryPages.Repositories.Interfaces;

namespace AS.Crossbow.EntryPages.Repositories
{
    public class NetworkRepository : BaseRepository, INetworkRepository
    {
        #region Constants

        public static IList<string> GetLanesSortableColumns = new List<string>
        {
            "Lane",
            "VolumeImpact",
            "BuyingPatternImpact",
            "VolumeImpact",
            "CarrierRatesImpact",
            "FreightAllowancesImpact",
            "NewLanesImpact",
            "DiscontinuedLanesImpact",
            "FreightConsolidationImpact",
            "BackhaulUsageImpact",
            "AccessorialFeesImpact",
            "FuelImpact"
        };

        #endregion

        #region INetworkRepository Members

        public IList<Lane> GetLanes(int tenantId, IList<int> laneIds)
        {
            if (laneIds == null)
            {
                throw new ArgumentNullException(nameof(laneIds));
            }
            if (laneIds.Count <= 0)
            {
                throw new ArgumentException("Must provide a list of lane IDs", nameof(laneIds));
            }
            var ret = new List<Lane>();

            var sql =
                $@"
SELECT
         l.ID
        ,l.Shipper_ID
        ,l.Receiver_ID
        ,l.ShipFrom_Location_ID
        ,l.ShipTo_Location_ID
        ,l.Miles
        ,l.AIMS_LaneAnalysis_ID
        ,s.Name AS Shipper_Name
        ,sf.Name AS ShipFrom_Name
        ,sf.City AS ShipFrom_City
        ,sf.State AS ShipFrom_State
        ,sf.PostalCode AS ShipFrom_PostalCode
        ,st.Name AS ShipTo_Name
        ,st.City AS ShipTo_City
        ,st.State AS ShipTo_State
        ,st.PostalCode AS ShipTo_PostalCode
    FROM
        core.lane AS l
        INNER JOIN core.shipper AS s ON l.Shipper_ID = s.ID
        INNER JOIN core.location AS sf ON l.ShipFrom_Location_ID = sf.ID
        INNER JOIN core.location AS st ON l.ShipTo_Location_ID = st.ID
    WHERE
        l.Tenant_ID = @tenantId
        AND l.ID IN ({string
                    .Join(",", laneIds)});
";
            using (var connection = ConnectionManager.GetOpenReadOnlyConnection(tenantId))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (var command = transaction.CreateCommand())
                    {
                        command.CommandText = sql;
                        command.AddInputParameter("tenantId", tenantId);

                        using (var reader = ExecuteSafeDataReader(command))
                        {
                            while (reader.Read())
                            {
                                ret.Add(MapToLane(reader));
                            }
                        }
                    }
                }
            }

            return ret;
        }

        public PagedResponse<LaneSavingsImpactDto> GetLanesBySavingsImpact(
            int tenantId,
            IList<int> calendarDetailIds,
            int? receiverId = null,
            int offset = 0,
            int limit = 10,
            string sortColumn = "VolumeImpact",
            string sortOrder = "asc")
        {
            ValidateSortColumn(sortColumn);
            ValidateSortorder(sortOrder);
            var lanes = new List<LaneSavingsImpactDto>();
            var totalRows = 0;
            if (calendarDetailIds.Count <= 0)
            {
                return new PagedResponse<LaneSavingsImpactDto>(totalRows, lanes);
            }

            // lane.Vendor.Name + "</a><br/>" + lane.ShipFrom.City + " " + lane.ShipFrom.State + " to " + lane.ShipTo.City + " " + lane.ShipTo.State
            var sql =
                $@"
SELECT SQL_CALC_FOUND_ROWS
         p.Tenant_ID
        ,p.Lane_ID
        ,SUM(p.{sortColumn}) AS Impact
    FROM
        datamart.pilllane AS p
    WHERE
        p.Tenant_ID = @tenantId
        AND (p.Receiver_ID = @receiverId OR @receiverId IS NULL)
        AND p.CalendarDetail_ID IN ({string
                    .Join(",", calendarDetailIds)})
    GROUP BY
         p.Tenant_ID
        ,p.Lane_ID
    HAVING ABS(SUM(p.{sortColumn})) >= 1
    ORDER BY SUM(p.{sortColumn}) {sortOrder}
    LIMIT {offset},{limit};

SELECT FOUND_ROWS();
";
            using (var connection = ConnectionManager.GetOpenReadOnlyConnection(tenantId))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (var command = transaction.CreateCommand())
                    {
                        command.CommandText = sql;
                        command.AddInputParameter("tenantId", tenantId);
                        if (receiverId.HasValue)
                        {
                            command.AddInputParameter("receiverId", receiverId.Value);
                        }
                        else
                        {
                            command.AddInputParameter("receiverId", DBNull.Value);
                        }

                        using (var reader = ExecuteSafeDataReader(command))
                        {
                            while (reader.Read())
                            {
                                lanes.Add(MapToLaneSavingsImpactDto(reader));
                            }

                            if (reader.NextResult() && reader.Read())
                            {
                                totalRows = reader.GetInt32(0);
                            }
                        }
                    }
                }
            }

            return new PagedResponse<LaneSavingsImpactDto>(totalRows, lanes);
        }

        public IEnumerable<NewAndDiscontinuedLaneDto> GetNewAndDiscontinuedLaneDtos(
            int tenantId,
            IList<int> periods,
            int? receiverId = null)
        {
            var ret = new List<NewAndDiscontinuedLaneDto>();
            var sqlAll =
                $@"
SELECT
         p.Tenant_ID
        ,p.Lane_ID
        ,p.CalendarDetail_ID
        ,p.NewLanesCount
        ,p.DiscontinuedLanesCount
    FROM
        datamart.pilllane AS p
    WHERE
        p.Tenant_ID = @tenantId
        AND (p.NewLanesCount = 1 OR p.DiscontinuedLanesCount = 1)
        AND p.CalendarDetail_ID IN ({string
                    .Join(",", periods)})
        AND (p.Receiver_ID = @receiverId OR @receiverId IS NULL)
";
            using (var connection = ConnectionManager.GetOpenReadOnlyConnection(tenantId))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (var command = transaction.CreateCommand())
                    {
                        command.CommandText = sqlAll;
                        command.AddInputParameter("tenantId", tenantId);
                        if (receiverId.HasValue)
                        {
                            command.AddInputParameter("receiverId", receiverId.Value);
                        }
                        else
                        {
                            command.AddInputParameter("receiverId", DBNull.Value);
                        }

                        using (var reader = ExecuteSafeDataReader(command))
                        {
                            while (reader.Read())
                            {
                                ret.Add(MapToNewAndDiscontinuedLaneDto(reader));
                            }
                        }
                    }
                }
            }

            return ret;
        }

        public PillsSummaryDto GetPillsSummaryDto(
            int tenantId,
            int? receiverId,
            DateTime startPeriodStartDate,
            DateTime endPeriodStartDate)
        {
            using (var connection = ConnectionManager.GetOpenWritableConnection(tenantId))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (var command = transaction.CreateCommand())
                    {
                        command.CommandText = "datamart.spPillSummary";
                        command.CommandType = CommandType.StoredProcedure;
                        command.AddInputParameter("$tenantId", tenantId);
                        if (receiverId.HasValue)
                        {
                            command.AddInputParameter("$receiverId", receiverId.Value);
                        }
                        else
                        {
                            command.AddInputParameter("$receiverId", null);
                        }

                        command.AddInputParameter("$startDate", startPeriodStartDate);
                        command.AddInputParameter("$endDate", endPeriodStartDate);

                        using (var reader = ExecuteSafeDataReader(command))
                        {
                            if (reader.Read())
                            {
                                var dto = new PillsSummaryDto();
                                Map(reader, dto);
                                return dto;
                            }
                        }
                    }
                }
            }
            return null;
        }

        public IEnumerable<ReceiverDto> GetReceiverDtos(int tenantId)
        {
            var dtos = new List<ReceiverDto>();
            using (var connection = ConnectionManager.GetOpenReadOnlyConnection(tenantId))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (var command = transaction.CreateCommand())
                    {
                        command.CommandText = @"
SELECT
    r.ID,
    r.Number,
    r.Name,
    IF(IFNULL(pr.nbr, 0) > 0, 1, 0) AS HasPillReceiverRecords
FROM core.receiver r
LEFT JOIN (
    SELECT Receiver_id, COUNT(*) AS nbr
    FROM datamart.pillreceiver
    WHERE Tenant_ID = @tenantId
    GROUP BY Receiver_id
) pr
    ON pr.Receiver_id = r.ID
WHERE r.Tenant_Id = @tenantId
ORDER BY r.Name";
                        command.AddInputParameter("tenantId", tenantId);

                        using (var reader = ExecuteSafeDataReader(command))
                        {
                            while (reader.Read())
                            {
                                dtos.Add(
                                    new ReceiverDto
                                    {
                                        Id = reader.GetInt32("ID"),
                                        Number = reader.GetString("Number"),
                                        Name = reader.GetString("Name"),
                                        HasPillReceiverRecords = reader.GetBoolean("HasPillReceiverRecords")
                                    });
                            }
                        }
                    }
                }
            }
            return dtos;
        }

        public IList<SavingsDto> GetSavings(int tenantId, int receiverId, DateTime minDate, DateTime maxDate)
        {
            var dtos = new List<SavingsDto>();

            var singleReceiverSql = @"
SELECT 
  p.Savings,
  p.ShipmentCount,
  c.Name,
  c.Abbreviation,
  c.StartDate,
  c.EndDate,
  c.Id AS CalendarDetailId,
  c.Period,
  c.Year
FROM core.calendardetail c
INNER JOIN datamart.pillreceiver p
  ON p.CalendarDetail_ID = c.ID
  AND p.Tenant_ID = @tenantId
  AND p.Receiver_id = @receiverId
WHERE c.StartDate <= @maxDate
AND c.EndDate >= @minDate
ORDER BY c.StartDate";

            using (var connection = ConnectionManager.GetOpenReadOnlyConnection(tenantId))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (var command = transaction.CreateCommand())
                    {
                        command.CommandText = singleReceiverSql;
                        command.AddInputParameter("receiverId", receiverId);
                        command.AddInputParameter("tenantId", tenantId);
                        command.AddInputParameter("minDate", minDate);
                        command.AddInputParameter("maxDate", maxDate);

                        using (var reader = ExecuteSafeDataReader(command))
                        {
                            while (reader.Read())
                            {
                                var dto = new SavingsDto();
                                Map(reader, dto);
                                dtos.Add(dto);
                            }
                        }
                    }
                }
            }
            return dtos;
        }

        public IList<SavingsDto> GetSavings(int tenantId, DateTime minDate, DateTime maxDate)
        {
            var dtos = new List<SavingsDto>();

            var allReceiversSql = @"
SELECT 
  p.Savings,
  p.ShipmentCount,
  c.Name,
  c.Abbreviation,
  c.StartDate,
  c.EndDate,
  c.Id AS CalendarDetailId,
  c.Period,
  c.Year
FROM core.calendardetail c
INNER JOIN datamart.pillnetwork p
  ON p.CalendarDetail_ID = c.ID
  AND p.Tenant_ID = @tenantId
WHERE c.StartDate <= @maxDate
AND c.EndDate >= @minDate
ORDER BY c.StartDate";

            using (var connection = ConnectionManager.GetOpenReadOnlyConnection(tenantId))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (var command = transaction.CreateCommand())
                    {
                        command.CommandText = allReceiversSql;
                        command.AddInputParameter("tenantId", tenantId);
                        command.AddInputParameter("minDate", minDate);
                        command.AddInputParameter("maxDate", maxDate);

                        using (var reader = ExecuteSafeDataReader(command))
                        {
                            while (reader.Read())
                            {
                                var dto = new SavingsDto();
                                Map(reader, dto);
                                dtos.Add(dto);
                            }
                        }
                    }
                }
            }
            return dtos;
        }

        public IList<SavingsDriverPillLanePeriod> GetSavingsDriverPillLanePeriods(
            int tenantId,
            int? receiverId,
            IList<int> calendarDetailIds)
        {
            var ret = new List<SavingsDriverPillLanePeriod>();
            if (calendarDetailIds.Count <= 0)
            {
                return ret;
            }

            var sqlAll =
                $@"
SELECT
         p.Tenant_ID
        ,p.Lane_ID
        ,p.CalendarDetail_ID
        ,p.ShipmentCount
        ,p.WeightPerShipment
        ,p.BuyingPatternImpact
        ,p.VolumeImpact
        ,p.Weight
        ,p.CarrierRatesImpact
        ,p.RatesTLPerMile
        ,p.FreightAllowancesImpact
        ,p.AllowancePerCWT
        ,p.NewLanesImpact
        ,p.NewLanesCount
        ,p.DiscontinuedLanesImpact
        ,p.DiscontinuedLanesCount
        ,p.Savings
        ,p.FreightConsolidationImpact
        ,p.LoadCount
        ,p.LoadCountMultiStop
        ,p.BackhaulUsageImpact
        ,p.LoadCountBackhaul
        ,p.AccessorialFeesImpact
        ,p.AccessorialFeesPerLoad
        ,p.FuelImpact
        ,p.FuelPercentCost
        ,p.ShipmentCountTL
        ,p.ShipmentCountIM
        ,p.ShipmentCountLTL
    FROM
        datamart.pilllane AS p
    WHERE
        p.Tenant_ID = @tenantId
        AND p.CalendarDetail_ID IN ({string
                    .Join(",", calendarDetailIds)})
        AND (p.Receiver_ID = @receiverId OR @receiverId IS NULL)
";
            using (var connection = ConnectionManager.GetOpenReadOnlyConnection(tenantId))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (var command = transaction.CreateCommand())
                    {
                        command.CommandText = sqlAll;
                        command.AddInputParameter("tenantId", tenantId);
                        if (receiverId.HasValue)
                        {
                            command.AddInputParameter("receiverId", receiverId.Value);
                        }
                        else
                        {
                            command.AddInputParameter("receiverId", DBNull.Value);
                        }

                        using (var reader = ExecuteSafeDataReader(command))
                        {
                            while (reader.Read())
                            {
                                ret.Add(MapToPillLanePeriod(reader));
                            }
                        }
                    }
                }
            }

            return ret;
        }

        public IList<SavingsDriverPillLanePeriod> GetSavingsDriverPillLanePeriods(
            int tenantId,
            IList<int> laneIds,
            IList<int> calendarDetailIds)
        {
            var ret = new List<SavingsDriverPillLanePeriod>();
            if (laneIds.Count <= 0 || calendarDetailIds.Count <= 0)
            {
                return ret;
            }

            var sqlAll =
                $@"
SELECT
         p.Tenant_ID
        ,p.Lane_ID
        ,p.CalendarDetail_ID
        ,p.ShipmentCount
        ,p.WeightPerShipment
        ,p.BuyingPatternImpact
        ,p.VolumeImpact
        ,p.Weight
        ,p.CarrierRatesImpact
        ,p.RatesTLPerMile
        ,p.FreightAllowancesImpact
        ,p.AllowancePerCWT
        ,p.NewLanesImpact
        ,p.NewLanesCount
        ,p.DiscontinuedLanesImpact
        ,p.DiscontinuedLanesCount
        ,p.Savings
        ,p.FreightConsolidationImpact
        ,p.LoadCount
        ,p.LoadCountMultiStop
        ,p.BackhaulUsageImpact
        ,p.LoadCountBackhaul
        ,p.AccessorialFeesImpact
        ,p.AccessorialFeesPerLoad
        ,p.FuelImpact
        ,p.FuelPercentCost
        ,p.ShipmentCountTL
        ,p.ShipmentCountIM
        ,p.ShipmentCountLTL
    FROM
        datamart.pilllane AS p
    WHERE
        p.Tenant_ID = @tenantId
        AND p.Lane_ID IN ({string
                    .Join(",", laneIds)})
        AND p.CalendarDetail_ID IN ({string.Join(",", calendarDetailIds)})
";
            using (var connection = ConnectionManager.GetOpenReadOnlyConnection(tenantId))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (var command = transaction.CreateCommand())
                    {
                        command.CommandText = sqlAll;
                        command.AddInputParameter("tenantId", tenantId);

                        using (var reader = ExecuteSafeDataReader(command))
                        {
                            while (reader.Read())
                            {
                                ret.Add(MapToPillLanePeriod(reader));
                            }
                        }
                    }
                }
            }

            return ret;
        }

        public IList<SavingsDriverPillPeriod> GetSavingsDriverPillPeriods(
            int tenantId,
            int? receiverId,
            IList<int> calendarDetailIds)
        {
            var ret = new List<SavingsDriverPillPeriod>();
            if (calendarDetailIds.Count <= 0)
            {
                return ret;
            }

            var select = @"
SELECT
         p.Tenant_ID
        ,p.CalendarDetail_ID
        ,p.WeightPerShipment
        ,p.ShipmentCount
        ,p.BuyingPatternImpact
        ,p.VolumeImpact
        ,p.Weight
        ,p.CarrierRatesImpact
        ,p.RatesTLPerMile
        ,p.FreightAllowancesImpact
        ,p.AllowancePerCWT
        ,p.NewLanesImpact
        ,p.NewLanesCount
        ,p.DiscontinuedLanesImpact
        ,p.DiscontinuedLanesCount
        ,p.Savings
        ,p.FreightConsolidationImpact
        ,p.LoadCount
        ,p.LoadCountMultiStop
        ,p.BackhaulUsageImpact
        ,p.LoadCountBackhaul
        ,p.AccessorialFeesImpact
        ,p.AccessorialFeesPerLoad
        ,p.FuelImpact
        ,p.FuelPercentCost
        ,p.ShipmentCountTL
        ,p.ShipmentCountIM
        ,p.ShipmentCountLTL
";

            var sqlAll =
                $@"
{select}
    FROM
        datamart.pillnetwork AS p
    WHERE
        p.Tenant_ID = @tenantId
        AND p.CalendarDetail_ID IN ({string
                    .Join(",", calendarDetailIds)})
";

            var sqlSpecificReceiver =
                $@"
{select}
    FROM
        datamart.pillreceiver AS p
    WHERE
        p.Tenant_ID = @tenantId
        AND p.CalendarDetail_ID IN ({string
                    .Join(",", calendarDetailIds)})
        AND p.Receiver_id = @receiverId
";
            using (var connection = ConnectionManager.GetOpenReadOnlyConnection(tenantId))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (var command = transaction.CreateCommand())
                    {
                        if (receiverId.HasValue)
                        {
                            command.CommandText = sqlSpecificReceiver;
                            command.AddInputParameter("receiverId", receiverId.Value);
                        }
                        else
                        {
                            command.CommandText = sqlAll;
                        }
                        command.AddInputParameter("tenantId", tenantId);

                        using (var reader = ExecuteSafeDataReader(command))
                        {
                            while (reader.Read())
                            {
                                ret.Add(MapToPillPeriod(reader));
                            }
                        }
                    }
                }
            }

            return ret;
        }

        #endregion

        #region Public Methods

        public static void ValidateSortColumn(string sortColumn)
        {
            var isValid =
                GetLanesSortableColumns.Any(
                    x => string.Equals(x, sortColumn, StringComparison.InvariantCultureIgnoreCase));
            if (!isValid)
            {
                throw new ArgumentException($"{sortColumn} is not a sortable column.");
            }
        }

        public static void ValidateSortorder(string sortOrder)
        {
            var isValid = string.Equals("asc", sortOrder, StringComparison.InvariantCultureIgnoreCase) ||
                          string.Equals("desc", sortOrder, StringComparison.InvariantCultureIgnoreCase);
            if (!isValid)
            {
                throw new ArgumentException($"{sortOrder} is not a valid sort order.");
            }
        }

        #endregion

        #region Private Methods

        private void Map(SafeDataReader reader, PillsSummaryDto dto)
        {
            dto._2a_VolumeImpact = reader.GetDecimal("2a_VolumeImpact");
            dto._2b_WeightEnd = reader.GetDecimal("2b_WeightEnd");
            dto._2c_WeightStart = reader.GetDecimal("2c_WeightStart");
            dto._3a_CarrierRatesImpact = reader.GetDecimal("3a_CarrierRatesImpact");
            dto._3b_NonBHLinehaulCostPerMileEnd = reader.GetDecimal("3b_NonBHLinehaulCostPerMileEnd");
            dto._3c_NonBHLinehaulCostPerMileStart = reader.GetDecimal("3c_NonBHLinehaulCostPerMileStart");
            dto._4a_BuyingPatternImpact = reader.GetDecimal("4a_BuyingPatternImpact");
            dto._4b_WeightPerShipmentEnd = reader.GetDecimal("4b_WeightPerShipmentEnd");
            dto._4c_WeightPerShipmentStart = reader.GetDecimal("4c_WeightPerShipmentStart");
            dto._5a_NewLanesImpact = reader.GetDecimal("5a_NewLanesImpact");
            dto._5b_NewLaneCount = reader.GetDecimal("5b_NewLaneCount");
            dto._6a_BackhaulUsageImpact = reader.GetDecimal("6a_BackhaulUsageImpact");
            dto._6b_BackhaulUsageEnd = reader.GetDecimal("6b_BackhaulUsageEnd");
            dto._6c_BackhaulUsageStart = reader.GetDecimal("6c_BackhaulUsageStart");
            dto._7a_AccessorialFeesImpact = reader.GetDecimal("7a_AccessorialFeesImpact");
            dto._7b_AccessorialFeesPerLoadEnd = reader.GetDecimal("7b_AccessorialFeesPerLoadEnd");
            dto._7c_AccessorialFeesPerLoadStart = reader.GetDecimal("7c_AccessorialFeesPerLoadStart");
            dto._8a_FreightConsolidationImpact = reader.GetDecimal("8a_FreightConsolidationImpact");
            dto._8b_MultiStopPercentEnd = reader.GetDecimal("8b_MultiStopPercentEnd");
            dto._8c_MultiStopPercentStart = reader.GetDecimal("8c_MultiStopPercentStart");
            dto._9a_FuelImpact = reader.GetDecimal("9a_FuelImpact");
            dto._9b_FuelCostPercentEnd = reader.GetDecimal("9b_FuelCostPercentEnd");
            dto._9c_FuelCostPercentStart = reader.GetDecimal("9c_FuelCostPercentStart");
            dto._10a_FreightAllowanceImpact = reader.GetDecimal("10a_FreightAllowanceImpact");
            dto._10b_AllowancePerPoundEnd = reader.GetDecimal("10b_AllowancePerPoundEnd");
            dto._10c_AllowancePerPoundStart = reader.GetDecimal("10c_AllowancePerPoundStart");
            dto._11a_DiscontinuedLanesImpact = reader.GetDecimal("11a_DiscontinuedLanesImpact");
            dto._11b_DiscontinuedLaneCount = reader.GetDecimal("11b_DiscontinuedLaneCount");
            dto._12d_PoundsShippedStart = reader.GetDecimal("12d_PoundsShippedStart");
            dto._12e_PoundsShippedEnd = reader.GetDecimal("12e_PoundsShippedEnd");
            dto._12f_CasesShippedStart = reader.GetDecimal("12f_CasesShippedStart");
            dto._12g_CasesShippedEnd = reader.GetDecimal("12g_CasesShippedEnd");
            dto._12h_PoundsPerShipmentStart = reader.GetDecimal("12h_PoundsPerShipmentStart");
            dto._12i_PoundsPerShipmentEnd = reader.GetDecimal("12i_PoundsPerShipmentEnd");
            dto._13d_LTLCostPerCWTStart = reader.GetDecimal("13d_LTLCostPerCWTStart");
            dto._13e_LTLCostPerCWTEnd = reader.GetDecimal("13e_LTLCostPerCWTEnd");
            dto._13f_TLCostPerMileStart = reader.GetDecimal("13f_TLCostPerMileStart");
            dto._13g_TLCostPerMileEnd = reader.GetDecimal("13g_TLCostPerMileEnd");
            dto._13h_IMCostPerMileStart = reader.GetDecimal("13h_IMCostPerMileStart");
            dto._13i_IMCostPerMileEnd = reader.GetDecimal("13i_IMCostPerMileEnd");
            dto._14d_UtilizationStart = reader.GetDecimal("14d_UtilizationStart");
            dto._14e_UtilizationEnd = reader.GetDecimal("14e_UtilizationEnd");
            dto._14f_PoundsPerShipmentStart = reader.GetDecimal("14f_PoundsPerShipmentStart");
            dto._14g_PoundsPerShipmentEnd = reader.GetDecimal("14g_PoundsPerShipmentEnd");
            dto._14h_LTLUsageStart = reader.GetDecimal("14h_LTLUsageStart");
            dto._14i_LTLUsageEnd = reader.GetDecimal("14i_LTLUsageEnd");
            dto._15a_LanesNew = reader.GetDecimal("15a_LanesNew");
            dto._15b_NewSavings = reader.GetDecimal("15b_NewSavings");
            dto._15c_NewMonthlyAllowance = reader.GetDecimal("15c_NewMonthlyAllowance");
            dto._16d_BackhaulLoadsStart = reader.GetDecimal("16d_BackhaulLoadsStart");
            dto._16e_BackhaulLoadsEnd = reader.GetDecimal("16e_BackhaulLoadsEnd");
            dto._16f_CasesMovedBackhaulStart = reader.GetDecimal("16f_CasesMovedBackhaulStart");
            dto._16g_CasesMovedBackhaulEnd = reader.GetDecimal("16g_CasesMovedBackhaulEnd");
            dto._16h_PoundsPerBackhaulStart = reader.GetDecimal("16h_PoundsPerBackhaulStart");
            dto._16i_PoundsPerBackhaulEnd = reader.GetDecimal("16i_PoundsPerBackhaulEnd");
            dto._17d_AccessorialFeesPerLoadStart = reader.GetDecimal("17d_AccessorialFeesPerLoadStart");
            dto._17e_AccessorialFeesPerLoadEnd = reader.GetDecimal("17e_AccessorialFeesPerLoadEnd");
            dto._17f_AccessorialPercentStart = reader.GetDecimal("17f_AccessorialPercentStart");
            dto._17g_AccessorialPercentEnd = reader.GetDecimal("17g_AccessorialPercentEnd");
            dto._17h_DetentionChargesPerLoadStart = reader.GetDecimal("17h_DetentionChargesPerLoadStart");
            dto._17i_DetentionChargesPerLoadEnd = reader.GetDecimal("17i_DetentionChargesPerLoadEnd");
            dto._18a_LanesDiscontinued = reader.GetDecimal("18a_LanesDiscontinued");
            dto._18b_PriorSavings = reader.GetDecimal("18b_PriorSavings");
            dto._18c_LostMonthlyAllowance = reader.GetDecimal("18c_LostMonthlyAllowance");
            dto._19d_AllowancePerCWTStart = reader.GetDecimal("19d_AllowancePerCWTStart");
            dto._19e_AllowancePerCWTEnd = reader.GetDecimal("19e_AllowancePerCWTEnd");
            dto._19f_AllowancePerCaseStart = reader.GetDecimal("19f_AllowancePerCaseStart");
            dto._19g_AllowancePerCaseEnd = reader.GetDecimal("19g_AllowancePerCaseEnd");
            dto._19h_LinehaulCostPerCWTStart = reader.GetDecimal("19h_LinehaulCostPerCWTStart");
            dto._19i_LinehaulCostPerCWTEnd = reader.GetDecimal("19i_LinehaulCostPerCWTEnd");
            dto._20d_FuelPercentOfCostStart = reader.GetDecimal("20d_FuelPercentOfCostStart");
            dto._20e_FuelPercentOfCostEnd = reader.GetDecimal("20e_FuelPercentOfCostEnd");
            dto._20f_FuelCostPerMileStart = reader.GetDecimal("20f_FuelCostPerMileStart");
            dto._20g_FuelCostPerMileEnd = reader.GetDecimal("20g_FuelCostPerMileEnd");
            dto._20h_PricePerGallonStart = reader.GetDecimal("20h_PricePerGallonStart");
            dto._20i_PricePerGallonEnd = reader.GetDecimal("20i_PricePerGallonEnd");
            dto._21d_PercentLoadsConsolidatedStart = reader.GetDecimal("21d_PercentLoadsConsolidatedStart");
            dto._21e_PercentLoadsConsolidatedEnd = reader.GetDecimal("21e_PercentLoadsConsolidatedEnd");
            dto._21f_LoadsConsolidatedStart = reader.GetDecimal("21f_LoadsConsolidatedStart");
            dto._21g_LoadsConsolidatedEnd = reader.GetDecimal("21g_LoadsConsolidatedEnd");
            dto._21h_LTLConsolidatedStart = reader.GetDecimal("21h_LTLConsolidatedStart");
            dto._21i_LTLConsolidatedEnd = reader.GetDecimal("21i_LTLConsolidatedEnd");
        }

        private void Map(SafeDataReader reader, SavingsDto dto)
        {
            var shipmentCount = reader.GetInt32Nullable("ShipmentCount");
            if (shipmentCount.HasValue && shipmentCount.Value > 0)
            {
                dto.Savings = reader.GetDecimalNullable("Savings");
            }
            dto.CalendarDetailName = reader.GetString("Name");
            dto.CalendarDetailAbbreviation = reader.GetString("Abbreviation");
            dto.CalendarDetailEndDate = reader.GetDateTime("EndDate");
            dto.CalendarDetailStartDate = reader.GetDateTime("StartDate");
            dto.CalendarDetailId = reader.GetInt32("CalendarDetailId");
            dto.Period = reader.GetInt32("Period");
            dto.Year = reader.GetInt32("Year");
        }

        private Facility MapToFacility(SafeDataReader reader, string prefix)
        {
            return new Facility(
                name: reader.GetString($"{prefix}Name"),
                city: reader.GetString($"{prefix}City"),
                state: reader.GetString($"{prefix}State"),
                zip: reader.GetString($"{prefix}PostalCode"));
        }

        private Lane MapToLane(
            SafeDataReader reader,
            string lanePrefix = "",
            string shipperPrefix = "Shipper_",
            string shipFromPrefix = "ShipFrom_",
            string shipToPrefix = "ShipTo_")
        {
            var vendor = MapToVendor(reader, shipperPrefix);
            var shipFrom = MapToFacility(reader, shipFromPrefix);
            var shipTo = MapToFacility(reader, shipToPrefix);
            return new Lane(
                shipFrom: shipFrom,
                shipTo: shipTo,
                vendor: vendor,
                id: reader.GetInt32($"{lanePrefix}ID"),
                laneAnalysisId: reader.GetInt32($"{lanePrefix}AIMS_LaneAnalysis_ID"));
        }

        private LaneSavingsImpactDto MapToLaneSavingsImpactDto(SafeDataReader reader)
        {
            return new LaneSavingsImpactDto(
                laneId: reader.GetInt32("Lane_ID"),
                tenantId: reader.GetInt32("Tenant_ID"),
                impact: reader.GetDouble("Impact"));
        }

        private NewAndDiscontinuedLaneDto MapToNewAndDiscontinuedLaneDto(SafeDataReader reader)
        {
            return new NewAndDiscontinuedLaneDto(
                tenantId: reader.GetInt32("Tenant_ID"),
                calendarDetailId: reader.GetInt32("CalendarDetail_ID"),
                laneId: reader.GetInt32("Lane_ID"),
                newLanesCount: reader.GetInt32("NewLanesCount"),
                discontinuedLanesCount: reader.GetInt32("DiscontinuedLanesCount"));
        }

        private SavingsDriverPillLanePeriod MapToPillLanePeriod(SafeDataReader reader)
        {
            return new SavingsDriverPillLanePeriod(
                shipmentCount: reader.GetInt32("ShipmentCount"),
                laneId: reader.GetInt32("Lane_ID"),
                weightPerShipment: reader.GetDouble("WeightPerShipment"),
                buyingPatternImpact: reader.GetDouble("BuyingPatternImpact"),
                weight: reader.GetDouble("Weight"),
                volumeImpact: reader.GetDouble("VolumeImpact"),
                carrierRatesImpact: reader.GetDouble("CarrierRatesImpact"),
                ratesTlPerMile: reader.GetDoubleNullable("RatesTlPerMile"),
                freightAllowancesImpact: reader.GetDouble("FreightAllowancesImpact"),
                allowancePerCwt: reader.GetDouble("AllowancePerCwt"),
                newLanesImpact: reader.GetDouble("NewLanesImpact"),
                newLanesCount: reader.GetInt32("NewLanesCount"),
                discontinuedLanesImpact: reader.GetDouble("DiscontinuedLanesImpact"),
                discontinuedLanesCount: reader.GetInt32("DiscontinuedLanesCount"),
                savings: reader.GetDouble("Savings"),
                freightConsolidationImpact: reader.GetDouble("FreightConsolidationImpact"),
                loadCount: reader.GetInt32("LoadCount"),
                loadCountMultiStop: reader.GetInt32("LoadCountMultiStop"),
                backhaulUsageImpact: reader.GetDouble("BackhaulUsageImpact"),
                loadCountBackhaul: reader.GetInt32("LoadCountBackhaul"),
                accessorialFeesImpact: reader.GetDouble("AccessorialFeesImpact"),
                accessorialFeesPerLoad: reader.GetDouble("AccessorialFeesPerLoad"),
                fuelImpact: reader.GetDouble("FuelImpact"),
                fuelPercentCost: reader.GetDouble("FuelPercentCost"),
                shipmentCountTl: reader.GetInt32("ShipmentCountTL"),
                shipmentCountIm: reader.GetInt32("ShipmentCountIM"),
                shipmentCountLtl: reader.GetInt32("ShipmentCountLTL"),
                calendarDetailId: reader.GetInt32("CalendarDetail_ID"),
                calendarDetail: null,
                tenantId: reader.GetInt32("Tenant_ID"));
        }

        private SavingsDriverPillPeriod MapToPillPeriod(SafeDataReader reader)
        {
            return new SavingsDriverPillPeriod(
                shipmentCount: reader.GetInt32("ShipmentCount"),
                weightPerShipment: reader.GetDouble("WeightPerShipment"),
                buyingPatternImpact: reader.GetDouble("BuyingPatternImpact"),
                weight: reader.GetDouble("Weight"),
                volumeImpact: reader.GetDouble("VolumeImpact"),
                carrierRatesImpact: reader.GetDouble("CarrierRatesImpact"),
                ratesTlPerMile: reader.GetDoubleNullable("RatesTlPerMile"),
                freightAllowancesImpact: reader.GetDouble("FreightAllowancesImpact"),
                allowancePerCwt: reader.GetDouble("AllowancePerCwt"),
                newLanesImpact: reader.GetDouble("NewLanesImpact"),
                newLanesCount: reader.GetInt32("NewLanesCount"),
                discontinuedLanesImpact: reader.GetDouble("DiscontinuedLanesImpact"),
                discontinuedLanesCount: reader.GetInt32("DiscontinuedLanesCount"),
                savings: reader.GetDouble("Savings"),
                freightConsolidationImpact: reader.GetDouble("FreightConsolidationImpact"),
                loadCount: reader.GetInt32("LoadCount"),
                loadCountMultiStop: reader.GetInt32("LoadCountMultiStop"),
                backhaulUsageImpact: reader.GetDouble("BackhaulUsageImpact"),
                loadCountBackhaul: reader.GetInt32("LoadCountBackhaul"),
                accessorialFeesImpact: reader.GetDouble("AccessorialFeesImpact"),
                accessorialFeesPerLoad: reader.GetDouble("AccessorialFeesPerLoad"),
                fuelImpact: reader.GetDouble("FuelImpact"),
                fuelPercentCost: reader.GetDouble("FuelPercentCost"),
                shipmentCountTl: reader.GetInt32("ShipmentCountTL"),
                shipmentCountIm: reader.GetInt32("ShipmentCountIM"),
                shipmentCountLtl: reader.GetInt32("ShipmentCountLTL"),
                calendarDetailId: reader.GetInt32("CalendarDetail_ID"),
                calendarDetail: null,
                tenantId: reader.GetInt32("Tenant_ID"));
        }

        private Vendor MapToVendor(SafeDataReader reader, string prefix = "Shipper_")
        {
            return new Vendor(name: reader.GetString($"{prefix}Name"), id: reader.GetInt32($"{prefix}ID"));
        }

        #endregion
    }
}