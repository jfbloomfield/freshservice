﻿namespace AS.Crossbow.EntryPages.Repositories.DTO.Network
{
    public class NewAndDiscontinuedLaneDto
    {
        #region Constructors

        public NewAndDiscontinuedLaneDto(
            int calendarDetailId,
            int discontinuedLanesCount,
            int laneId,
            int newLanesCount,
            int tenantId)
        {
            CalendarDetailId = calendarDetailId;
            DiscontinuedLanesCount = discontinuedLanesCount;
            LaneId = laneId;
            NewLanesCount = newLanesCount;
            TenantId = tenantId;
        }

        #endregion

        #region Properties

        public int CalendarDetailId { get; }
        public int DiscontinuedLanesCount { get; }
        public int LaneId { get; }
        public int NewLanesCount { get; }
        public int TenantId { get; }

        #endregion
    }
}