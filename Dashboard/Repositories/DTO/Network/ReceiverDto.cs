﻿namespace AS.Crossbow.EntryPages.Repositories.DTO.Network
{
    public class ReceiverDto
    {
        #region Properties

        public bool HasPillReceiverRecords { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Number { get; set; }

        #endregion
    }
}