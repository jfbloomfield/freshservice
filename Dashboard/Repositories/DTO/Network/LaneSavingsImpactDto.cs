﻿namespace AS.Crossbow.EntryPages.Repositories.DTO.Network
{
    public class LaneSavingsImpactDto
    {
        #region Constructors

        public LaneSavingsImpactDto(int tenantId, int laneId, double impact)
        {
            Impact = impact;
            LaneId = laneId;
            TenantId = tenantId;
        }

        #endregion

        #region Properties

        public double Impact { get; }
        public int LaneId { get; }
        public int TenantId { get; }

        #endregion
    }
}