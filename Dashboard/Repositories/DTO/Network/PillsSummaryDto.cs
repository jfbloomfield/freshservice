﻿namespace AS.Crossbow.EntryPages.Repositories.DTO.Network
{
    public class PillsSummaryDto
    {
        #region Properties

        public decimal _10a_FreightAllowanceImpact { get; set; }
        public decimal _10b_AllowancePerPoundEnd { get; set; }
        public decimal _10c_AllowancePerPoundStart { get; set; }
        public decimal _11a_DiscontinuedLanesImpact { get; set; }
        public decimal _11b_DiscontinuedLaneCount { get; set; }
        public decimal _12d_PoundsShippedStart { get; set; }
        public decimal _12e_PoundsShippedEnd { get; set; }
        public decimal _12f_CasesShippedStart { get; set; }
        public decimal _12g_CasesShippedEnd { get; set; }
        public decimal _12h_PoundsPerShipmentStart { get; set; }
        public decimal _12i_PoundsPerShipmentEnd { get; set; }
        public decimal _13d_LTLCostPerCWTStart { get; set; }
        public decimal _13e_LTLCostPerCWTEnd { get; set; }
        public decimal _13f_TLCostPerMileStart { get; set; }
        public decimal _13g_TLCostPerMileEnd { get; set; }
        public decimal _13h_IMCostPerMileStart { get; set; }
        public decimal _13i_IMCostPerMileEnd { get; set; }
        public decimal _14d_UtilizationStart { get; set; }
        public decimal _14e_UtilizationEnd { get; set; }
        public decimal _14f_PoundsPerShipmentStart { get; set; }
        public decimal _14g_PoundsPerShipmentEnd { get; set; }
        public decimal _14h_LTLUsageStart { get; set; }
        public decimal _14i_LTLUsageEnd { get; set; }
        public decimal _15a_LanesNew { get; set; }
        public decimal _15b_NewSavings { get; set; }
        public decimal _15c_NewMonthlyAllowance { get; set; }
        public decimal _16d_BackhaulLoadsStart { get; set; }
        public decimal _16e_BackhaulLoadsEnd { get; set; }
        public decimal _16f_CasesMovedBackhaulStart { get; set; }
        public decimal _16g_CasesMovedBackhaulEnd { get; set; }
        public decimal _16h_PoundsPerBackhaulStart { get; set; }
        public decimal _16i_PoundsPerBackhaulEnd { get; set; }
        public decimal _17d_AccessorialFeesPerLoadStart { get; set; }
        public decimal _17e_AccessorialFeesPerLoadEnd { get; set; }
        public decimal _17f_AccessorialPercentStart { get; set; }
        public decimal _17g_AccessorialPercentEnd { get; set; }
        public decimal _17h_DetentionChargesPerLoadStart { get; set; }
        public decimal _17i_DetentionChargesPerLoadEnd { get; set; }
        public decimal _18a_LanesDiscontinued { get; set; }
        public decimal _18b_PriorSavings { get; set; }
        public decimal _18c_LostMonthlyAllowance { get; set; }
        public decimal _19d_AllowancePerCWTStart { get; set; }
        public decimal _19e_AllowancePerCWTEnd { get; set; }
        public decimal _19f_AllowancePerCaseStart { get; set; }
        public decimal _19g_AllowancePerCaseEnd { get; set; }
        public decimal _19h_LinehaulCostPerCWTStart { get; set; }
        public decimal _19i_LinehaulCostPerCWTEnd { get; set; }
        public decimal _20d_FuelPercentOfCostStart { get; set; }
        public decimal _20e_FuelPercentOfCostEnd { get; set; }
        public decimal _20f_FuelCostPerMileStart { get; set; }
        public decimal _20g_FuelCostPerMileEnd { get; set; }
        public decimal _20h_PricePerGallonStart { get; set; }
        public decimal _20i_PricePerGallonEnd { get; set; }
        public decimal _21d_PercentLoadsConsolidatedStart { get; set; }
        public decimal _21e_PercentLoadsConsolidatedEnd { get; set; }
        public decimal _21f_LoadsConsolidatedStart { get; set; }
        public decimal _21g_LoadsConsolidatedEnd { get; set; }
        public decimal _21h_LTLConsolidatedStart { get; set; }
        public decimal _21i_LTLConsolidatedEnd { get; set; }
        public decimal _2a_VolumeImpact { get; set; }
        public decimal _2b_WeightEnd { get; set; }
        public decimal _2c_WeightStart { get; set; }
        public decimal _3a_CarrierRatesImpact { get; set; }
        public decimal _3b_NonBHLinehaulCostPerMileEnd { get; set; }
        public decimal _3c_NonBHLinehaulCostPerMileStart { get; set; }
        public decimal _4a_BuyingPatternImpact { get; set; }
        public decimal _4b_WeightPerShipmentEnd { get; set; }
        public decimal _4c_WeightPerShipmentStart { get; set; }
        public decimal _5a_NewLanesImpact { get; set; }
        public decimal _5b_NewLaneCount { get; set; }
        public decimal _6a_BackhaulUsageImpact { get; set; }
        public decimal _6b_BackhaulUsageEnd { get; set; }
        public decimal _6c_BackhaulUsageStart { get; set; }
        public decimal _7a_AccessorialFeesImpact { get; set; }
        public decimal _7b_AccessorialFeesPerLoadEnd { get; set; }
        public decimal _7c_AccessorialFeesPerLoadStart { get; set; }
        public decimal _8a_FreightConsolidationImpact { get; set; }
        public decimal _8b_MultiStopPercentEnd { get; set; }
        public decimal _8c_MultiStopPercentStart { get; set; }
        public decimal _9a_FuelImpact { get; set; }
        public decimal _9b_FuelCostPercentEnd { get; set; }
        public decimal _9c_FuelCostPercentStart { get; set; }

        #endregion
    }
}