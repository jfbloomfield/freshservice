﻿using System;

namespace AS.Crossbow.EntryPages.Repositories.DTO.Network
{
    public class SavingsDto
    {
        #region Properties

        public string CalendarDetailAbbreviation { get; set; }
        public DateTime CalendarDetailEndDate { get; set; }
        public int CalendarDetailId { get; set; }
        public string CalendarDetailName { get; set; }
        public DateTime CalendarDetailStartDate { get; set; }
        public int Period { get; set; }
        public decimal? Savings { get; set; }
        public int Year { get; set; }

        #endregion
    }
}