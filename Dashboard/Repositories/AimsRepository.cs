﻿using System.Data;
using AS.Crossbow.EntryPages.Common;
using AS.Crossbow.EntryPages.Models;
using AS.Crossbow.EntryPages.Repositories.Interfaces;
using AS.Framework.Common.Extensions;
using AS.Framework.Common.Repositories;

namespace AS.Crossbow.EntryPages.Repositories
{
    public class AimsRepository : BaseRepository, IAimsRepository
    {
        #region IAimsRepository Members

        public AimsTenantInformation GetTenantInformation()
        {
            using (var connection = GetOpenConnection())
            {
                using (var command = connection.CreateCommand())
                {
                    AimsTenantInformation aimsTenantInformation = null;
                    command.CommandText =
                        "SELECT value, @@SERVERNAME AS ServerName FROM aimslog.dbo.aspparameters WHERE Name = 'TENANT_ID'";
                    command.CommandType = CommandType.Text;
                    command.AddInputParameterWithValue("tenantid", Constants.AspParameters.TENANT_ID);

                    using (var reader = ExecuteReader(command))
                    {
                        while (reader.Read())
                        {
                            aimsTenantInformation = new AimsTenantInformation();
                            aimsTenantInformation.Tenant_ID = reader.GetInt32("value");
                            aimsTenantInformation.DbServerName = reader.GetString("ServerName");
                        }
                    }
                    return aimsTenantInformation;
                }
            }
        }

        #endregion
    }
}