using System;
using System.Collections.Generic;
using AS.Crossbow.EntryPages.Models;
using AS.Crossbow.EntryPages.Models.NetworkPage;
using AS.Crossbow.EntryPages.Repositories.DTO.Network;

namespace AS.Crossbow.EntryPages.Repositories.Interfaces
{
    public interface INetworkRepository
    {
        #region Public Methods

        IList<Lane> GetLanes(int tenantId, IList<int> laneIds);

        PagedResponse<LaneSavingsImpactDto> GetLanesBySavingsImpact(
            int tenantId,
            IList<int> calendarDetailIds,
            int? receiverId = null,
            int offset = 0,
            int limit = 10,
            string sortColumn = "VolumeImpact",
            string sortOrder = "asc");

        IEnumerable<NewAndDiscontinuedLaneDto> GetNewAndDiscontinuedLaneDtos(
            int tenantId,
            IList<int> periods,
            int? receiverId = null);

        PillsSummaryDto GetPillsSummaryDto(
            int tenantId,
            int? receiverId,
            DateTime startPeriodStartDate,
            DateTime endPeriodStartDate);

        IEnumerable<ReceiverDto> GetReceiverDtos(int tenantId);

        IList<SavingsDto> GetSavings(int tenantId, int receiverId, DateTime minDate, DateTime maxDate);
        IList<SavingsDto> GetSavings(int tenantId, DateTime minDate, DateTime maxDate);

        IList<SavingsDriverPillLanePeriod> GetSavingsDriverPillLanePeriods(
            int tenantId,
            int? receiverId,
            IList<int> calendarDetailIds);

        IList<SavingsDriverPillLanePeriod> GetSavingsDriverPillLanePeriods(
            int tenantId,
            IList<int> laneIds,
            IList<int> calendarDetailIds);

        IList<SavingsDriverPillPeriod> GetSavingsDriverPillPeriods(
            int tenantId,
            int? receiverId,
            IList<int> calendarDetailIds);

        #endregion
    }
}