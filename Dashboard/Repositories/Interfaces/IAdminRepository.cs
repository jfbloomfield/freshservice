﻿using System.Collections.Generic;
using AS.Crossbow.EntryPages.Models.Admin;

namespace AS.Crossbow.EntryPages.Repositories.Interfaces
{
    public interface IAdminRepository
    {
        #region Public Methods

        AccountSettings GetAccountSettings(int tenantId);

        IList<BusinessType> GetBusinessTypes(int tenantId);
        string GetDBServerNameByTenantId(int tenantId);
        IList<string> GetStates(int tenantId);
        int InsertAccountSettings(int tenantId, AccountSettings accountSettings);
        void UpdateAccountSettings(int tenantId, AccountSettings accountSettings);

        #endregion
    }
}