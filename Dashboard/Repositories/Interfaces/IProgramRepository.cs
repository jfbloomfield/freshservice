﻿namespace AS.Crossbow.EntryPages.Repositories.Interfaces
{
    public interface IProgramRepository
    {
        #region Public Methods

        int? GetCurrentYearCaseCount(int tenantId);
        int? GetCurrentYearNetworkCaseCount(int tenantId);

        #endregion
    }
}