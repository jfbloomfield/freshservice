﻿using System.Data;
using AS.Crossbow.EntryPages.Models.Admin;
using AS.ValidationEngine.Model;

namespace AS.Crossbow.EntryPages.Repositories.Interfaces
{
    public interface IExcelUploadRepository
    {
        #region Public Methods

        LookupList GetModeLookupList(IDbTransaction transaction);

        LookupList GetEquipmentLookupList(int tenantId, IDbTransaction transaction);
        int InsertImportLog(IDbTransaction transaction, ExcelImportFile excelImportFile);

        #endregion
    }
}