﻿using AS.Crossbow.EntryPages.Models;

namespace AS.Crossbow.EntryPages.Repositories.Interfaces
{
    public interface IAimsRepository
    {
        #region Public Methods

        AimsTenantInformation GetTenantInformation();

        #endregion
    }
}