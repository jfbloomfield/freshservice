using System.Data;
using AS.Crossbow.EntryPages.Models;

namespace AS.Crossbow.EntryPages.Repositories.Interfaces
{
    public interface ICalendarRepository
    {
        #region Public Methods

        Calendar GetDefaultCalendar(int tenantId);

        #endregion
    }
}