﻿using System;
using System.Collections.Generic;
using System.Data;
using AS.Crossbow.Core.Data;
using AS.Crossbow.EntryPages.Models.Admin;
using AS.Crossbow.EntryPages.Repositories.Interfaces;
using AS.ValidationEngine.Model;

namespace AS.Crossbow.EntryPages.Repositories
{
    public class ExcelUploadRepository : BaseRepository, IExcelUploadRepository
    {
        #region IExcelUploadRepository Members

        public LookupList GetEquipmentLookupList(int tenantId, IDbTransaction transaction)
        {
            if (transaction == null)
            {
                throw new ArgumentNullException(nameof(transaction));
            }
            var lookupList = new LookupList();
            lookupList.Type = "core.equipment.referenceid";

            using (var command = transaction.Connection.CreateCommand())
            {
                command.Transaction = transaction;
                command.CommandText = "SELECT referenceid FROM core.equipment WHERE tenant_id = @tenantId";
                command.CommandType = CommandType.Text;
                command.AddInputParameter("tenantID", tenantId);
                using (var reader = ExecuteReader(command))
                {
                    lookupList.Values = new List<string>();
                    while (reader.Read())
                    {
                        lookupList.Values.Add(reader["referenceid"].ToString());
                    }
                }
            }

            return lookupList;
        }

        public int InsertImportLog(IDbTransaction transaction, ExcelImportFile excelImportFile)
        {
            if (transaction == null)
            {
                throw new ArgumentNullException(nameof(transaction));
            }
            if (excelImportFile == null)
            {
                throw new ArgumentNullException(nameof(excelImportFile));
            }

            var id = 0;
            using (var command = transaction.Connection.CreateCommand())
            {
                command.Transaction = transaction;
                command.CommandText = @"INSERT INTO core.importlog
(Tenant_ID, User_ID, TimeStamp, RowCount, Source, FileName, StoredFileName)
VALUES(@tenantID, @userID, CURRENT_TIMESTAMP, @rowCount, @source, @fileName, @storedFileName); 
SELECT LAST_INSERT_ID() ;";
                command.AddInputParameter("tenantID", excelImportFile.TenantId);
                command.AddInputParameter("userID", 1);
                command.AddInputParameter("rowCount", excelImportFile.ValidateDataSet.TypedData.Tables[0].Rows.Count);
                command.AddInputParameter("source", "Website");
                command.AddInputParameter("fileName", excelImportFile.OriginalFileName);
                command.AddInputParameter("storedFileName", excelImportFile.StoredFileName);
                command.CommandType = CommandType.Text;
                var val =  ExecuteScalar(command).ToString();
                int.TryParse(val,out id);
            }

            
            return id;
        }

        public void LoadExcelFile(IDbTransaction transaction, ExcelImportFile excelImportFile)
        {
            if (transaction == null)
            {
                throw new ArgumentNullException(nameof(transaction));
            }
            if (excelImportFile == null)
            {
                throw new ArgumentNullException(nameof(excelImportFile));
            }
        }

        public LookupList GetModeLookupList(IDbTransaction transaction)
        {
            if (transaction == null)
            {
                throw new ArgumentNullException(nameof(transaction));
            }
            var lookupList = new LookupList();
            lookupList.Type = "core.mode.code";

            using (var command = transaction.Connection.CreateCommand())
            {
                command.Transaction = transaction;
                command.CommandText = "SELECT Code FROM core.mode";
                command.CommandType = CommandType.Text;
                using (var reader = ExecuteReader(command))
                {
                    lookupList.Values = new List<string>();
                    while (reader.Read())
                    {
                        lookupList.Values.Add(reader["Code"].ToString());
                    }
                }
            }

            return lookupList;
        }
        
        #endregion
    }
}