///<reference path="../typings/browser.d.ts" />

import { bootstrap }    from  '@angular/platform-browser-dynamic';
import { HTTP_PROVIDERS } from '@angular/http'
import { ROUTER_PROVIDERS } from '@angular/router';
import { enableProdMode } from '@angular/core';


import 'rxjs/Rx';
import { AppComponent } from './app.component';  

//enableProdMode();
bootstrap(AppComponent, [HTTP_PROVIDERS, ROUTER_PROVIDERS]);
