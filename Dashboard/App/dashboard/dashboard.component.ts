﻿import {Component, Injectable, OnInit} from '@angular/core';
import {ROUTER_DIRECTIVES} from '@angular/router';
import {Tile} from '../tile/tile';
import {TileComponent} from '../tile/tile.component';
import {FreshServiceDataService} from './dashboard-data.service';
var __moduleName: any;

@Component({
    template: `
        <div>Fresh Service Dashboard</div>
            <div class="tiles">
            <tile *ngFor="let tile of tiles; let idx = index" [tile]="tile" [position]="idx"></tile>
        </div>
        `,
    moduleId: __moduleName,
    directives: [TileComponent,ROUTER_DIRECTIVES],
    providers: [FreshServiceDataService]
})
@Injectable()
export class DashboardComponent implements OnInit {
    private tickets = [];

    constructor(private freshServiceDataService:FreshServiceDataService) { }

    tiles: Tile[] = [];

    ngOnInit() {
        this.tiles[0] = {
            title: 'Ticket Title',
            number: 34
        };

        this.freshServiceDataService.getTickets().subscribe((tickets) => {
            this.tickets = tickets;
            
            this.tiles = _.map(tickets, (ticket)=>{
                return {number: ticket.Id, title: description}
            });
        });
    }
}