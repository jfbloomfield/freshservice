﻿/// <reference path="../../Scripts/TypeLite.Net4.d.ts"/>

import {Injectable} from '@angular/core';
import {Http, Response, URLSearchParams, Headers} from '@angular/http';

import {Observable, BehaviorSubject} from 'rxjs/Rx'
import {Http, Response} from "angular2/http";

@Injectable()
export class FreshServiceDataService {
    constructor(private http: Http) { }
//curl -u helpdesk@arrowstream.com:EpNOzVSqe7zI -H "Content-Type: application/json" -X GET
    private fsURL = 'https://arrowstream.freshservice.com/ticket_fields.json';

    getTickets (): Observable<any[]> {

        var creds = "username=helpdesk@arrowstream.com&password=EpNOzVSqe7zI";
        var headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        
        return this.http.post(this.fsURL,creds,{headers: headers}).map(this.extractData).catch(this.handleError);
    }

    private extractData(res: Response) {
        let body = res.json();
        return body.data || {};
    }

    private handleError(error:any){
        if (console && console.log) {
            console.log(error);
        }
        return Observable.throw(error);
    }
}

