import {Input, Output, EventEmitter, Directive} from '@angular/core';

@Directive({
    selector: '[tile-hover]',
    host: {
        '(mouseenter)': 'onMouseEnter()',
        '(mouseleave)': 'onMouseLeave()'
    },
    directives: []
})
export class TileDirective {
    @Input() value:string;
    @Output() valueChange = new EventEmitter();

    onMouseEnter() {
        this.valueChange.emit(true);
    }

    onMouseLeave() {
        this.valueChange.emit(false);
    }
}
