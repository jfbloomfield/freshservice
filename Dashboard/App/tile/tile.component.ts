import {Component, Input, ViewChild} from '@angular/core';
import {NgClass} from '@angular/common';

import {TileDirective} from './tile.directive';
import {Tile} from './tile';
var __moduleName:any;

@Component({
    moduleId: __moduleName,
    selector: 'tile',
    templateUrl: 'tile.html',
    styleUrls: ['tile.css'],
    directives: [TileDirective]
})
export class TileComponent {
    @Input() tile:Tile;
    @Input() position:number;
    private collapseFooter:boolean = false;
}