import {Directive, Input, ElementRef} from '@angular/core';

import ProgressBar from 'kimmobrunfeldt/progressbar.js/dist/progressbar';

@Directive({
    selector: '[tile-graph]',
})
export class GraphDirective {
    @Input() value:string;

    private el:HTMLElement;

    constructor(el:ElementRef) {
        this.el = el.nativeElement;
    }

    ngOnInit() {
        var bar = new ProgressBar.Circle(this.el, {
            color: '#aaa',
            strokeWidth: 6,
            trailWidth: 6,
            easing: 'easeInOut',
            duration: 3000,
            text: {
                autoStyleContainer: false
            },
            from: {color: '#98C7EA', width: 6},
            to: {color: '#245B84', width: 6},
            step: function (state, circle) {
                circle.path.setAttribute('stroke', state.color);
                circle.path.setAttribute('stroke-width', state.width);

                var value = Math.round(circle.value() * 100);
                circle.setText(`${value}%`);
            }
        });

        bar.animate(this.value);  // Number from 0.0 to 1.0
    }
}  
