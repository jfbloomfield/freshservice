import { Component } from '@angular/core';
import { Routes, Router, ROUTER_DIRECTIVES } from '@angular/router';

import { DashboardComponent } from './dashboard/dashboard.component';
@Component({
    selector: 'my-app',
    template: `
    <router-outlet></router-outlet>
  `,
    directives: [ROUTER_DIRECTIVES]
})
@Routes([ 
        { path: '/',  component: DashboardComponent},
        { path: '*', component: DashboardComponent},
])
export class AppComponent {
    constructor(private router: Router) {}
}