import {Component, Input, ViewChild} from '@angular/core';

import numeral from 'numeral';
import d3 from 'd3';
import d3tip from 'd3-tip';
d3tip(d3);
var __moduleName:any;

@Component({
    selector: 'savings-driver-trend-chart',
    moduleId: __moduleName,
    template: '<svg #chart class="trend-chart buying-pattern"></svg>',
    directives: []
})
export class ChartComponent {
    @ViewChild('chart') chartEl;
    @Input() periodSavings;
    @Input() simpleChart = false;
    @Input() chartSettings;
    private el;

    ngAfterViewInit() {
        this.el = this.chartEl.nativeElement;
        if (this.el instanceof SVGElement) {
            var chart = this.drawTrendChart(this.el, this.periodSavings, this.chartSettings, this.simpleChart);

            function nodeThatContainsElement(nodes:NodeList, element:Element) {
                for (var i = 0; i < nodes.length; ++i) {
                    var node = nodes[i];
                    if ((<any>node).contains(element)) {
                        return node;
                    }
                }
                return null;
            }

            // bootstrap table just blows things away. Watch it specifically if we are a child of one.
            var bootstrapTables = document.querySelectorAll(".bootstrap-table tbody");
            var tableContainer = nodeThatContainsElement(bootstrapTables, this.el);
            if (tableContainer !== null) {
                var observer = new MutationObserver(mutations => {
                    mutations.forEach(mutation => {
                        var triggeringChild = nodeThatContainsElement(mutation.removedNodes, this.el);
                        if (triggeringChild !== null) {
                            chart.dispose();
                        }
                    });
                });

                var config:MutationObserverInit = {childList: true};
                observer.observe(tableContainer, config);
            }
        } else {
            console.error("Element should have been an svg element.");
        }
    }

    drawTrendChart(svgElement, savingsDriverPeriods, chartSettings, isSimpleChart = true, tip = null) {

        var exitDuration = 250;
        var updateDuration = 250;

        var svg = d3.select(svgElement);
        var data = savingsDriverPeriods;

        var yTicks = 3;

        var fullHeight = 120;
        var fullWidth = 500;

        var margin = {top: 10, right: 10, bottom: 25, left: 40};

        if (isSimpleChart) {
            fullHeight = 50;
            fullWidth = 300;
            yTicks = 3;
            margin = {top: 5, right: 5, bottom: 5, left: 5};
        }

        var width = fullWidth - margin.left - margin.right;
        var height = fullHeight - margin.top - margin.bottom;

        var x = d3.scale.ordinal()
            .rangePoints([0, width], 1);


        x.domain(data.map(chartSettings.xValue));

        var xLabel = d3.scale.ordinal().domain(data.map(chartSettings.xValue)).range(data.map(chartSettings.xAxisFormatter));

        var yRange = d3.extent(data, (d) => {
            if (chartSettings.isDefined(d)) {
                return chartSettings.yValue(d);
            }
            return null;
        });
        yRange[0] = Math.min(0, yRange[0]);

        var y = d3.scale.linear()
            .domain(yRange)
            .range([height, 0])
            .nice(yTicks);

        var line = d3.svg.line()
            .defined(chartSettings.isDefined)
            .x((d, i:number) => x(chartSettings.xValue(d)))
            .y((d) => y(chartSettings.yValue(d)));

        svg
            .datum(data)
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .classed("simple", isSimpleChart)
            .classed("full", !isSimpleChart);

        var gChart = svg.select("g.g-chart");
        if (gChart.empty()) {
            gChart = svg.append("g")
                .attr("class", "g-chart");
        }

        gChart
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        if (!isSimpleChart) {
            var xAxis = d3.svg.axis()
                .scale(x)
                .orient("bottom")
                .tickSize(0, 0)
                .tickPadding(10)
                .tickFormat(xLabel);

            var yAxis = d3.svg.axis()
                .ticks(yTicks)
                .scale(y)
                .orient("left")
                .innerTickSize(-width)
                .tickFormat(chartSettings.yAxisFormatter);

            var gXAxis = gChart.select("g.x.axis");
            if (gXAxis.empty()) {
                gXAxis = gChart.append("g")
                    .attr("class", "x axis");
            }
            gXAxis
                .attr("transform", "translate(0," + height + ")")
                .transition()
                .delay(exitDuration)
                .duration(updateDuration)
                .ease("exp-in-out")
                .call(xAxis);

            var gYAxis = gChart.select("g.y.axis");
            if (gYAxis.empty()) {
                gYAxis = gChart.append("g")
                    .attr("class", "y axis");
            }
            gYAxis
                .transition()
                .delay(exitDuration)
                .duration(updateDuration)
                .ease("exp-in-out")
                .call(yAxis);
        }

        var trendLine = gChart.select("path.trend-line");
        if (trendLine.empty()) {
            trendLine = gChart.append("path").attr("class", "line trend-line").style("opacity", 0);
        }
        trendLine
            .transition()
            .delay(exitDuration)
            .duration(updateDuration)
            .attr("d", line)
            .style("opacity", 1);

        if (typeof tip === "undefined" || tip === null) {
            tip = d3.tip().attr("class", "d3-tip animate");
            svg.call(tip);
        }

        tip.html((d) => {
            var val = chartSettings.yValueFormatter(chartSettings.yValue(d));
            var period = chartSettings.xValueFormatter(d);
            return `${period}<br /><strong>${val}</strong>`;
        });

        var specials = chartSettings.specialData(data);

        var dots = gChart.selectAll(".dot")
            .data(data.filter(chartSettings.isDefined), (d) => d.CalendarDetail.Id);

        dots.exit()
            .transition()
            .duration(exitDuration)
            .ease("exp-in-out")
            .style("opacity", 0)
            .remove();

        dots
            .classed("first", (d, i) => {
                return chartSettings.areSamePeriod(d, specials.first)
            })
            .classed("last", (d, i) => {
                return chartSettings.areSamePeriod(d, specials.last)
            })
            .classed("best", (d, i) => {
                return specials.best.some((v) => chartSettings.areSamePeriod(d, v))
            })
            .classed("worst", (d, i) => {
                return specials.worst.some((v) => chartSettings.areSamePeriod(d, v))
            })
            .classed("detached", (d, i) => {
                return specials.detached.some((v) => chartSettings.areSamePeriod(d, v))
            })
            .classed("highlight", (d, i) => {
                return specials.highlight.some((v) => chartSettings.areSamePeriod(d, v))
            })
            .transition()
            .delay(exitDuration)
            .duration(updateDuration)
            .ease("exp-in-out")
            .attr("cx", line.x())
            .attr("cy", line.y())
            .attr("r", 3.5)
            .style("opacity", 1);

        dots
            .enter().append("circle")
            .style("opacity", 0)
            .classed("dot", true)
            .classed("first", (d, i) => {
                return chartSettings.areSamePeriod(d, specials.first)
            })
            .classed("last", (d, i) => {
                return chartSettings.areSamePeriod(d, specials.last)
            })
            .classed("best", (d, i) => {
                return specials.best.some((v) => chartSettings.areSamePeriod(d, v))
            })
            .classed("worst", (d, i) => {
                return specials.worst.some((v) => chartSettings.areSamePeriod(d, v))
            })
            .classed("detached", (d, i) => {
                return specials.detached.some((v) => chartSettings.areSamePeriod(d, v))
            })
            .classed("highlight", (d, i) => {
                return specials.highlight.some((v) => chartSettings.areSamePeriod(d, v))
            })
            .attr("cx", line.x())
            .attr("cy", line.y())
            .attr("r", 3.5)
            .on("mouseover", tip.show)
            .on("mouseout", tip.hide)
            .transition()
            .delay(exitDuration + updateDuration)
            .ease("exp-in-out")
            .style("opacity", 1);

        return {
            dispose: () => {
                tip.destroy()
            },
            tip: tip
        };
    };
}