﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http.Dependencies;
using StructureMap;

namespace AS.Freshservice.Dashboard.DependencyResolution
{
    public class StructureMapResolver : IDependencyResolver
    {
        #region Member Variables

        protected IContainer Container;

        #endregion

        #region Constructors

        public StructureMapResolver(IContainer container)
        {
            if (container == null)
            {
                throw new ArgumentNullException("container");
            }
            Container = container;
        }

        #endregion

        #region IDependencyResolver Members

        public IDependencyScope BeginScope()
        {
            var child = Container.CreateChildContainer();
            return new StructureMapResolver(child);
        }

        #endregion

        #region IDependencyScope Members

        public object GetService(Type serviceType)
        {
            if (serviceType.IsClass)
            {
                return GetConcreteService(serviceType);
            }
            return GetInterfaceService(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            try
            {
                return Container.GetAllInstances(serviceType).Cast<object>();
            }
            catch (StructureMapException)
            {
                return null;
            }
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            Container.Dispose();
        }

        #endregion

        #region Private Methods

        private object GetConcreteService(Type serviceType)
        {
            try
            {
                return Container.GetInstance(serviceType);
            }
            catch (StructureMapException)
            {
                return null;
            }
        }

        private object GetInterfaceService(Type serviceType)
        {
            return Container.TryGetInstance(serviceType);
        }

        #endregion
    }
}