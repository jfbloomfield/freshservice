# AS.Crossbow

### Build Instructions

```sh
npm install
jspm install
gulp build
```

###Production Instructions
Production will need to be bundled so there is a separate gulp task that both builds and bundles the application (Just Program currently)

```sh
npm install
jspm install
gulp deploy
```

Compiled js and map files are output to the EntryPages/target folder with their respective page folders
Until the network page is upgraded to Angular2, we have separate gulp tasks for network and program.
In this case most tasks are suffixed by the page name with a hyphen

For example:
```sh
gulp watch-network

gulp html-program
```