﻿using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Filters;
using AS.Framework.Common;
using AS.Framework.Common.Helpers;

namespace AS.Crossbow.EntryPages.Attributes
{
    public class AimsWebApiAuthenticationAttribute : IAuthenticationFilter
    {
        #region Member Variables

        private readonly SecurityHelper _securityHelper = new SecurityHelper();

        #endregion

        #region Constructors

        public AimsWebApiAuthenticationAttribute(SecurityHelper securityHelper)
        {
            _securityHelper = securityHelper;
        }

        #endregion

        #region Properties

        public bool AllowMultiple { get; private set; }

        #endregion

        #region IAuthenticationFilter Members

        public Task AuthenticateAsync(HttpAuthenticationContext context, CancellationToken cancellationToken)
        {
            if (IsLoggedIn())
            {
                _securityHelper.PutUserOnThread();
            }
            return Task.FromResult(0);
        }

        public Task ChallengeAsync(
            HttpAuthenticationChallengeContext challengeContext,
            CancellationToken cancellationToken)
        {
            challengeContext.Result = new ResultWithAuthenticationChallengeWhenAppropriate(challengeContext.Result);
            return Task.FromResult(0);
        }

        #endregion

        #region Private Methods

        private bool IsLoggedIn()
        {
            if (HttpContext.Current != null && HttpContext.Current.Session != null)
            {
                return HttpContext.Current.Session[Constants.SessionConstants.USER_ID] != null;
            }
            return false;
        }

        #endregion
    }

    public class ResultWithAuthenticationChallengeWhenAppropriate : IHttpActionResult
    {
        #region Member Variables

        private readonly IHttpActionResult challengeContextResult;

        #endregion

        #region Constructors

        public ResultWithAuthenticationChallengeWhenAppropriate(IHttpActionResult challengeContextResult)
        {
            this.challengeContextResult = challengeContextResult;
        }

        #endregion

        #region IHttpActionResult Members

        public async Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            var response = await challengeContextResult.ExecuteAsync(cancellationToken);
            if (response.StatusCode == HttpStatusCode.Unauthorized)
            {
                response.Headers.WwwAuthenticate.Add(new AuthenticationHeaderValue("None", "AIMS"));
            }
            return response;
        }

        #endregion
    }
}