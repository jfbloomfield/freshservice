﻿using System;
using System.Web.Http;
using System.Web.Http.Controllers;
using AS.Framework.Common.Exceptions;
using AS.Framework.Common.Repositories;
using AS.Framework.Permissions.BusinessClasses;
using AS.Framework.Permissions.BusinessClasses.Interfaces;
using AS.Framework.Permissions.Common;
using AS.Framework.Permissions.Repositories;
using Elmah;

namespace AS.Crossbow.EntryPages.Attributes
{
    public class AimsWebApiAuthorizeAttribute : AuthorizeAttribute
    {
        #region Member Variables

        private readonly IPermissionBC _permissionBC;

        #endregion

        #region Constructors

        public AimsWebApiAuthorizeAttribute(IPermissionBC permissionBC)
        {
            _permissionBC = permissionBC;
        }

        public AimsWebApiAuthorizeAttribute(string permissionCode, PermissionEnums.PermissionType permissionType)
        {
            _permissionBC = new PermissionBC(new PermissionUserRepository(new RoleRepository()));
            PermissionCode = permissionCode;
            PermissionType = permissionType;
        }

        #endregion

        #region Properties

        public string PermissionCode { get; set; }
        public PermissionEnums.PermissionType PermissionType { get; set; }

        #endregion

        #region Private Methods

        protected override bool IsAuthorized(HttpActionContext httpActionContext)
        {
            using (new UnitOfWork())
            {
                if (!_permissionBC.DoesPermissionCodeExist(PermissionCode))
                {
                    throw new InvalidPermissionCodeException(PermissionCode);
                }
                if (!_permissionBC.HasPermission(PermissionCode, PermissionType))
                {
                    ErrorSignal.FromCurrentContext()
                        .Raise(
                            new NotAuthorizedException(
                                string.Format(
                                    "User does not have permission type:{0}, permissionCode:{1}",
                                    PermissionCode,
                                    Enum.GetName(
                                        typeof (PermissionEnums.PermissionType),
                                        (PermissionEnums.PermissionType) 1))));

                    return false;
                }
                return true;
            }
        }

        #endregion
    }
}