﻿//using System.Collections.Generic;
//using System.Configuration;
//using System.IdentityModel.Tokens;
//using System.Security.Claims;
//using System.Threading.Tasks;
//using System.Web.Helpers;
//using Microsoft.IdentityModel.Protocols;
//using Microsoft.Owin;
//using Microsoft.Owin.Security;
//using Microsoft.Owin.Security.Cookies;
//using Microsoft.Owin.Security.OpenIdConnect;
//using Owin;
//
////[assembly: OwinStartup(typeof(AS.Crossbow.EntryPages.Startup))]
//
//namespace AS.Crossbow.EntryPages
//{
//    public class ClaimTypes
//    {
//        public const string SUBJECT = "sub";
//        public const string GIVEN_NAME = "given_name";
//        public const string FAMILY_NAME = "family_name";
//        public const string ROLE = "role";
//    }
//    public partial class Startup
//    {
//       
//
//        #region Public Methods
//
//        public void Configuration(IAppBuilder app)
//        {
//            AntiForgeryConfig.UniqueClaimTypeIdentifier = ClaimTypes.SUBJECT;
//            JwtSecurityTokenHandler.InboundClaimTypeMap = new Dictionary<string, string>();
//
//            //app.UseCookieAuthentication(new CookieAuthenticationOptions { AuthenticationType = "Cookies" });
//            app.UseCookieAuthentication(new CookieAuthenticationOptions { CookieManager = new SystemWebCookieManager() });
//
//            app.UseOpenIdConnectAuthentication(
//                new OpenIdConnectAuthenticationOptions
//                {
//                    Authority = ConfigurationManager.AppSettings["IdentityServerEndpoint"],
//                    ClientId = "crossbow",
//                    RedirectUri = ConfigurationManager.AppSettings["IdentityServerRedirectUrl"],
//                    PostLogoutRedirectUri = ConfigurationManager.AppSettings["UserManagementRedirectUrl"],
//                    ResponseType = "id_token",
//                    Scope = "openid profile roles",
//                    SignInAsAuthenticationType = "Cookies",
//                    UseTokenLifetime = false,
//                    Notifications = new OpenIdConnectAuthenticationNotifications
//                    {
//                        SecurityTokenValidated = async n =>
//                        {
//                            var id = n.AuthenticationTicket.Identity;
//
//                            // we want to keep first name, last name, subject and roles
//                            var givenName = id.FindFirst(ClaimTypes.GIVEN_NAME);
//                            var familyName = id.FindFirst(ClaimTypes.FAMILY_NAME);
//                            var sub = id.FindFirst(ClaimTypes.SUBJECT);
//                            var roles = id.FindAll(ClaimTypes.ROLE);
//                            var tenantId = id.FindFirst("tid");
//                            var createdAt = id.FindFirst("created_at");
//
//
//                            // create new identity and set name and role claim type
//                            var nid = new ClaimsIdentity(
//                                id.AuthenticationType,
//                                ClaimTypes.GIVEN_NAME,
//                                ClaimTypes.ROLE);
//
//                            nid.AddClaim(givenName);
//                            nid.AddClaim(familyName);
//                            nid.AddClaim(sub);
//                            nid.AddClaims(roles);
//                            nid.AddClaim(tenantId);
//                            nid.AddClaim(createdAt);
//
//                            nid.AddClaim(new Claim("id_token", n.ProtocolMessage.IdToken));
//
//                            n.AuthenticationTicket = new AuthenticationTicket(nid, n.AuthenticationTicket.Properties);
//                        },
//                        RedirectToIdentityProvider = n =>
//                        {
//                            if (n.ProtocolMessage.RequestType == OpenIdConnectRequestType.LogoutRequest)
//                            {
//                                var idTokenHint = n.OwinContext.Authentication.User.FindFirst("id_token");
//
//                                if (idTokenHint != null)
//                                {
//                                    n.ProtocolMessage.IdTokenHint = idTokenHint.Value;
//                                }
//                            }
//
//                            return Task.FromResult(0);
//                        }
//                    }
//                });
//        }
//
//        #endregion  
//    }
//}