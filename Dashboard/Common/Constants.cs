﻿namespace AS.Crossbow.EntryPages.Common
{
    public class Constants
    {
        public static class AspParameters
        {
            public const string PAGE_TITLE = "NAVBAR_TITLE";
            public const string TENANT_ID = "TENANT_ID";
        }
        #region Nested Type: PermissionCode

        public static class PermissionCode
        {
            #region Constants

            public const string NETWORK_PAGE = "CB_NETWORK_PAGE";
            public const string PROGRAM_PAGE = "CB_PROGRAM_PAGE";
            public const string ADMIN_PAGES = "CROSSBOW_ADMIN_PAGES";

            #endregion
        }

        #endregion
    }
}