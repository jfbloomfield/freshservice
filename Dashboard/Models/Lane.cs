﻿namespace AS.Crossbow.EntryPages.Models
{
    public class Lane
    {
        #region Constructors

        public Lane(
            int id,
            int laneAnalysisId,
            Facility shipFrom,
            Facility shipTo,
            Vendor vendor,
            bool isSpotlightLane = false)
        {
            Id = id;
            LaneAnalysisId = laneAnalysisId;
            IsSpotlightLane = isSpotlightLane;
            ShipFrom = shipFrom;
            ShipTo = shipTo;
            Vendor = vendor;
        }

        #endregion

        #region Properties

        public int Id { get; }
        public bool IsSpotlightLane { get; }
        public int LaneAnalysisId { get; }
        public Facility ShipFrom { get; }
        public Facility ShipTo { get; }
        public Vendor Vendor { get; }

        #endregion
    }
}