﻿namespace AS.Crossbow.EntryPages.Models
{
    public class EntryPagesViewModel
    {
        #region Constructors

        public EntryPagesViewModel(string templateVersion, string userName, int tenantId, string userDomain, int userId)
        {
            TemplateVersion = templateVersion;
            UserName = userName;
            TenantId = tenantId;
            UserDomain = userDomain;
            UserId = userId;
        }

        #endregion

        #region Properties

        public string IsLoggedIn
        {
            get
            {
                if (UserName != string.Empty)
                {
                    return "true";
                }
                return "false";
            }
        }

        public string TemplateVersion { get; set; }

        public int TenantId { get; set; }
        public string UserDomain { get; set; }
        public int UserId { get; set; }
        public string UserName { get; set; }

        #endregion
    }
}