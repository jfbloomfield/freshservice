﻿using System.Collections.Generic;
using Newtonsoft.Json;
using TypeLite;

namespace AS.Crossbow.EntryPages.Models
{
    public class PagedResponse<T>
    {
        #region Constructors

        public PagedResponse(int total, IEnumerable<T> rows)
        {
            Total = total;
            Rows = rows;
        }

        #endregion

        #region Properties

        [JsonProperty("rows")]
        [TsProperty(Name = "rows")]
        public IEnumerable<T> Rows { get; set; }

        [JsonProperty("total")]
        [TsProperty(Name = "total")]
        public int Total { get; set; }

        #endregion
    }
}