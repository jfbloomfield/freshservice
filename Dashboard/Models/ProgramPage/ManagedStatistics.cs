﻿namespace AS.Crossbow.EntryPages.Models.ProgramPage
{
    public class ManagedStatistics
    {
        #region Constructors

        public ManagedStatistics(int? allCases, int? managedCases)
        {
            AllCases = allCases;
            ManagedCases = managedCases;
            if (ManagedCases.HasValue && AllCases.HasValue && AllCases.Value != 0)
            {
                ManagedCasesRatio = ManagedCases.Value / (double) AllCases.Value;
            }
        }

        #endregion

        #region Properties

        public int? AllCases { get; }
        public int? ManagedCases { get; }
        public double? ManagedCasesRatio { get; }

        #endregion
    }
}