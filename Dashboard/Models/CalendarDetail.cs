﻿using System;

namespace AS.Crossbow.EntryPages.Models
{
    public class CalendarDetail
    {
        #region Constructors

        public CalendarDetail(
            string abbreviation,
            int calendarId,
            DateTime endDate,
            int id,
            string name,
            short period,
            DateTime startDate)
        {
            Abbreviation = abbreviation;
            CalendarId = calendarId;
            EndDate = endDate;
            Id = id;
            Name = name;
            Period = period;
            StartDate = startDate;
        }

        #endregion

        #region Properties

        public string Abbreviation { get; }
        public int CalendarId { get; }
        public DateTime EndDate { get; }
        public int Id { get; }
        public string Name { get; }
        public short Period { get; }
        public DateTime StartDate { get; }

        #endregion
    }
}