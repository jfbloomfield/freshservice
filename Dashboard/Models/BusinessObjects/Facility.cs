﻿namespace AS.Crossbow.EntryPages.Models
{
    public class Facility
    {
        #region Constructors

        public Facility(string city, string name, string state, string zip)
        {
            City = city;
            Name = name;
            State = state;
            Zip = zip;
        }

        #endregion

        #region Properties

        public string City { get; }
        public string Name { get; }
        public string State { get; }
        public string Zip { get; }

        #endregion
    }
}