﻿namespace AS.Crossbow.EntryPages.Models.NetworkPage
{
    public enum PeriodType
    {
        Monthly = 0,
        FiscalPeriod = 1
    }
}