﻿namespace AS.Crossbow.EntryPages.Models.BusinessObjects
{
    public class DistributionCenter
    {
        #region Properties

        public bool HasData { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }

        #endregion
    }
}