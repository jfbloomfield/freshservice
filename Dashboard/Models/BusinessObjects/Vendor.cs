﻿namespace AS.Crossbow.EntryPages.Models
{
    public class Vendor
    {
        #region Constructors

        public Vendor(int id, string name)
        {
            Id = id;
            Name = name;
        }

        #endregion

        #region Properties

        public int Id { get; }
        public string Name { get; }

        #endregion
    }
}