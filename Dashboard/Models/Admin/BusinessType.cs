﻿namespace AS.Crossbow.EntryPages.Models.Admin
{
    public class BusinessType
    {
        #region Properties

        public string Description { get; set; }
        public int Id { get; set; }

        #endregion
    }
}