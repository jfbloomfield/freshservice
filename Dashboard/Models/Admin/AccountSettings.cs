﻿using System.Collections.Generic;

namespace AS.Crossbow.EntryPages.Models.Admin
{
    public class AccountSettings
    {
        #region Properties

        public int Id { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public BusinessType BusinessType { get; set; }
        public IList<BusinessType> BusinessTypes { get; set; }
        public string City { get; set; }
        public string CompanyName { get; set; }
        public string ContactName { get; set; }
        public string PhoneNumber { get; set; }
        public string State { get; set; }
        public IList<string> States { get; set; }
        public string ZipCode { get; set; }

        #endregion
    }
}