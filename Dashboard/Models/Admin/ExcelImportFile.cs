﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AS.ValidationEngine.Model;

namespace AS.Crossbow.EntryPages.Models.Admin
{
    public class ExcelImportFile 
    {
        public string CheckFilePath { get; set; }
        public string OriginalFileName { get; set; }
        public ValidatedDataSet ValidateDataSet { get; set; }

        public Guid StoredFileName { get; set; }
        public int NumberOfRecords { get; set; }
        public  int TenantId { get; set; }
    }
}