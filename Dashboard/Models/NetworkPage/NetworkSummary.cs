﻿using AS.Crossbow.EntryPages.Models.NetworkPage.PillSummary;

namespace AS.Crossbow.EntryPages.Models.NetworkPage
{
    public class NetworkSummary
    {
        #region Properties

        public NetworkChartPeriod InitialEndPeriod { get; set; }
        public NetworkChartPeriod InitialStartPeriod { get; set; }
        public NetworkChart NetworkChart { get; set; }
        public PillSummaries PillSummaries { get; set; }

        #endregion
    }
}