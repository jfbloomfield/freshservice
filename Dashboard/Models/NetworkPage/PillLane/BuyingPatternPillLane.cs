﻿namespace AS.Crossbow.EntryPages.Models.NetworkPage.PillLane
{
    public class BuyingPatternPillLane : PillLane
    {
        #region Properties

        public decimal ChangeEquipmentUtilizationPercent { get; set; }
        public decimal ChangeInPoundsPerShipment { get; set; }

        #endregion
    }
}