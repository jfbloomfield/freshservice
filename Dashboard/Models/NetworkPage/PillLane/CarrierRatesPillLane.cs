﻿namespace AS.Crossbow.EntryPages.Models.NetworkPage.PillLane
{
    public class CarrierRatesPillLane : PillLane
    {
        #region Properties

        public decimal ChangeInLineHaulRatePerMile { get; set; }
        public decimal ChangeInLineHaulCostPerCwt{ get; set; }

        #endregion
    }
}