﻿namespace AS.Crossbow.EntryPages.Models.NetworkPage.PillLane
{
    public class VolumePillLane : PillLane
    {
        #region Properties

        public decimal ChangeInPoundsPerShipment { get; set; }
        public decimal ChangeInPoundsShipped { get; set; }

        #endregion
    }
}