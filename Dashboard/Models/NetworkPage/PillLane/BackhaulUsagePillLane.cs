﻿namespace AS.Crossbow.EntryPages.Models.NetworkPage.PillLane
{
    public class BackhaulUsagePillLane : PillLane
    {
        #region Properties

        public decimal BackhaulLoadsDelta { get; set; }
        public decimal PoundsPerBackhaulDelta { get; set; }

        #endregion
    }
}