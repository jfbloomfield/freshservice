﻿namespace AS.Crossbow.EntryPages.Models.NetworkPage.PillLane
{
    public class NewPillLane : PillLane
    {
        #region Properties

        public decimal NewAllowance { get; set; }
        public decimal NewCost { get; set; }

        #endregion
    }
}