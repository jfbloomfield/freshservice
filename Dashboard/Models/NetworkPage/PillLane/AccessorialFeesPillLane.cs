﻿namespace AS.Crossbow.EntryPages.Models.NetworkPage.PillLane
{
    public class AccessorialFeesPillLane : PillLane
    {
        #region Properties

        public decimal AccessorialFeesPerLoadDelta { get; set; }

        #endregion
    }
}