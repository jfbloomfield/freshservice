﻿namespace AS.Crossbow.EntryPages.Models.NetworkPage.PillLane
{
    public class FreightAllowancePillLane : PillLane
    {
        #region Properties

        public decimal AllowancePerCWTDelta { get; set; }
        public decimal LinehaulCostPerCWTDelta { get; set; }

        #endregion
    }
}