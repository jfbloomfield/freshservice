﻿namespace AS.Crossbow.EntryPages.Models.NetworkPage.PillLane
{
    public class FuelPillLane : PillLane
    {
        #region Properties

        public decimal FuelPercentOfCostDelta { get; set; }
        public decimal FuelCostPerLoadDelta { get; set; }

        #endregion
    }
}