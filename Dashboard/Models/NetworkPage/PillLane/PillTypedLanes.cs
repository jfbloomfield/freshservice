﻿using System.Collections.Generic;

namespace AS.Crossbow.EntryPages.Models.NetworkPage.PillLane
{
    public class PillTypedLanes
    {
        #region Constructors

        public PillTypedLanes(PillType pillType)
        {
            PillType = pillType;
        }

        #endregion

        #region Properties

        public List<PillLane> PillLanes { get; set; }
        public PillType PillType { get; private set; }

        #endregion
    }
}