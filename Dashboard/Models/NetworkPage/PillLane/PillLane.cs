﻿namespace AS.Crossbow.EntryPages.Models.NetworkPage.PillLane
{
    public class PillLane
    {
        #region Properties

        public Lane Lane { get; set; }
        public decimal PercentImpact { get; set; }
        public decimal SavingsImpact { get; set; }

        #endregion
    }
}