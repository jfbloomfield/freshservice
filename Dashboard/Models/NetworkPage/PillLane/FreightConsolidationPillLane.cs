﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AS.Crossbow.EntryPages.Models.NetworkPage.PillLane
{
    public class FreightConsolidationPillLane : PillLane
    {
        public decimal PercentLoadsConsolidatedDelta { get; set; }
        public decimal UtilizationDelta { get; set; }
    }
}