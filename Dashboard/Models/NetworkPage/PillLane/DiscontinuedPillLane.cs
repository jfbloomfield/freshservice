﻿namespace AS.Crossbow.EntryPages.Models.NetworkPage.PillLane
{
    public class DiscontinuedPillLane : PillLane
    {
        #region Properties

        public decimal FormerAllowance { get; set; }
        public decimal FormerCost { get; set; }

        #endregion
    }
}