﻿using System.Collections.Generic;
using System.Linq;

namespace AS.Crossbow.EntryPages.Models.NetworkPage
{
    public class SavingsDriverLane
    {
        #region Constructors

        public SavingsDriverLane(Lane lane, IList<SavingsDriverPillLanePeriod> periods)
        {
            Lane = lane;
            Periods = periods;
        }

        #endregion

        #region Properties

        public double AccessorialFeesImpact => Periods.Skip(1).Sum(p => p.AccessorialFeesImpact);
        public double BackhaulUsageImpact => Periods.Skip(1).Sum(p => p.BackhaulUsageImpact);
        public double BuyingPatternImpact => Periods.Skip(1).Sum(p => p.BuyingPatternImpact);
        public double CarrierRatesImpact => Periods.Skip(1).Sum(p => p.CarrierRatesImpact);
        public double DiscontinuedLanesImpact => Periods.Skip(1).Sum(p => p.DiscontinuedLanesImpact);
        public double FreightAllowancesImpact => Periods.Skip(1).Sum(p => p.FreightAllowancesImpact);
        public double FreightConsolidationImpact => Periods.Skip(1).Sum(p => p.FreightConsolidationImpact);
        public double FuelImpact => Periods.Skip(1).Sum(p => p.FuelImpact);
        public Lane Lane { get; }
        public double NewLanesImpact => Periods.Skip(1).Sum(p => p.NewLanesImpact);
        public IList<SavingsDriverPillLanePeriod> Periods { get; }
        public double VolumeImpact => Periods.Skip(1).Sum(p => p.VolumeImpact);

        #endregion
    }
}