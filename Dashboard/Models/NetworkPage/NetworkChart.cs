﻿using System.Collections.Generic;

namespace AS.Crossbow.EntryPages.Models.NetworkPage
{
    public class NetworkChart
    {
        #region Properties

        public List<NetworkChartBar> Bars { get; set; }
        public PeriodType PeriodType { get; set; }

        #endregion
    }
}