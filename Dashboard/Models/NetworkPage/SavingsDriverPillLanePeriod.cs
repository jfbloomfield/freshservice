﻿namespace AS.Crossbow.EntryPages.Models.NetworkPage
{
    public class SavingsDriverPillLanePeriod : SavingsDriverPillPeriod
    {
        #region Constructors

        public SavingsDriverPillLanePeriod(
            int laneId,
            int tenantId,
            int calendarDetailId,
            CalendarDetail calendarDetail,
            int shipmentCount,
            double weightPerShipment,
            double buyingPatternImpact,
            double weight,
            double volumeImpact,
            double carrierRatesImpact,
            double? ratesTlPerMile,
            double freightAllowancesImpact,
            double allowancePerCwt,
            double newLanesImpact,
            int newLanesCount,
            double discontinuedLanesImpact,
            int discontinuedLanesCount,
            double savings,
            double freightConsolidationImpact,
            int loadCount,
            int loadCountMultiStop,
            double backhaulUsageImpact,
            int loadCountBackhaul,
            double accessorialFeesImpact,
            double accessorialFeesPerLoad,
            double fuelImpact,
            double fuelPercentCost,
            int shipmentCountTl,
            int shipmentCountLtl,
            int shipmentCountIm)
            : base(
                calendarDetailId: calendarDetailId,
                calendarDetail: calendarDetail,
                tenantId: tenantId,
                shipmentCount: shipmentCount,
                weightPerShipment: weightPerShipment,
                buyingPatternImpact: buyingPatternImpact,
                weight: weight,
                volumeImpact: volumeImpact,
                carrierRatesImpact: carrierRatesImpact,
                ratesTlPerMile: ratesTlPerMile,
                freightAllowancesImpact: freightAllowancesImpact,
                allowancePerCwt: allowancePerCwt,
                newLanesImpact: newLanesImpact,
                newLanesCount: newLanesCount,
                discontinuedLanesImpact: discontinuedLanesImpact,
                discontinuedLanesCount: discontinuedLanesCount,
                savings: savings,
                freightConsolidationImpact: freightConsolidationImpact,
                loadCount: loadCount,
                loadCountMultiStop: loadCountMultiStop,
                backhaulUsageImpact: backhaulUsageImpact,
                loadCountBackhaul: loadCountBackhaul,
                accessorialFeesImpact: accessorialFeesImpact,
                accessorialFeesPerLoad: accessorialFeesPerLoad,
                fuelImpact: fuelImpact,
                fuelPercentCost: fuelPercentCost,
                shipmentCountTl: shipmentCountTl,
                shipmentCountLtl: shipmentCountLtl,
                shipmentCountIm: shipmentCountIm)
        {
            LaneId = laneId;
        }

        #endregion

        #region Properties

        public int LaneId { get; }

        #endregion
    }
}