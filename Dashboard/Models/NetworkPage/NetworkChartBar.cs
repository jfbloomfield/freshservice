﻿namespace AS.Crossbow.EntryPages.Models.NetworkPage
{
    public class NetworkChartBar
    {
        #region Properties

        public NetworkChartPeriod Period { get; set; }
        public decimal? Value { get; set; }

        #endregion
    }
}