﻿namespace AS.Crossbow.EntryPages.Models.NetworkPage
{
    public enum PillType
    {
        VolumeImpact = 0,
        CarrierRatesImpact = 1,
        BuyingPatternImpact = 2,
        BackhaulUsageImpact = 3,
        AccessorialFeesImpact = 4,
        FreightConsolidationImpact = 5,
        FuelImpact = 6,
        FreightAllowancesImpact = 7,
        NewLanesImpact = 8,
        DiscontinuedLanesImpact = 9,
        ConsolidationImpact = 10
    }
}