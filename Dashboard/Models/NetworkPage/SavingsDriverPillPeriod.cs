﻿namespace AS.Crossbow.EntryPages.Models.NetworkPage
{
    public class SavingsDriverPillPeriod
    {
        #region Constructors

        public SavingsDriverPillPeriod(
            int tenantId,
            int calendarDetailId,
            CalendarDetail calendarDetail,
            int shipmentCount,
            double weightPerShipment,
            double buyingPatternImpact,
            double weight,
            double volumeImpact,
            double carrierRatesImpact,
            double? ratesTlPerMile,
            double freightAllowancesImpact,
            double allowancePerCwt,
            double newLanesImpact,
            int newLanesCount,
            double discontinuedLanesImpact,
            int discontinuedLanesCount,
            double savings,
            double freightConsolidationImpact,
            int loadCount,
            int loadCountMultiStop,
            double backhaulUsageImpact,
            int loadCountBackhaul,
            double accessorialFeesImpact,
            double accessorialFeesPerLoad,
            double fuelImpact,
            double fuelPercentCost,
            int shipmentCountTl,
            int shipmentCountLtl,
            int shipmentCountIm)
        {
            ShipmentCount = shipmentCount;
            WeightPerShipment = weightPerShipment;
            BuyingPatternImpact = buyingPatternImpact;
            Weight = weight;
            VolumeImpact = volumeImpact;
            CarrierRatesImpact = carrierRatesImpact;
            RatesTlPerMile = ratesTlPerMile;
            FreightAllowancesImpact = freightAllowancesImpact;
            AllowancePerCwt = allowancePerCwt;
            NewLanesImpact = newLanesImpact;
            NewLanesCount = newLanesCount;
            DiscontinuedLanesImpact = discontinuedLanesImpact;
            DiscontinuedLanesCount = discontinuedLanesCount;
            Savings = savings;
            FreightConsolidationImpact = freightConsolidationImpact;
            LoadCount = loadCount;
            LoadCountMultiStop = loadCountMultiStop;
            BackhaulUsageImpact = backhaulUsageImpact;
            LoadCountBackhaul = loadCountBackhaul;
            AccessorialFeesImpact = accessorialFeesImpact;
            AccessorialFeesPerLoad = accessorialFeesPerLoad;
            FuelImpact = fuelImpact;
            FuelPercentCost = fuelPercentCost;
            ShipmentCountTl = shipmentCountTl;
            ShipmentCountLtl = shipmentCountLtl;
            ShipmentCountIm = shipmentCountIm;
            TenantId = tenantId;
            CalendarDetailId = calendarDetailId;
            CalendarDetail = calendarDetail;
        }

        #endregion

        #region Properties

        public double AccessorialFeesImpact { get; }
        public double AccessorialFeesPerLoad { get; }
        public double AllowancePerCwt { get; }
        public double BackhaulUsageImpact { get; }
        public double BuyingPatternImpact { get; }
        public CalendarDetail CalendarDetail { get; }
        public int CalendarDetailId { get; }
        public double CarrierRatesImpact { get; }
        public int DiscontinuedLanesCount { get; }
        public double DiscontinuedLanesImpact { get; }
        public double FreightAllowancesImpact { get; }
        public double FreightConsolidationImpact { get; }
        public double FuelImpact { get; }
        public double FuelPercentCost { get; }
        public int ShipmentCountTl { get; set; }
        public int ShipmentCountLtl { get; set; }
        public int ShipmentCountIm { get; set; }

        public double? LoadBackhaulPercent => LoadCount == 0 ? (double?) null : LoadCountBackhaul / (double) LoadCount;

        public int LoadCount { get; }
        public int LoadCountBackhaul { get; }
        public int LoadCountMultiStop { get; }

        public double? LoadMultiStopPercent => LoadCount == 0 ? (double?) null : LoadCountMultiStop / (double) LoadCount
            ;

        public int NewLanesCount { get; }
        public double NewLanesImpact { get; }
        public double? RatesTlPerMile { get; }
        public double Savings { get; }
        public int ShipmentCount { get; }
        public int SingleDiscontinuedLanesCount { get; set; }
        public int SingleNewLanesCount { get; set;  }
        public int TenantId { get; }
        public double VolumeImpact { get; }
        public double Weight { get; }
        public double WeightPerShipment { get; }

        #endregion
    }
}