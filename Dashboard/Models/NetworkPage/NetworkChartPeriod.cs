﻿using System;

namespace AS.Crossbow.EntryPages.Models.NetworkPage
{
    public class NetworkChartPeriod
    {
        #region Properties

        public int CalendarDetailId { get; set; }
        public string Code { get; set; }
        public string Desc { get; set; }
        public DateTime EndDate { get; set; }
        public int Period { get; set; }
        public DateTime StartDate { get; set; }
        public int Year { get; set; }

        #endregion
    }
}