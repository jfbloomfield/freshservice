﻿namespace AS.Crossbow.EntryPages.Models.NetworkPage.PillSummary
{
    public class FreightAllowancesPillSummary : PillSummary
    {
        #region Properties

        public decimal? EndingAllowancePerCase { get; set; }
        public decimal EndingAllowancePerCwt { get; set; }
        public decimal? EndingTruckloadLineHaulCostPerCwt { get; set; }
        public decimal PillSummaryAllowancePerPoundEnd { get; set; }
        public decimal PillSummaryAllowancePerPoundStart { get; set; }
        public decimal? StartingAllowancePerCase { get; set; }
        public decimal StartingAllowancePerCwt { get; set; }
        public decimal? StartingTruckloadLineHaulCostPerCwt { get; set; }

        #endregion
    }
}