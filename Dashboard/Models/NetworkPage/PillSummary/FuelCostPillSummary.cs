﻿namespace AS.Crossbow.EntryPages.Models.NetworkPage.PillSummary
{
    public class FuelCostPillSummary : PillSummary
    {
        #region Properties

        public decimal? EndingDeptOfEnergyDieselIndexCostPerCwt { get; set; }
        public decimal? EndingFuelCostPercentOfLoadCost { get; set; }
        public decimal? EndingFuelCostPerMile { get; set; }
        public decimal PillSummaryFuelCostPercentEnd { get; set; }
        public decimal PillSummaryFuelCostPercentStart { get; set; }
        public decimal? StartingDeptOfEnergyDieselIndexCostPerCwt { get; set; }
        public decimal? StartingFuelCostPercentOfLoadCost { get; set; }
        public decimal? StartingFuelCostPerMile { get; set; }

        #endregion
    }
}