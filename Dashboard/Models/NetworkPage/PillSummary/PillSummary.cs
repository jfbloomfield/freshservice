﻿namespace AS.Crossbow.EntryPages.Models.NetworkPage.PillSummary
{
    public class PillSummary
    {
        #region Properties

        public decimal ImpactAmount { get; set; }

        #endregion
    }
}