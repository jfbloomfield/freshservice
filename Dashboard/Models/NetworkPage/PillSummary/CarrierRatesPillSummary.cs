﻿namespace AS.Crossbow.EntryPages.Models.NetworkPage.PillSummary
{
    public class CarrierRatesPillSummary : PillSummary
    {
        #region Properties

        public decimal? EndingIntermodalLineHaulCostPerMile { get; set; }
        public decimal? EndingLtlLineHaulCostPerCwt { get; set; }
        public decimal? EndingTruckloadLineHaulCostPerMile { get; set; }
        public decimal PillSummaryNonBHLinehaulCostPerMileEnd { get; set; }
        public decimal PillSummaryNonBHLinehaulCostPerMileStart { get; set; }
        public decimal? StartingIntermodalLineHaulCostPerMile { get; set; }
        public decimal? StartingLtlLineHaulCostPerCwt { get; set; }
        public decimal? StartingTruckloadLineHaulCostPerMile { get; set; }

        #endregion
    }
}