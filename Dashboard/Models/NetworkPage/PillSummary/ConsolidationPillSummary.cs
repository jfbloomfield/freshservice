﻿namespace AS.Crossbow.EntryPages.Models.NetworkPage.PillSummary
{
    public class ConsolidationPillSummary : PillSummary
    {
        #region Properties

        public decimal? EndingConsolidatedLoads { get; set; }
        public decimal? EndingLtlLoadsConsolidatedToTruckload { get; set; }
        public decimal? EndingPercentOfAllLoadsConsolidated { get; set; }
        public decimal PillSummaryMultiStopPercentEnd { get; set; }
        public decimal PillSummaryMultiStopPercentStart { get; set; }
        public decimal? StartingConsolidatedLoads { get; set; }
        public decimal? StartingLtlLoadsConsolidatedToTruckload { get; set; }
        public decimal? StartingPercentOfAllLoadsConsolidated { get; set; }

        #endregion
    }
}