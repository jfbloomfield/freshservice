﻿namespace AS.Crossbow.EntryPages.Models.NetworkPage.PillSummary
{
    public class NewLanesPillSummary : PillSummary
    {
        #region Properties

        public decimal NewLanesUnderManagement { get; set; }
        public decimal? NewMonthlyAllowance { get; set; }
        public decimal PillSummaryNewLaneCount { get; set; }
        public decimal? SavingsOnNewLanes { get; set; }

        #endregion
    }
}