﻿namespace AS.Crossbow.EntryPages.Models.NetworkPage.PillSummary
{
    public class AccessorialFeesPillSummary : PillSummary
    {
        #region Properties

        public decimal? EndingDetentionChargesPerLoad { get; set; }
        public decimal EndingFeesPerLoad { get; set; }
        public decimal? EndingPercentOfTotalCost { get; set; }
        public decimal PillSummaryAccessorialFeesPerLoadEnd { get; set; }
        public decimal PillSummaryAccessorialFeesPerLoadStart { get; set; }
        public decimal? StartingDetentionChargesPerLoad { get; set; }
        public decimal StartingFeesPerLoad { get; set; }
        public decimal? StartingPercentOfTotalCost { get; set; }

        #endregion
    }
}