﻿using System.Collections.Generic;

namespace AS.Crossbow.EntryPages.Models.NetworkPage.PillSummary
{
    public class PillSummaries
    {
        #region Properties

        public AccessorialFeesPillSummary AccessorialFeesPillSummary { get; set; }
        public BackhaulUsagePillSummary BackhaulUsagePillSummary { get; set; }
        public BuyingPatternPillSummary BuyingPatternPillSummary { get; set; }
        public CarrierRatesPillSummary CarrierRatesPillSummary { get; set; }
        public ConsolidationPillSummary ConsolidationPillSummary { get; set; }
        public DiscontinuedLanesPillSummary DiscontinuedLanesPillSummary { get; set; }
        public FreightAllowancesPillSummary FreightAllowancesPillSummary { get; set; }
        public FuelCostPillSummary FuelCostPillSummary { get; set; }
        public NewLanesPillSummary NewLanesPillSummary { get; set; }
        public IList<SavingsDriverPillPeriod> SavingsDriverPillPeriods { get; set; }
        public VolumePillSummary VolumePillSummary { get; set; }

        #endregion
    }
}