﻿namespace AS.Crossbow.EntryPages.Models.NetworkPage.PillSummary
{
    public class VolumePillSummary : PillSummary
    {
        #region Properties

        public decimal EndingPoundsPerShipment { get; set; }
        public decimal? EndingShippedCases { get; set; }
        public decimal EndingShippedPounds { get; set; }
        public decimal PillSummaryWeightEnd { get; set; }
        public decimal PillSummaryWeightStart { get; set; }
        public decimal StartingPoundsPerShipment { get; set; }
        public decimal? StartingShippedCases { get; set; }
        public decimal StartingShippedPounds { get; set; }

        #endregion
    }
}