﻿namespace AS.Crossbow.EntryPages.Models.NetworkPage.PillSummary
{
    public class BuyingPatternPillSummary : PillSummary
    {
        #region Properties

        public decimal? EndingEquipmentUtilizationPercent { get; set; }
        public decimal? EndingLtlUsagePercent { get; set; }
        public decimal EndingPoundsPerShipment { get; set; }
        public decimal PillSummaryWeightPerShipmentEnd { get; set; }
        public decimal PillSummaryWeightPerShipmentStart { get; set; }
        public decimal? StartingEquipmentUtilizationPercent { get; set; }
        public decimal? StartingLtlUsagePercent { get; set; }
        public decimal StartingPoundsPerShipment { get; set; }

        #endregion
    }
}