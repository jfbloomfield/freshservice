﻿namespace AS.Crossbow.EntryPages.Models.NetworkPage.PillSummary
{
    public class DiscontinuedLanesPillSummary : PillSummary
    {
        #region Properties

        public decimal FreightLanesDiscontinuedByManagement { get; set; }
        public decimal? LostMonthlyAllowance { get; set; }
        public decimal PillSummaryDiscontinuedLaneCount { get; set; }
        public decimal? PriorSavingsOnDiscontinuedLanes { get; set; }

        #endregion
    }
}