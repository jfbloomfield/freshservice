﻿namespace AS.Crossbow.EntryPages.Models.NetworkPage.PillSummary
{
    public class BackhaulUsagePillSummary : PillSummary
    {
        #region Properties

        public decimal? EndingCases { get; set; }
        public decimal EndingLoadsPercent { get; set; }
        public decimal? EndingPoundsPerLoad { get; set; }
        public decimal PillSummaryBackhaulUsageEnd { get; set; }
        public decimal PillSummaryBackhaulUsageStart { get; set; }
        public decimal? StartingCases { get; set; }
        public decimal StartingLoadsPercent { get; set; }
        public decimal? StartingPoundsPerLoad { get; set; }

        #endregion
    }
}