﻿using System.Collections.Generic;

namespace AS.Crossbow.EntryPages.Models
{
    public class Calendar
    {
        #region Constructors

        public Calendar(int id, bool isDefault, string name, int tenantId, IList<CalendarDetail> periods)
        {
            Id = id;
            IsDefault = isDefault;
            Name = name;
            TenantId = tenantId;
            Periods = periods;
        }

        #endregion

        #region Properties

        public int Id { get; }
        public bool IsDefault { get; }
        public string Name { get; }
        public IList<CalendarDetail> Periods { get; }
        public int TenantId { get; }

        #endregion
    }
}