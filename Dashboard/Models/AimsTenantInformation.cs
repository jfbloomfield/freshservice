﻿namespace AS.Crossbow.EntryPages.Models
{
    public class AimsTenantInformation
    {
        #region Properties

        public string DbServerName { get; set; }
        public int Tenant_ID { get; set; }

        #endregion
    }
}