module AS.Crossbow.EntryPages.Models.NetworkPage {
	export const enum PeriodType {
		Monthly = 0,
		FiscalPeriod = 1
	}
	export const enum PillType {
		VolumeImpact = 0,
		CarrierRatesImpact = 1,
		BuyingPatternImpact = 2,
		BackhaulUsageImpact = 3,
		AccessorialFeesImpact = 4,
		FreightConsolidationImpact = 5,
		FuelImpact = 6,
		FreightAllowancesImpact = 7,
		NewLanesImpact = 8,
		DiscontinuedLanesImpact = 9,
		ConsolidationImpact = 10
	}
}
module System {
	export const enum DayOfWeek {
		Sunday = 0,
		Monday = 1,
		Tuesday = 2,
		Wednesday = 3,
		Thursday = 4,
		Friday = 5,
		Saturday = 6
	}
}
module System.Data {
	export const enum SchemaSerializationMode {
		IncludeSchema = 1,
		ExcludeSchema = 2
	}
	export const enum SerializationFormat {
		Xml = 0,
		Binary = 1
	}
}
module System.Globalization {
	export const enum CalendarAlgorithmType {
		Unknown = 0,
		SolarCalendar = 1,
		LunarCalendar = 2,
		LunisolarCalendar = 3
	}
	export const enum CalendarWeekRule {
		FirstDay = 0,
		FirstFullWeek = 1,
		FirstFourDayWeek = 2
	}
	export const enum CultureTypes {
		NeutralCultures = 1,
		SpecificCultures = 2,
		InstalledWin32Cultures = 4,
		AllCultures = 7,
		UserCustomCulture = 8,
		ReplacementCultures = 16,
		WindowsOnlyCultures = 32,
		FrameworkCultures = 64
	}
	export const enum DigitShapes {
		Context = 0,
		None = 1,
		NativeNational = 2
	}
}

