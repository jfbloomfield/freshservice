﻿using System.Linq;
using AS.Crossbow.Core.Data;
using FluentAssertions;
using NUnit.Framework;

namespace Core.Tests.Data
{
    [TestFixture]
    public class SafeDataReaderTests
    {
        #region Tests

        [Test]
        public void AsEnumerable_Can()
        {
            var repository = new MyRepository();

            using (var connection = ConnectionManager.GetOpenReadOnlyConnection(1))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (var command = transaction.CreateCommand())
                    {
                        command.CommandText = "select * from mode";

                        using (var safeDataReader = repository.MyExecuteSafeDataReader(command))
                        {
                            var count = safeDataReader.AsEnumerable().Count();

                            count.Should().BeGreaterThan(0);
                        }
                    }
                }
            }
        }

        [Test]
        public void HasRows_Can()
        {
            var repository = new MyRepository();

            using (var connection = ConnectionManager.GetOpenReadOnlyConnection(1))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (var command = transaction.CreateCommand())
                    {
                        command.CommandText = "select * from mode";

                        using (var safeDataReader = repository.MyExecuteSafeDataReader(command))
                        {
                            safeDataReader.HasRows().Should().Be(true);
                        }
                    }
                }
            }
        }

        [Test]
        public void ToDataTable_Can()
        {
            var repository = new MyRepository();

            using (var connection = ConnectionManager.GetOpenReadOnlyConnection(1))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (var command = transaction.CreateCommand())
                    {
                        command.CommandText = "select * from mode";

                        using (var safeDataReader = repository.MyExecuteSafeDataReader(command))
                        {
                            var dt = safeDataReader.ToDataTable();

                            dt.Rows.Count.Should().BeGreaterThan(0);
                        }
                    }
                }
            }
        }

        #endregion
    }
}