﻿using System;
using System.Data;
using System.Data.Common;
using AS.Crossbow.Core.Common;
using AS.Crossbow.Core.Data;
using FluentAssertions;
using NUnit.Framework;

namespace Core.Tests.Data
{
    [TestFixture]
    public class BaseRepositoryTests
    {
        #region Tests

        [Test]
        public void ExecuteNonQuery_Can()
        {
            var repository = GetObjectToTest();

            using (var connection = ConnectionManager.GetOpenReadOnlyConnection(1))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (var command = transaction.CreateCommand())
                    {
                        command.CommandText = "select 1 from DUAL";

                        Action act = () => repository.MyExecuteNonQuery(command);

                        act.ShouldNotThrow();
                    }
                }
            }
        }

        [Test]
        public void ExecuteNonQuery_Can_Rollback_Transaction()
        {
            const string INSERT = @"
            INSERT INTO mode
            (Code, Name)
            VALUES
            (@code, @name)
";

            const string SELECT = @"
            SELECT * FROM mode where CODE=@code and Name=@name            
";

            var repository = GetObjectToTest();

            using (var connection = ConnectionManager.GetOpenWritableConnection(1))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (var command = transaction.CreateCommand())
                    {
                        command.CommandText = INSERT;

                        command.AddInputParameter("code", "TEST");
                        command.AddInputParameter("name", "TEST");

                        repository.MyExecuteNonQuery(command);
                    }

                    using (var command = transaction.CreateCommand())
                    {
                        command.CommandText = SELECT;

                        command.AddInputParameter("code", "TEST");
                        command.AddInputParameter("name", "TEST");

                        using (var reader = (DbDataReader) repository.MyExecuteReader(command))
                        {
                            reader.HasRows.Should().Be(true);
                        }
                    }

                    transaction.Rollback();
                }

                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (var command = transaction.CreateCommand())
                    {
                        command.CommandText = SELECT;

                        command.AddInputParameter("code", "TEST");
                        command.AddInputParameter("name", "TEST");

                        using (var reader = (DbDataReader) repository.MyExecuteReader(command))
                        {
                            reader.HasRows.Should().Be(false);
                        }
                    }
                }
            }
        }

        [Test]
        public void ExecuteNonQuery_CommandTimeout_Defaults_To_Thirty_Seconds()
        {
            var repository = GetObjectToTest();

            using (var connection = ConnectionManager.GetOpenReadOnlyConnection(1))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (var command = transaction.CreateCommand())
                    {
                        command.CommandText = "select 1 from DUAL";
                        repository.MyExecuteNonQuery(command);

                        command.CommandTimeout.Should().Be(30);
                    }
                }
            }
        }

        [Test]
        public void ExecuteNonQuery_CommandTimeout_Overload_Is_Utilized()
        {
            var repository = GetObjectToTest();

            using (var connection = ConnectionManager.GetOpenReadOnlyConnection(1))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (var command = transaction.CreateCommand())
                    {
                        command.CommandText = "select 1 from DUAL";
                        repository.MyExecuteNonQuery(command, 20);

                        command.CommandTimeout.Should().Be(20);
                    }
                }
            }
        }

        [Test]
        public void ExecuteNonQuery_Nested_Transaction_Throws_InvalidOperationException()
        {
            const string INSERT = @"
            INSERT INTO mode
            (Code, Name)
            VALUES
            (@code, @name)
";

            var repository = GetObjectToTest();

            using (var connection = ConnectionManager.GetOpenWritableConnection(1))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (var command = transaction.CreateCommand())
                    {
                        command.CommandText = INSERT;

                        command.AddInputParameter("code", "TEST");
                        command.AddInputParameter("name", "TEST");

                        repository.MyExecuteNonQuery(command);
                    }

                    Action act = () => TransactionManager.GetTransaction(connection);
                    act.ShouldThrow<InvalidOperationException>().WithMessage("Nested transactions are not supported.");

                    transaction.Rollback();
                }
            }
        }

        [Test]
        public void ExecuteNonQuery_Throws_ApplicationDbException_For_Database_Exception()
        {
            var repository = GetObjectToTest();

            using (var connection = ConnectionManager.GetOpenReadOnlyConnection(1))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (var command = transaction.CreateCommand())
                    {
                        command.CommandText = "select * from Made_Up_Table";

                        Action act = () => repository.MyExecuteNonQuery(command);
                        act.ShouldThrow<ApplicationDbException>();
                    }
                }
            }
        }

        [Test]
        public void ExecuteNonQuery_Throws_ArgumentNullException_For_Command()
        {
            Action act = () => GetObjectToTest().MyExecuteNonQuery(null);

            act.ShouldThrow<ArgumentNullException>();
        }

        [Test]
        public void ExecuteNonQuery_Throws_MissingTransactionException_For_Command_Without_Transaction()
        {
            var repository = GetObjectToTest();

            using (var connection = ConnectionManager.GetOpenReadOnlyConnection(1))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (var command = transaction.CreateCommand())
                    {
                        command.Transaction = null;
                        command.CommandText = "select 1 from DUAL";

                        Action act = () => repository.MyExecuteNonQuery(command);

                        act.ShouldThrow<MissingTransactionException>()
                            .WithMessage(
                                "A transaction was not associated with the command being executed.  Transactions are required.  Please attached a transaction to your command prior to executing it.");
                    }
                }
            }
        }

        [Test]
        public void ExecuteReader_Can()
        {
            var repository = GetObjectToTest();

            using (var connection = ConnectionManager.GetOpenReadOnlyConnection(1))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (var command = transaction.CreateCommand())
                    {
                        command.CommandText = "select 1 from DUAL";

                        Action act = () =>
                        {
                            using (var dr = repository.MyExecuteReader(command))
                            {
                            }
                        };
                        act.ShouldNotThrow();
                    }
                }
            }
        }

        [Test]
        public void ExecuteReader_Can_Return_Multiple_Resultsets()
        {
            var repository = GetObjectToTest();

            using (var connection = ConnectionManager.GetOpenReadOnlyConnection(1))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (var command = transaction.CreateCommand())
                    {
                        command.CommandText = "select 1 from DUAL;select 1 from DUAL";

                        using (var reader = repository.MyExecuteReader(command))
                        {
                            reader.NextResult().Should().Be(true);
                        }
                    }
                }
            }
        }

        [Test]
        public void ExecuteReader_CommandBehavior_Can_Override()
        {
            var repository = GetObjectToTest();

            using (var connection = ConnectionManager.GetOpenReadOnlyConnection(1))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (var command = transaction.CreateCommand())
                    {
                        command.CommandText = "select 1 from DUAL";

                        using (var reader = repository.MyExecuteReader(command, CommandBehavior.CloseConnection))
                        {
                            reader.Close();

                            connection.State.Should().Be(ConnectionState.Closed);
                        }
                    }
                }
            }
        }

        [Test]
        public void ExecuteReader_CommandBehavior_Defaults_To_Default()
        {
            var repository = GetObjectToTest();

            using (var connection = ConnectionManager.GetOpenReadOnlyConnection(1))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (var command = transaction.CreateCommand())
                    {
                        command.CommandText = "select 1 from DUAL";

                        using (var reader = repository.MyExecuteReader(command))
                        {
                            reader.Close();

                            connection.State.Should().Be(ConnectionState.Open);
                        }
                    }
                }
            }
        }

        [Test]
        public void ExecuteReader_CommandTimeout_Defaults_To_Thirty_Seconds()
        {
            var repository = GetObjectToTest();

            using (var connection = ConnectionManager.GetOpenReadOnlyConnection(1))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (var command = transaction.CreateCommand())
                    {
                        command.CommandText = "select 1 from DUAL";

                        using (repository.MyExecuteReader(command))
                        {
                            command.CommandTimeout.Should().Be(30);
                        }
                    }
                }
            }
        }

        [Test]
        public void ExecuteReader_CommandTimeout_Overload_Is_Utilized()
        {
            var repository = GetObjectToTest();

            using (var connection = ConnectionManager.GetOpenReadOnlyConnection(1))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (var command = transaction.CreateCommand())
                    {
                        command.CommandText = "select 1 from DUAL";

                        using (repository.MyExecuteReader(command, CommandBehavior.Default, 20))
                        {
                            command.CommandTimeout.Should().Be(20);
                        }
                    }
                }
            }
        }

        [Test]
        public void ExecuteReader_Throws_ApplicationDbException_For_Database_Exception()
        {
            var repository = GetObjectToTest();

            using (var connection = ConnectionManager.GetOpenReadOnlyConnection(1))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (var command = transaction.CreateCommand())
                    {
                        command.CommandText = "select * from Made_Up_Table";

                        Action act = () => repository.MyExecuteReader(command);

                        act.ShouldThrow<ApplicationDbException>();
                    }
                }
            }
        }

        [Test]
        public void ExecuteReader_Throws_ArgumentNullException_For_Command()
        {
            Action act = () => GetObjectToTest().MyExecuteReader(null);

            act.ShouldThrow<ArgumentNullException>();
        }

        [Test]
        public void ExecuteReader_Throws_MissingTransactionException_For_Command_Without_Transaction()
        {
            var repository = GetObjectToTest();

            using (var connection = ConnectionManager.GetOpenReadOnlyConnection(1))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (var command = transaction.CreateCommand())
                    {
                        command.Transaction = null;
                        command.CommandText = "select 1 from DUAL";

                        Action act = () => repository.MyExecuteReader(command);

                        act.ShouldThrow<MissingTransactionException>();
                    }
                }
            }
        }

        [Test]
        public void ExecuteScalar_Can()
        {
            var repository = GetObjectToTest();

            using (var connection = ConnectionManager.GetOpenReadOnlyConnection(1))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (var command = transaction.CreateCommand())
                    {
                        command.CommandText = "select 1 from DUAL";

                        Action act = () => repository.MyExecuteScalar(command);

                        act.ShouldNotThrow();
                    }
                }
            }
        }

        [Test]
        public void ExecuteScalar_Can_Return_Id_After_Insert()
        {
            const string INSERT = @"
            INSERT INTO mode
            (Code, Name)
            VALUES
            (@code, @name);
            SELECT LAST_INSERT_ID()
";

            var repository = GetObjectToTest();

            using (var connection = ConnectionManager.GetOpenWritableConnection(1))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (var command = transaction.CreateCommand())
                    {
                        command.CommandText = INSERT;

                        command.AddInputParameter("code", "TEST");
                        command.AddInputParameter("name", "TEST");

                        var id = Convert.ToInt32(repository.MyExecuteScalar(command));

                        id.Should().BeGreaterThan(0);
                    }

                    transaction.Rollback();
                }
            }
        }

        [Test]
        public void ExecuteScalar_CommandTimeout_Defaults_To_Thirty_Seconds()
        {
            var repository = GetObjectToTest();

            using (var connection = ConnectionManager.GetOpenReadOnlyConnection(1))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (var command = transaction.CreateCommand())
                    {
                        command.CommandText = "select 1 from DUAL";
                        repository.MyExecuteScalar(command);

                        command.CommandTimeout.Should().Be(30);
                    }
                }
            }
        }

        [Test]
        public void ExecuteScalar_CommandTimeout_Overload_Is_Utlized()
        {
            var repository = GetObjectToTest();

            using (var connection = ConnectionManager.GetOpenReadOnlyConnection(1))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (var command = transaction.CreateCommand())
                    {
                        command.CommandText = "select 1 from DUAL";
                        repository.MyExecuteScalar(command, 20);

                        command.CommandTimeout.Should().Be(20);
                    }
                }
            }
        }

        [Test]
        public void ExecuteScalar_Throws_ApplicationDbException_For_Database_Exception()
        {
            var repository = GetObjectToTest();

            using (var connection = ConnectionManager.GetOpenReadOnlyConnection(1))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (var command = transaction.CreateCommand())
                    {
                        command.CommandText = "select * from Made_Up_Table";

                        Action act = () => repository.MyExecuteScalar(command);
                        act.ShouldThrow<ApplicationDbException>();
                    }
                }
            }
        }

        [Test]
        public void ExecuteScalar_Throws_ArgumentNullException_For_Command()
        {
            Action act = () => GetObjectToTest().MyExecuteScalar(null);

            act.ShouldThrow<ArgumentNullException>();
        }

        [Test]
        public void ExecuteScalar_Throws_MissingTransactionException_For_Command_Without_Exception()
        {
            var repository = GetObjectToTest();

            using (var connection = ConnectionManager.GetOpenReadOnlyConnection(1))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (var command = transaction.CreateCommand())
                    {
                        command.Transaction = null;
                        command.CommandText = "select 1 from DUAL";

                        Action act = () => repository.MyExecuteScalar(command);
                        act.ShouldThrow<MissingTransactionException>();
                    }
                }
            }
        }

        [Test]
        public void FillDataSet_Can()
        {
            var repository = GetObjectToTest();

            using (var connection = ConnectionManager.GetOpenReadOnlyConnection(1))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (
                        var dataAdapter = DbProviderFactories.GetFactory((DbConnection) connection).CreateDataAdapter())
                    {
                        using (var command = transaction.CreateCommand())
                        {
                            command.CommandText = "select 1 from DUAL";

                            dataAdapter.Should().NotBe(null, "Could not retrieve a data adapter");

                            dataAdapter.SelectCommand = (DbCommand) command;

                            var ds = new DataSet();

                            Action act = () => repository.MyFillDataSet(dataAdapter, ds);
                            act.ShouldNotThrow();
                        }
                    }
                }
            }
        }

        [Test]
        public void FillDataSet_CommandTimeout_Defaults_To_Thirty_Seconds()
        {
            var repository = GetObjectToTest();

            using (var connection = ConnectionManager.GetOpenReadOnlyConnection(1))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (
                        var dataAdapter = DbProviderFactories.GetFactory((DbConnection) connection).CreateDataAdapter())
                    {
                        using (var command = transaction.CreateCommand())
                        {
                            command.CommandText = "select 1 from DUAL";

                            dataAdapter.Should().NotBe(null, "Could not retrieve a data adapter");

                            dataAdapter.SelectCommand = (DbCommand) command;

                            var ds = new DataSet();

                            repository.MyFillDataSet(dataAdapter, ds);

                            command.CommandTimeout.Should().Be(30);
                        }
                    }
                }
            }
        }

        [Test]
        public void FillDataSet_CommandTimeout_Overload_Is_Utilized()
        {
            var repository = GetObjectToTest();

            using (var connection = ConnectionManager.GetOpenReadOnlyConnection(1))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (
                        var dataAdapter = DbProviderFactories.GetFactory((DbConnection) connection).CreateDataAdapter())
                    {
                        using (var command = transaction.CreateCommand())
                        {
                            command.CommandText = "select 1 from DUAL";

                            dataAdapter.Should().NotBe(null, "Could not retrieve a data adapter");

                            dataAdapter.SelectCommand = (DbCommand) command;

                            var ds = new DataSet();

                            repository.MyFillDataSet(dataAdapter, ds, 20);

                            command.CommandTimeout.Should().Be(20);
                        }
                    }
                }
            }
        }

        [Test]
        public void FillDataSet_Throws_ApplicationDbException_For_Database_Exception()
        {
            var repository = GetObjectToTest();

            using (var connection = ConnectionManager.GetOpenReadOnlyConnection(1))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (
                        var dataAdapter = DbProviderFactories.GetFactory((DbConnection) connection).CreateDataAdapter())
                    {
                        using (var command = transaction.CreateCommand())
                        {
                            command.CommandText = "select * from Made_Up_Table";

                            dataAdapter.Should().NotBe(null, "Could not retrieve a data adapter");

                            dataAdapter.SelectCommand = (DbCommand) command;

                            var ds = new DataSet();

                            Action act = () => repository.MyFillDataSet(dataAdapter, ds);
                            act.ShouldThrow<ApplicationDbException>();
                        }
                    }
                }
            }
        }

        [Test]
        public void FillDataSet_Throws_ArgumentException_For_Missing_Select_Command()
        {
            var repository = GetObjectToTest();

            using (var connection = ConnectionManager.GetOpenReadOnlyConnection(1))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (
                        var dataAdapter = DbProviderFactories.GetFactory((DbConnection) connection).CreateDataAdapter())
                    {
                        using (var command = transaction.CreateCommand())
                        {
                            command.CommandText = "select * from Made_Up_Table";

                            dataAdapter.Should().NotBe(null, "Could not retrieve a data adapter");

                            dataAdapter.SelectCommand = null;

                            var ds = new DataSet();

                            Action act = () => repository.MyFillDataSet(dataAdapter, ds);
                            act.ShouldThrow<ArgumentException>();
                        }
                    }
                }
            }
        }

        [Test]
        public void FillDataSet_Throws_ArgumentNullException_For_Adapter()
        {
            Action act = () => GetObjectToTest().MyFillDataSet(null, new DataSet());
            act.ShouldThrow<ArgumentNullException>();
        }

        [Test]
        public void FillDataSet_Throws_ArgumentNullException_For_DataSet()
        {
            var repository = GetObjectToTest();

            using (var connection = ConnectionManager.GetOpenReadOnlyConnection(1))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (
                        var dataAdapter = DbProviderFactories.GetFactory((DbConnection) connection).CreateDataAdapter())
                    {
                        using (var command = transaction.CreateCommand())
                        {
                            command.CommandText = "select * from Made_Up_Table";

                            dataAdapter.Should().NotBe(null, "Could not retrieve a data adapter");

                            dataAdapter.SelectCommand = (DbCommand) command;

                            Action act = () => repository.MyFillDataSet(dataAdapter, null);
                            act.ShouldThrow<ArgumentNullException>();
                        }
                    }
                }
            }
        }

        [Test]
        public void FillDataSet_Throws_MissingTransactionException_For_Command_Without_Transaction()
        {
            var repository = GetObjectToTest();

            using (var connection = ConnectionManager.GetOpenReadOnlyConnection(1))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (
                        var dataAdapter = DbProviderFactories.GetFactory((DbConnection) connection).CreateDataAdapter())
                    {
                        using (var command = transaction.CreateCommand())
                        {
                            command.Transaction = null;
                            command.CommandText = "select 1 from DUAL";

                            dataAdapter.Should().NotBe(null, "Could not retrieve a data adapter");

                            dataAdapter.SelectCommand = (DbCommand) command;

                            var ds = new DataSet();

                            Action act = () => repository.MyFillDataSet(dataAdapter, ds);
                            act.ShouldThrow<MissingTransactionException>();
                        }
                    }
                }
            }
        }

        [Test]
        public void FillDataTable_Can()
        {
            var repository = GetObjectToTest();

            using (var connection = ConnectionManager.GetOpenReadOnlyConnection(1))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (
                        var dataAdapter = DbProviderFactories.GetFactory((DbConnection) connection).CreateDataAdapter())
                    {
                        using (var command = transaction.CreateCommand())
                        {
                            command.CommandText = "select 1 from DUAL";

                            dataAdapter.Should().NotBe(null, "Could not retrieve a data adapter");

                            dataAdapter.SelectCommand = (DbCommand) command;

                            var dt = new DataTable();

                            Action act = () => repository.MyFillDataTable(dataAdapter, dt);
                            act.ShouldNotThrow();
                        }
                    }
                }
            }
        }

        [Test]
        public void FillDataTable_CommandTimeout_Defaults_To_Thirty_Seconds()
        {
            var repository = GetObjectToTest();

            using (var connection = ConnectionManager.GetOpenReadOnlyConnection(1))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (
                        var dataAdapter = DbProviderFactories.GetFactory((DbConnection) connection).CreateDataAdapter())
                    {
                        using (var command = transaction.CreateCommand())
                        {
                            command.CommandText = "select 1 from DUAL";

                            dataAdapter.Should().NotBe(null, "Could not retrieve a data adapter");

                            dataAdapter.SelectCommand = (DbCommand) command;

                            var dt = new DataTable();

                            repository.MyFillDataTable(dataAdapter, dt);

                            command.CommandTimeout.Should().Be(30);
                        }
                    }
                }
            }
        }

        [Test]
        public void FillDataTable_CommandTimeout_Overload_Is_Utilized()
        {
            var repository = GetObjectToTest();

            using (var connection = ConnectionManager.GetOpenReadOnlyConnection(1))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (
                        var dataAdapter = DbProviderFactories.GetFactory((DbConnection) connection).CreateDataAdapter())
                    {
                        using (var command = transaction.CreateCommand())
                        {
                            command.CommandText = "select 1 from DUAL";

                            dataAdapter.Should().NotBe(null, "Could not retrieve a data adapter");

                            dataAdapter.SelectCommand = (DbCommand) command;

                            var dt = new DataTable();

                            repository.MyFillDataTable(dataAdapter, dt, 20);

                            command.CommandTimeout.Should().Be(20);
                        }
                    }
                }
            }
        }

        [Test]
        public void FillDataTable_Throws_ApplicationDbException_For_Database_Exception()
        {
            var repository = GetObjectToTest();

            using (var connection = ConnectionManager.GetOpenReadOnlyConnection(1))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (
                        var dataAdapter = DbProviderFactories.GetFactory((DbConnection) connection).CreateDataAdapter())
                    {
                        using (var command = transaction.CreateCommand())
                        {
                            command.CommandText = "select * from Made_Up_Table";

                            dataAdapter.Should().NotBe(null, "Could not retrieve a data adapter");

                            dataAdapter.SelectCommand = (DbCommand) command;

                            var dt = new DataTable();

                            Action act = () => repository.MyFillDataTable(dataAdapter, dt);
                            act.ShouldThrow<ApplicationDbException>();
                        }
                    }
                }
            }
        }

        [Test]
        public void FillDataTable_Throws_ArgumentException_For_Missing_Select_Command()
        {
            var repository = GetObjectToTest();

            using (var connection = ConnectionManager.GetOpenReadOnlyConnection(1))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (
                        var dataAdapter = DbProviderFactories.GetFactory((DbConnection) connection).CreateDataAdapter())
                    {
                        using (var command = transaction.CreateCommand())
                        {
                            command.CommandText = "select * from Made_Up_Table";

                            dataAdapter.Should().NotBe(null, "Could not retrieve a data adapter");

                            dataAdapter.SelectCommand = null;

                            var dt = new DataTable();

                            Action act = () => repository.MyFillDataTable(dataAdapter, dt);
                            act.ShouldThrow<ArgumentException>();
                        }
                    }
                }
            }
        }

        [Test]
        public void FillDataTable_Throws_ArgumentNullException_For_Adapter()
        {
            Action act = () => GetObjectToTest().MyFillDataTable(null, new DataTable());
            act.ShouldThrow<ArgumentNullException>();
        }

        [Test]
        public void FillDataTable_Throws_ArgumentNullException_For_DataTable()
        {
            var repository = GetObjectToTest();

            using (var connection = ConnectionManager.GetOpenReadOnlyConnection(1))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (
                        var dataAdapter = DbProviderFactories.GetFactory((DbConnection) connection).CreateDataAdapter())
                    {
                        using (var command = transaction.CreateCommand())
                        {
                            command.CommandText = "select * from Made_Up_Table";

                            dataAdapter.Should().NotBe(null, "Could not retrieve a data adapter");

                            dataAdapter.SelectCommand = (DbCommand) command;

                            Action act = () => repository.MyFillDataTable(dataAdapter, null);
                            act.ShouldThrow<ArgumentNullException>();
                        }
                    }
                }
            }
        }

        [Test]
        public void FillDataTable_Throws_MissingTransactionException_For_Command_Without_Transaction()
        {
            var repository = GetObjectToTest();

            using (var connection = ConnectionManager.GetOpenReadOnlyConnection(1))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (
                        var dataAdapter = DbProviderFactories.GetFactory((DbConnection) connection).CreateDataAdapter())
                    {
                        using (var command = transaction.CreateCommand())
                        {
                            command.Transaction = null;
                            command.CommandText = "select 1 from DUAL";

                            dataAdapter.Should().NotBe(null, "Could not retrieve a data adapter");

                            dataAdapter.SelectCommand = (DbCommand) command;

                            var dt = new DataTable();

                            Action act = () => repository.MyFillDataTable(dataAdapter, dt);
                            act.ShouldThrow<MissingTransactionException>();
                        }
                    }
                }
            }
        }

        [Test]
        public void UpdateDataSet_Can()
        {
            var repository = GetObjectToTest();

            using (var connection = ConnectionManager.GetOpenWritableConnection(1))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (
                        var dataAdapter = DbProviderFactories.GetFactory((DbConnection) connection).CreateDataAdapter())
                    {
                        var ds = new DataSet();

                        using (var command = transaction.CreateCommand())
                        {
                            command.CommandText = "select * from mode";

                            dataAdapter.Should().NotBe(null, "Could not retrieve a data adapter");

                            dataAdapter.SelectCommand = (DbCommand) command;

                            repository.MyFillDataSet(dataAdapter, ds);
                        }

                        ds.Tables[0].Rows[0]["Code"] = "Test";

                        using (var command = transaction.CreateCommand())
                        {
                            command.CommandText = "Update mode set Code=@Code, Name=@Name where Id=@Id";

                            var parm = command.CreateParameter();
                            parm.ParameterName = "id";
                            parm.DbType = DbType.Int32;
                            parm.SourceColumn = "Id";
                            command.Parameters.Add(parm);

                            parm = command.CreateParameter();
                            parm.ParameterName = "code";
                            parm.DbType = DbType.String;
                            parm.SourceColumn = "Code";
                            command.Parameters.Add(parm);

                            parm = command.CreateParameter();
                            parm.ParameterName = "name";
                            parm.DbType = DbType.String;
                            parm.SourceColumn = "Name";
                            command.Parameters.Add(parm);

                            dataAdapter.UpdateCommand = (DbCommand) command;

                            repository.MyUpdateDataSet(dataAdapter, ds);
                        }

                        using (var command = transaction.CreateCommand())
                        {
                            command.CommandText = "Select Code from mode where Code='Test'";

                            repository.MyExecuteScalar(command).Should().Be("Test");
                        }
                    }

                    transaction.Rollback();
                }
            }
        }

        [Test]
        public void UpdateDataSet_Throws_ApplicationDbException_For_Database_Exception()
        {
            var repository = GetObjectToTest();

            using (var connection = ConnectionManager.GetOpenReadOnlyConnection(1))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (
                        var dataAdapter = DbProviderFactories.GetFactory((DbConnection) connection).CreateDataAdapter())
                    {
                        var ds = new DataSet();

                        using (var command = transaction.CreateCommand())
                        {
                            command.CommandText = "select * from mode";

                            dataAdapter.Should().NotBe(null, "Could not retrieve a data adapter");

                            dataAdapter.SelectCommand = (DbCommand) command;

                            repository.MyFillDataSet(dataAdapter, ds);
                        }

                        ds.Tables[0].Rows[0]["Code"] = "Test";

                        using (var command = transaction.CreateCommand())
                        {
                            command.CommandText = "Update Made_Up_Table set Code=@Code, Name=@Name where Id=@Id";

                            var parm = command.CreateParameter();
                            parm.ParameterName = "id";
                            parm.DbType = DbType.Int32;
                            parm.SourceColumn = "Id";
                            command.Parameters.Add(parm);

                            parm = command.CreateParameter();
                            parm.ParameterName = "code";
                            parm.DbType = DbType.String;
                            parm.SourceColumn = "Code";
                            command.Parameters.Add(parm);

                            parm = command.CreateParameter();
                            parm.ParameterName = "name";
                            parm.DbType = DbType.String;
                            parm.SourceColumn = "Name";
                            command.Parameters.Add(parm);

                            dataAdapter.UpdateCommand = (DbCommand) command;

                            Action act = () => repository.MyUpdateDataSet(dataAdapter, ds);
                            act.ShouldThrow<ApplicationDbException>();
                        }
                    }

                    transaction.Rollback();
                }
            }
        }

        [Test]
        public void UpdateDataSet_Throws_ArgumentException_For_Missing_Update_Command()
        {
            var repository = GetObjectToTest();

            using (var connection = ConnectionManager.GetOpenReadOnlyConnection(1))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (
                        var dataAdapter = DbProviderFactories.GetFactory((DbConnection) connection).CreateDataAdapter())
                    {
                        var ds = new DataSet();

                        using (var command = transaction.CreateCommand())
                        {
                            command.CommandText = "select * from mode";

                            dataAdapter.Should().NotBe(null, "Could not retrieve a data adapter");

                            dataAdapter.SelectCommand = (DbCommand) command;

                            repository.MyFillDataSet(dataAdapter, ds);
                        }

                        ds.Tables[0].Rows[0]["Code"] = "Test";

                        dataAdapter.UpdateCommand = null;

                        Action act = () => repository.MyUpdateDataSet(dataAdapter, ds);
                        act.ShouldThrow<ArgumentException>();
                    }

                    transaction.Rollback();
                }
            }
        }

        [Test]
        public void UpdateDataSet_Throws_ArgumentNullException_For_Adapter()
        {
            Action act = () => GetObjectToTest().MyUpdateDataSet(null, new DataSet());
            act.ShouldThrow<ArgumentNullException>();
        }

        [Test]
        public void UpdateDataSet_Throws_ArgumentNullException_For_DataSet()
        {
            var repository = GetObjectToTest();

            using (var connection = ConnectionManager.GetOpenReadOnlyConnection(1))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (
                        var dataAdapter = DbProviderFactories.GetFactory((DbConnection) connection).CreateDataAdapter())
                    {
                        using (var command = transaction.CreateCommand())
                        {
                            command.CommandText = "select 1 from DUAL";

                            dataAdapter.Should().NotBe(null, "Could not retrieve a data adapter");

                            dataAdapter.SelectCommand = (DbCommand) command;

                            Action act = () => repository.MyUpdateDataSet(dataAdapter, null);
                            act.ShouldThrow<ArgumentNullException>();
                        }
                    }
                }
            }
        }

        [Test]
        public void UpdateDataSet_Throws_MissingTransactionException_For_Command_Without_Transaction()
        {
            var repository = GetObjectToTest();

            using (var connection = ConnectionManager.GetOpenReadOnlyConnection(1))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (
                        var dataAdapter = DbProviderFactories.GetFactory((DbConnection) connection).CreateDataAdapter())
                    {
                        var ds = new DataSet();

                        using (var command = transaction.CreateCommand())
                        {
                            command.CommandText = "select * from mode";

                            dataAdapter.Should().NotBe(null, "Could not retrieve a data adapter");

                            dataAdapter.SelectCommand = (DbCommand) command;

                            repository.MyFillDataSet(dataAdapter, ds);
                        }

                        ds.Tables[0].Rows[0]["Code"] = "Test";

                        using (var command = transaction.CreateCommand())
                        {
                            command.Transaction = null;

                            command.CommandText = "Update mode set Code=@Code, Name=@Name where Id=@Id";

                            var parm = command.CreateParameter();
                            parm.ParameterName = "id";
                            parm.DbType = DbType.Int32;
                            parm.SourceColumn = "Id";
                            command.Parameters.Add(parm);

                            parm = command.CreateParameter();
                            parm.ParameterName = "code";
                            parm.DbType = DbType.String;
                            parm.SourceColumn = "Code";
                            command.Parameters.Add(parm);

                            parm = command.CreateParameter();
                            parm.ParameterName = "name";
                            parm.DbType = DbType.String;
                            parm.SourceColumn = "Name";
                            command.Parameters.Add(parm);

                            dataAdapter.UpdateCommand = (DbCommand) command;

                            Action act = () => repository.MyUpdateDataSet(dataAdapter, ds);
                            act.ShouldThrow<MissingTransactionException>();
                        }
                    }

                    transaction.Rollback();
                }
            }
        }

        [Test]
        public void UpdateDataTable_Can()
        {
            var repository = GetObjectToTest();

            using (var connection = ConnectionManager.GetOpenWritableConnection(1))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (
                        var dataAdapter = DbProviderFactories.GetFactory((DbConnection) connection).CreateDataAdapter())
                    {
                        var dt = new DataTable();

                        using (var command = transaction.CreateCommand())
                        {
                            command.CommandText = "select * from mode";

                            dataAdapter.Should().NotBe(null, "Could not retrieve a data adapter");

                            dataAdapter.SelectCommand = (DbCommand) command;

                            repository.MyFillDataTable(dataAdapter, dt);
                        }

                        dt.Rows[0]["Code"] = "Test";

                        using (var command = transaction.CreateCommand())
                        {
                            command.CommandText = "Update mode set Code=@Code, Name=@Name where Id=@Id";

                            var parm = command.CreateParameter();
                            parm.ParameterName = "id";
                            parm.DbType = DbType.Int32;
                            parm.SourceColumn = "Id";
                            command.Parameters.Add(parm);

                            parm = command.CreateParameter();
                            parm.ParameterName = "code";
                            parm.DbType = DbType.String;
                            parm.SourceColumn = "Code";
                            command.Parameters.Add(parm);

                            parm = command.CreateParameter();
                            parm.ParameterName = "name";
                            parm.DbType = DbType.String;
                            parm.SourceColumn = "Name";
                            command.Parameters.Add(parm);

                            dataAdapter.UpdateCommand = (DbCommand) command;

                            repository.MyUpdateDataTable(dataAdapter, dt);
                        }

                        using (var command = transaction.CreateCommand())
                        {
                            command.CommandText = "Select Code from mode where Code='Test'";

                            repository.MyExecuteScalar(command).Should().Be("Test");
                        }
                    }

                    transaction.Rollback();
                }
            }
        }

        [Test]
        public void UpdateDataTable_Throws_ApplicationDbException_For_Database_Exception()
        {
            var repository = GetObjectToTest();

            using (var connection = ConnectionManager.GetOpenReadOnlyConnection(1))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (
                        var dataAdapter = DbProviderFactories.GetFactory((DbConnection) connection).CreateDataAdapter())
                    {
                        var dt = new DataTable();

                        using (var command = transaction.CreateCommand())
                        {
                            command.CommandText = "select * from mode";

                            dataAdapter.Should().NotBe(null, "Could not retrieve a data adapter");

                            dataAdapter.SelectCommand = (DbCommand) command;

                            repository.MyFillDataTable(dataAdapter, dt);
                        }

                        dt.Rows[0]["Code"] = "Test";

                        using (var command = transaction.CreateCommand())
                        {
                            command.CommandText = "Update Made_Up_Table set Code=@Code, Name=@Name where Id=@Id";

                            var parm = command.CreateParameter();
                            parm.ParameterName = "id";
                            parm.DbType = DbType.Int32;
                            parm.SourceColumn = "Id";
                            command.Parameters.Add(parm);

                            parm = command.CreateParameter();
                            parm.ParameterName = "code";
                            parm.DbType = DbType.String;
                            parm.SourceColumn = "Code";
                            command.Parameters.Add(parm);

                            parm = command.CreateParameter();
                            parm.ParameterName = "name";
                            parm.DbType = DbType.String;
                            parm.SourceColumn = "Name";
                            command.Parameters.Add(parm);

                            dataAdapter.UpdateCommand = (DbCommand) command;

                            Action act = () => repository.MyUpdateDataTable(dataAdapter, dt);
                            act.ShouldThrow<ApplicationDbException>();
                        }
                    }

                    transaction.Rollback();
                }
            }
        }

        [Test]
        public void UpdateDataTable_Throws_ArgumentException_For_Missing_Update_Command()
        {
            var repository = GetObjectToTest();

            using (var connection = ConnectionManager.GetOpenReadOnlyConnection(1))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (
                        var dataAdapter = DbProviderFactories.GetFactory((DbConnection) connection).CreateDataAdapter())
                    {
                        var dt = new DataTable();

                        using (var command = transaction.CreateCommand())
                        {
                            command.CommandText = "select * from mode";

                            dataAdapter.Should().NotBe(null, "Could not retrieve a data adapter");

                            dataAdapter.SelectCommand = (DbCommand) command;

                            repository.MyFillDataTable(dataAdapter, dt);
                        }

                        dt.Rows[0]["Code"] = "Test";

                        dataAdapter.UpdateCommand = null;

                        Action act = () => repository.MyUpdateDataTable(dataAdapter, dt);
                        act.ShouldThrow<ArgumentException>();
                    }

                    transaction.Rollback();
                }
            }
        }

        [Test]
        public void UpdateDataTable_Throws_ArgumentNullException_For_Adapter()
        {
            Action act = () => GetObjectToTest().MyUpdateDataTable(null, new DataTable());
            act.ShouldThrow<ArgumentNullException>();
        }

        [Test]
        public void UpdateDataTable_Throws_ArgumentNullException_For_DataTable()
        {
            var repository = GetObjectToTest();

            using (var connection = ConnectionManager.GetOpenReadOnlyConnection(1))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (
                        var dataAdapter = DbProviderFactories.GetFactory((DbConnection) connection).CreateDataAdapter())
                    {
                        using (var command = transaction.CreateCommand())
                        {
                            command.CommandText = "select 1 from DUAL";

                            dataAdapter.Should().NotBe(null, "Could not retrieve a data adapter");

                            dataAdapter.SelectCommand = (DbCommand) command;

                            Action act = () => repository.MyUpdateDataTable(dataAdapter, null);
                            act.ShouldThrow<ArgumentNullException>();
                        }
                    }
                }
            }
        }

        [Test]
        public void UpdateDataTable_Throws_MissingTransactionException_For_Command_Without_Transaction()
        {
            var repository = GetObjectToTest();

            using (var connection = ConnectionManager.GetOpenReadOnlyConnection(1))
            {
                using (var transaction = TransactionManager.GetTransaction(connection))
                {
                    using (
                        var dataAdapter = DbProviderFactories.GetFactory((DbConnection) connection).CreateDataAdapter())
                    {
                        var dt = new DataTable();

                        using (var command = transaction.CreateCommand())
                        {
                            command.CommandText = "select * from mode";

                            dataAdapter.Should().NotBe(null, "Could not retrieve a data adapter");

                            dataAdapter.SelectCommand = (DbCommand) command;

                            repository.MyFillDataTable(dataAdapter, dt);
                        }

                        dt.Rows[0]["Code"] = "Test";

                        using (var command = transaction.CreateCommand())
                        {
                            command.Transaction = null;

                            command.CommandText = "Update mode set Code=@Code, Name=@Name where Id=@Id";

                            var parm = command.CreateParameter();
                            parm.ParameterName = "id";
                            parm.DbType = DbType.Int32;
                            parm.SourceColumn = "Id";
                            command.Parameters.Add(parm);

                            parm = command.CreateParameter();
                            parm.ParameterName = "code";
                            parm.DbType = DbType.String;
                            parm.SourceColumn = "Code";
                            command.Parameters.Add(parm);

                            parm = command.CreateParameter();
                            parm.ParameterName = "name";
                            parm.DbType = DbType.String;
                            parm.SourceColumn = "Name";
                            command.Parameters.Add(parm);

                            dataAdapter.UpdateCommand = (DbCommand) command;

                            Action act = () => repository.MyUpdateDataTable(dataAdapter, dt);
                            act.ShouldThrow<MissingTransactionException>();
                        }
                    }

                    transaction.Rollback();
                }
            }
        }

        #endregion

        #region Private Methods

        private MyRepository GetObjectToTest()
        {
            return new MyRepository();
        }

        #endregion
    }
}