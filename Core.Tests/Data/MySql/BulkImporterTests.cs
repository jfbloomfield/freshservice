﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Linq;
using AS.Crossbow.Core.Data.MySql;
using FluentAssertions;
using MySql.Data.MySqlClient;
using NUnit.Framework;

namespace Core.Tests.Data.MySql
{
    public class BulkImporterTests
    {
        #region Member Variables

        private BulkImporter _bulkImporter;

        private MySqlConnection _connection;
        private string _connectionString;
        private Random _rand;
        private IEnumerable<int> _range;
        private BulkImporterTestRepository _repository;

        #endregion

        #region Public Methods

        [Test]
        public void Can_Bulk_Insert()
        {
            var data = _range.Select(i => NewTestDatum(_rand)).ToList();
            using (var transaction = _connection.BeginTransaction())
            {
                _bulkImporter.BulkInsert(data, transaction, _repository.TableName);
                transaction.Commit();
            }
            var id = 10;
            var expectedDatum = data[id - 1];
            var resultDatum = _repository.GetTestDatum(_connection, id);
            resultDatum.Should().Be(expectedDatum, "TestDatum should have been inserted.");
        }

        [SetUp]
        public void SetUp()
        {
            _connectionString = ConfigurationManager.ConnectionStrings["DB_WRITABLE"].ConnectionString;
            _connection = new MySqlConnection(_connectionString);
            _connection.Open();
            _repository = new BulkImporterTestRepository();
            using (var transaction = _connection.BeginTransaction())
            {
                _repository.SetUpTables(_connection);
                transaction.Commit();
            }
            _bulkImporter = new BulkImporter();

            _rand = new Random();

            _range = Enumerable.Range(1, 10000);
        }

        [TearDown]
        public void TearDown()
        {
            using (var transaction = _connection.BeginTransaction())
            {
                _repository.TearDownTables(_connection);
                transaction.Commit();
            }
            _connection.Close();
            _connection.Dispose();
        }

        #endregion

        #region Private Methods

        private TestDatum NewTestDatum(Random rand)
        {
            return new TestDatum(
                score: rand.NextDouble() * Math.Pow(10, rand.Next(5)),
                name: "Doe, John\u00B9",
                points: rand.Next(),
                text1: "l;kas~djf;iu&$" + rand.Next(),
                text2: "iush\"f&3wq!(" + rand.Next(),
                text3: "asiogf87`23n^1DWE" + rand.Next());
        }

        #endregion
    }

    public class TestDatum : IEquatable<TestDatum>
    {
        #region Constructors

        public TestDatum(
            string name,
            double score,
            int points,
            string text1,
            string text2,
            string text3,
            string nullableText = null,
            int? nullableNumber = null,
            int? id = null)
        {
            Name = name;
            Score = score;
            Points = points;
            Text1 = text1;
            Text2 = text2;
            Text3 = text3;
            NullableText = nullableText;
            NullableNumber = nullableNumber;
            Id = id;
        }

        #endregion

        #region Properties

        public int? Id { get; set; }
        public string Name { get; set; }
        public int? NullableNumber { get; set; }
        public string NullableText { get; set; }
        public int Points { get; set; }
        public double Score { get; set; }
        public string Text1 { get; set; }
        public string Text2 { get; set; }
        public string Text3 { get; set; }

        #endregion

        #region IEquatable<TestDatum> Members

        public bool Equals(TestDatum other)
        {
            return Name == other.Name && Math.Abs(Score - other.Score) <= 0.0001 && Text1 == other.Text1 &&
                   Text2 == other.Text2 && Text3 == other.Text3 && NullableText == other.NullableText &&
                   NullableNumber == other.NullableNumber;
        }

        #endregion

        #region Public Methods

        public override bool Equals(object obj)
        {
            return obj.GetType() == typeof (TestDatum) ? Equals((TestDatum) obj) : base.Equals(obj);
        }

        #endregion
    }

    public class BulkImporterTestRepository
    {
        #region Constructors

        public BulkImporterTestRepository()
        {
            var random = new Random();
            TableName = "BulkImportTest" + random.Next(100, 999);
        }

        #endregion

        #region Properties

        public string TableName { get; }

        #endregion

        #region Public Methods

        public TestDatum GetTestDatum(DbConnection connection, int id)
        {
            var sql = $@"SELECT * FROM {TableName} WHERE id = @id";
            using (var command = connection.CreateCommand())
            {
                command.CommandText = sql;
                command.CommandType = CommandType.Text;
                var parameter = command.CreateParameter();
                parameter.ParameterName = "id";
                parameter.Value = id;
                command.Parameters.Add(parameter);

                using (var reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        var nullableNumberOrdinal = reader.GetOrdinal("NullableNumber");
                        var nullableTextOrdinal = reader.GetOrdinal("NullableText");
                        return new TestDatum(
                            id: reader.GetInt32(reader.GetOrdinal("Id")),
                            name: reader.GetString(reader.GetOrdinal("Name")),
                            score: reader.GetDouble(reader.GetOrdinal("Score")),
                            points: reader.GetInt32(reader.GetOrdinal("Points")),
                            text1: reader.GetString(reader.GetOrdinal("Text1")),
                            text2: reader.GetString(reader.GetOrdinal("Text2")),
                            text3: reader.GetString(reader.GetOrdinal("Text3")),
                            nullableText:
                                reader.IsDBNull(nullableTextOrdinal) ? null : reader.GetString(nullableTextOrdinal),
                            nullableNumber:
                                reader.IsDBNull(nullableNumberOrdinal)
                                    ? null
                                    : (int?) reader.GetInt32(nullableNumberOrdinal));
                    }
                    return null;
                }
            }
        }

        public void SetUpTables(DbConnection connection)
        {
            var sql =
                $@"
CREATE TABLE {TableName
                    } (
     ID int not null AUTO_INCREMENT
    ,Name varchar(100) not null
    ,Score double not null
    ,Points int not null
    ,Text1 varchar(100) not null
    ,Text2 varchar(100) not null
    ,Text3 varchar(100) not null
    ,NullableText varchar(100) null
    ,NullableNumber int null
    ,CONSTRAINT {
                    TableName}_PK PRIMARY KEY (ID)
);";

            using (var command = connection.CreateCommand())
            {
                command.CommandText = sql;
                command.CommandType = CommandType.Text;
                command.ExecuteNonQuery();
            }
        }

        public void TearDownTables(DbConnection connection)
        {
            var sql = $@"DROP TABLE IF EXISTS {TableName};";

            using (var command = connection.CreateCommand())
            {
                command.CommandText = sql;
                command.CommandType = CommandType.Text;
                command.ExecuteNonQuery();
            }
        }

        #endregion
    }
}