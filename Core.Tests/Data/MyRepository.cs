﻿using System.Data;
using System.Data.Common;
using AS.Crossbow.Core.Data;

namespace Core.Tests.Data
{
    public class MyRepository : BaseRepository
    {
        #region Public Methods

        public int MyExecuteNonQuery(IDbCommand command, int commandTimeout = 30)
        {
            return ExecuteNonQuery(command, commandTimeout);
        }

        public IDataReader MyExecuteReader(
            IDbCommand command,
            CommandBehavior commandBehavior = CommandBehavior.Default,
            int commandTimeout = 30)
        {
            return ExecuteReader(command, commandBehavior, commandTimeout);
        }

        public SafeDataReader MyExecuteSafeDataReader(
            IDbCommand command,
            CommandBehavior commandBehavior = CommandBehavior.Default,
            int commandTimeout = 30)
        {
            return ExecuteSafeDataReader(command, commandBehavior, commandTimeout);
        }

        public object MyExecuteScalar(IDbCommand command, int commandTimeout = 30)
        {
            return ExecuteScalar(command, commandTimeout);
        }

        public void MyFillDataSet(DbDataAdapter adapter, DataSet dataSet, int commandTimeout = 30)
        {
            FillDataSet(adapter, dataSet, commandTimeout);
        }

        public void MyFillDataTable(DbDataAdapter adapter, DataTable dataTable, int commandTimeout = 30)
        {
            FillDataTable(adapter, dataTable, commandTimeout);
        }

        public void MyUpdateDataSet(
            DbDataAdapter adapter,
            DataSet dataSet,
            int updateBatchSize = 1,
            int commandTimeout = 30)
        {
            UpdateDataSet(adapter, dataSet, updateBatchSize, commandTimeout);
        }

        public void MyUpdateDataTable(
            DbDataAdapter adapter,
            DataTable dataTable,
            int updateBatchSize = 1,
            int commandTimeout = 30)
        {
            UpdateDataTable(adapter, dataTable, updateBatchSize, commandTimeout);
        }

        #endregion
    }
}