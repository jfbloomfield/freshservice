﻿using System;
using System.Configuration;
using System.Data;
using AS.Crossbow.Core.Data;
using FluentAssertions;
using NUnit.Framework;

namespace Core.Tests.Data
{
    [TestFixture]
    [RequiresSTA]
    public class ConnectionManagerTests
    {
        #region Tests

        [Test]
        public void GetOpenReadOnlyConnection_Can()
        {
            using (var connection = ConnectionManager.GetOpenReadOnlyConnection(1))
            {
                connection.State.Should().Be(ConnectionState.Open);
            }
        }

        [Test]
        public void GetOpenWritableConnection_Can()
        {
            using (var connection = ConnectionManager.GetOpenWritableConnection(1))
            {
                connection.State.Should().Be(ConnectionState.Open);
            }
        }

        [Test]
        public void GetOpenConnection_Can()
        {
            using (var connection = ConnectionManager.GetOpenConnection("DB_READ_ONLY"))
            {
                connection.State.Should().Be(ConnectionState.Open);
            }
        }

        [Test]
        public void GetOpenConnection_Throws_ConfigurationErrorsException_When_Key_Is_Invalid()
        {
            Action act = () =>
            {
                using (var connection = ConnectionManager.GetOpenConnection("MADE_UP"))
                {

                }
            };

            act.ShouldThrow<ConfigurationErrorsException>();
        }

        #endregion
    }
}